### Useful Commands

# NPC Terminal
* storage.db['rooms.objects'].insert({ type: 'terminal', room: 'W0N0', x: 0, y:0 })


# Modify Storage
* storage.db['rooms.objects'].update({_id: IDOFOBJECT},{$set: {store: {energy:50000}}})
* storage.db['rooms.objects'].update({_id: IDOFOBJECT},{$set: {store: {energy:24000, L:24000, Z:24000, K:24000, U:24000, O:24000, H:24000, X:24000}}})
* storage.db['rooms.objects'].update({_id: IDOFOBJECT},{$set: {store: {energy:24000, L:24000, Z:24000, K:24000, U:24000, O:24000, H:24000, X:24000, ZK:6000, UL:6000, G:6000, GH:6000, GO:6000}}})
* storage.db['rooms.objects'].update({_id: IDOFOBJECT},{$set: {store: {energy:24000, L:6000, Z:6000, K:6000, U:6000, O:6000, H:6000, X:6000, UH:6000, UO:6000, KH:6000, KO:6000, LH:6000, LO:6000, ZH:6000, ZO:6000, GH:6000, GO:6000, OH:6000, ZK:6000, UL:6000, G:6000}}})

# Delete Items
* storage.db['rooms.objects'].removeWhere({_id: IDOFOBJECT})
* storage.db['rooms.objects'].removeWhere({type: 'constructionSite'})

# Construction Sites
* storage.db['rooms.objects'].update({ type: 'constructionSite' },{ $set: { progress: 99999 }})

# Ramparts
* storage.db['rooms.objects'].update({ type: 'rampart' },{ $set: { hits: 30000000 }})

# Invaders
* storage.db['rooms.objects'].insert({
  type: 'creep',
  user: '2',
  body:
    [ 
      { type: 'tough', hits: 100 },
      { type: 'move', hits: 100 },
      { type: 'ranged_attack', hits: 100, boost: 'KO' },
      { type: 'work', hits: 100, boost: 'ZH' },
      { type: 'attack', hits: 100, boost: 'UH' },
      { type: 'move', hits: 100 }
    ],
  hits: 4100,
  hitsMax: 5000,
  ticksToLive: 1500,
  x: x,
  y: y,
  room: 'roomName',
  store: {},
  storeCapacity: 0,
  name: 'invader_test'
})
