const log = require("./log");


function calculateAverageTick(force = false) {
    if (Game.time % 30 === 0 || force) {
        const now = new Date();
        const secondsSinceEpoch = Math.round(now.getTime() / 1000);
        const ticksPast = Game.time - global.tick;
        const timePast = secondsSinceEpoch - global.time;
        global.AVG_TICK_DURATION = timePast / ticksPast;
        global.tick = Game.time;
        global.time = secondsSinceEpoch;
    }
}


function creepConstants() {
    global.ROLE_ATTACKER = "Attacker";
    global.ROLE_BRUISER = "Bruiser";
    global.ROLE_FILLER = "Filler";
    global.ROLE_HARVESTER = "Harvester";
    global.ROLE_HEALER = "Healer";
    global.ROLE_MINER = "Miner";
    global.ROLE_RESERVER = "Reserver";
    global.ROLE_SCOUT = "Scout";
    global.ROLE_TANK = "Tank";
    global.ROLE_TRANSPORT = "Transport";
    global.ROLE_UPGRADER = "Upgrader";
    global.ROLE_WORKER = "Worker";

    global.CREEP_BODY = {
        [ROLE_ATTACKER]: [ATTACK, RANGED_ATTACK, TOUGH, MOVE, MOVE, MOVE],
        [ROLE_BRUISER]: [ATTACK, RANGED_ATTACK, TOUGH, MOVE, MOVE, MOVE],
        [ROLE_FILLER]: [CARRY, CARRY, MOVE],
        [ROLE_HARVESTER]: [WORK, WORK, CARRY, MOVE],
        [ROLE_HEALER]: [HEAL, TOUGH, MOVE, MOVE],
        [ROLE_MINER]: [WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE],
        [ROLE_RESERVER]: [CLAIM, CLAIM, MOVE],
        [ROLE_SCOUT]: [MOVE],
        [ROLE_TANK]: [TOUGH, MOVE],
        [ROLE_TRANSPORT]: [CARRY, CARRY, MOVE],
        [ROLE_UPGRADER]: [WORK, WORK, CARRY, MOVE],
        [ROLE_WORKER]: [WORK, WORK, CARRY, MOVE],
    }

    global.CREEP_MAX_SIZE = {
        [ROLE_ATTACKER]: 8,
        [ROLE_BRUISER]: 8,
        [ROLE_FILLER]: 10,
        [ROLE_HARVESTER]: 3,
        [ROLE_HEALER]:12,
        [ROLE_MINER]: 1,
        [ROLE_RESERVER]: 1,
        [ROLE_SCOUT]: 1,
        [ROLE_TANK]: 25,
        [ROLE_TRANSPORT]: 16,
        [ROLE_UPGRADER]: 10,
        [ROLE_WORKER]: 6,
    }


    global.SPAWN_PRIORITY = {
        // BRUISER_CRITICAL: 1,
        // ATTACKER_CRITICAL: 2,
        // HEALER_CRITICAL: 3,
        // TANK_STANDARD: 4,
        HARVESTER_CRITICAL: 1,
        FILLER_STANDARD: 2,
        ATTACKER_CRITICAL: 3,
        HEALER_CRITICAL: 4,
        BRUISER_CRITICAL: 5,
        TANK_STANDARD: 6,
        HARVESTER_STANDARD: 7,
        MINER_STANDARD: 8,
        SCOUT_STANDARD: 9,
        WORKER_CRITICAL: 10,
        HARVESTER_LOW: 11,
        UPGRADER_STANDARD: 12,
        TRANSPORT_HIGH: 13,
        RESERVER_STANDARD: 14,
        WORKER_STANDARD: 15,
        TRANSPORT_STANDARD: 16,
        WORKER_LOW: 17,
        TRANSPORT_LOW: 18,
    }
}


function colonyConstants() {
    global.OUTPOST_ALLOWED = {
        "1": 0,
        "2": 1,
        "3": 1,
        "4": 2,
        "5": 2,
        "6": 2,
        "7": 3,
        "8": 3,
    }
}



function buildPriority() {
    global.BUILD_PRIORITY = [
        STRUCTURE_SPAWN,
        STRUCTURE_TOWER,
        STRUCTURE_STORAGE,
        STRUCTURE_CONTAINER,
        STRUCTURE_EXTENSION,
        STRUCTURE_LINK,
        STRUCTURE_TERMINAL,
        STRUCTURE_EXTRACTOR,
        STRUCTURE_LAB,
        STRUCTURE_NUKER,
        STRUCTURE_OBSERVER,
        STRUCTURE_POWER_SPAWN,
        STRUCTURE_FACTORY,
        STRUCTURE_WALL,
        STRUCTURE_RAMPART,
        STRUCTURE_ROAD,
    ];
}


function boostTypes() {
    global.BOOST_TYPES = {
        "upgrade": [RESOURCE_CATALYZED_GHODIUM_ACID, RESOURCE_GHODIUM_ACID, RESOURCE_GHODIUM_HYDRIDE],
        "tough": [RESOURCE_CATALYZED_GHODIUM_ALKALIDE, RESOURCE_GHODIUM_ALKALIDE, RESOURCE_GHODIUM_OXIDE],
        "build": [RESOURCE_CATALYZED_LEMERGIUM_ACID, RESOURCE_LEMERGIUM_ACID, RESOURCE_LEMERGIUM_HYDRIDE],
    }
}


function labStates() {
    global.LAB_BOOSTING = "boosting";
    global.LAB_UNBOOSTING = "unboosting";

    global.LAB_AVAILABLE = "available";
    global.LAB_REQUESTED = "requested";
    global.LAB_WORKING = "working";
    global.LAB_DONE = "done";
}


function loadGlobals() {
    const SOMETHING_THAT_IS_MINE = _.find({...Game.structures, ...Game.creeps, ...Game.constructionSites});
    if (SOMETHING_THAT_IS_MINE) {
        global.USERNAME = SOMETHING_THAT_IS_MINE.owner.username;
    } else {
        global.USERNAME = "alice";
    }

    global.generatePixel = 0;
    global.toggleLog = log.toggleLog; // toggleLog available from console.
    global.ALLIANCE = ["CrAzYDubC", "Yuni"]; // TODO: add and remove from console command, auto add ppl who attack
    global.CONTROLLER_SIGNATURE = "We're All Mad Here";
    global.SCREEPS_NOVICE_SIGNATURE = "A new Novice or Respawn Area is being planned somewhere in this sector. Please make sure all important rooms are reserved.";

    buildPriority();
    colonyConstants();
    creepConstants();
    boostTypes();
    labStates();
    calculateAverageTick(true);
}


module.exports = {
    calculateAverageTick: calculateAverageTick,
    loadGlobals: loadGlobals,
}