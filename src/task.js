const log = require("./log");
const utilities = require("./utilities");


let currentRoomName = null;


function initializeTask(protoTask, roomName) {
    const fn = "initializeTask";
    currentRoomName = roomName;
    const taskName = protoTask.name;
    const target = utilities.deref(protoTask._target.ref);
    let task;
    log.write(log.TASK, fn, `Initializing task=${taskName} at target=${target} for ${protoTask._creep.name}`);
    switch(taskName) {
        case TaskAttack.taskName:
            task = new TaskAttack(target);
            // log.write(log.TASK|log.ATTACK, fn, ``);
            break;
        case TaskAttackController.taskName:
            task = new TaskAttackController(target);
            break;
        case TaskBuild.taskName:
            task = new TaskBuild(target);
            // log.write(log.TASK|log.WORKER, fn, ``);
            break;
        case TaskDismantle.taskName:
            task = new TaskDismantle(target);
            // log.write(log.TASK|log.WORKER, fn, ``);
            break;
        case TaskDrop.taskName:
            task = new TaskDrop(utilities.derefRoomPosition(protoTask._target._pos));
            // log.write(log.TASK|log.HARVESTER, fn, ``);
            break;
        case TaskGetBoosted.taskName:
            task = new TaskGetBoosted(target, protoTask.data.resourceType, protoTask.data.amount);
            break;
        case TaskGetUnBoosted.taskName:
            task = new TaskGetUnBoosted(target);
            break;
        case TaskGetRenewed.taskName:
            task = new TaskGetRenewed(target);
            break;
        case TaskGoTo.taskName:
            task = new TaskGoTo(utilities.derefRoomPosition(protoTask._target._pos));
            // log.write(log.TASK|log.GOTO, fn, ``);
            break;
        case TaskGoToRoom.taskName:
            task = new TaskGoToRoom(protoTask._target._pos.roomName);
            // log.write(log.TASK|log.GOTO, fn, ``);
            break;
        case TaskHarvest.taskName:
            task = new TaskHarvest(target);
            // log.write(log.TASK|log.HARVESTER|log.MINER, fn, ``);
            break;
        case TaskHeal.taskName:
            task = new TaskHeal(target);
            // log.write(log.TASK|log.HEALER, fn, ``);
            break;
        case TaskPickup.taskName:
            task = new TaskPickup(target);
            // log.write(log.TASK|log.FILLER|log.TRANSPORT|log.UPGRADER|log.WORKER, fn, ``);
            break;
        case TaskRepair.taskName:
            task = new TaskRepair(target);
            // log.write(log.TASK|log.WORKER, fn, ``);
            break;
        case TaskReserve.taskName:
            task = new TaskReserve(target);
            // log.write(log.TASK|log.RESREVER, fn, ``);
            break;
        case TaskSignController.taskName:
            task = new TaskSignController(target);
            // log.write(log.TASK|log.UPGRADER|log.WORKER, fn, ``);
            break;
        case TaskTransfer.taskName:
            task = new TaskTransfer(target);
            // log.write(log.TASK|log.FILLER|log.TRANSPORT|log.HARVESTER|log.MINER, fn, ``);
            break;
        case TaskTransferAll.taskName:
            task = new TaskTransferAll(target);
            // log.write(log.TASK|log.FILLER|log.TRANSPORT, fn, ``);
            break;
        case TaskUpgrade.taskName:
            task = new TaskUpgrade(target);
            // log.write(log.TASK|log.UPGRADER|log.WORKER, fn, ``);
            break;
        case TaskWithdraw.taskName:
            task = new TaskWithdraw(target);
            // log.write(log.TASK|log.FILLER|log.TRANSPORT|log.UPGRADER|log.WORKER, fn, ``);
            break;
        case TaskWithdrawAll.taskName:
            task = new TaskWithdrawAll(target);
            // log.write(log.TASK|log.TRANSPORT, fn, ``);
            break;
        default:
            task = new TaskInvalid(target);
            log.write(log.TASK|log.ERROR, fn, `Invalid task name ${taskName} from ${protoTask._creep.name}`);
            break;
    }

    task.proto = protoTask;
    return task;
}


class Task {
    constructor(name, target, options) {
        const fn = "Task";
        this.name = name;
        this._creep = {
            name: "",
        };
        if (target) {
            this._target = {
                ref: target.ref,
                _pos: target.pos,
            };
        } else {
            log.write(log.TASK, fn, `${name} with no target?`);
            this._target = {
                ref: "",
                _pos: {
                    x: 25,
                    y: 25,
                    roomName: currentRoomName, // seems to work I dont know
                }
            };
        }
        this._parent = null;
        this.settings = {
            targetRange: 1,
            oneShot: false,
        };
        this.tick = Game.time;
        this.options = options;
        this.data = {
            quiet: true,
        }
    }

    get proto() {
        return {
            name: this.name,
            _creep: this._creep,
            _target: this._target,
            _parent: this._parent,
            options: this.options,
            data: this.data,
            tick: this.tick,
        }
    }

    set proto(protoTask) {
        this._creep = protoTask._creep;
        this._target = protoTask._target;
        this._parent = protoTask._parent;
        this.options = protoTask.options;
        this.data = protoTask.data;
        this.tick = protoTask.tick;
    }

    get creep() {
        return Game.creeps[this._creep.name];
    }

    set creep(creep) {
        this._creep.name = creep.name;
      }

    get target() {
        return utilities.deref(this._target.ref);
    }

    get targetPos() {
        // TODO: The no target -1, -1, "", does't work when construction/repair sites are done.
        if (this.target) {
            this._target._pos = this.target.pos;
        }
        return utilities.derefRoomPosition(this._target._pos);
    }

    get parent() {
        return (this._parent ? initializeTask(this._parent) : null);
    }

    set parent(parentTask) {
        this._parent = parentTask ? parentTask.proto : null;
        if (this.creep) {
            this.creep.task = this
        }
    }

    get manifest() {
        let manifest = [this];
        let parent = this.parent;
        while (parent) {
            manifest.push(parent);
            parent = parent.parent;
        }
        return manifest
    }

    get targetManifest() {
        let targetRefs = [this._target.ref];
        let parent = this._parent;
        while (parent) {
            targetRefs.push(parent._target.ref);
            parent = parent._parent;
        }
        return _.map(targetRefs, ref => utilities.deref(ref));
    }

    get targetPosManifest() {
        let targetPositions = [this._target._pos];
        let parent = this._parent;
        while(parent) {
            targetPositions.push(parent._target._pos);
            parent = parent._parent;
        }
        return _.map(targetPositions, protoPos => utilities.derefRoomPosition(protoPos));
    }

    fork(newTask) {
        newTask.parent = this;
        if (this.creep) {
            this.creep.task = newTask;
        }
        return newTask;
    }

    isValid() {
        const fn = "Task.isValid";
        let validTask = false;
        if (this.creep) {
            validTask = this.isValidTask();
            log.write(log.TASK, fn, `${this.name} creep=${this.creep} validTask=${validTask}`);
        }
        let validTarget = false;
        if (this.target) {
            validTarget = this.isValidTarget();
            log.write(log.TASK, fn, `${this.name} target=${this.target} validTarget=${validTarget}`);
        } else {
            validTarget = false;
        }
        log.write(log.TASK, fn, `${this.name} validTask=${validTask} validTarget=${validTarget}`);
        if (validTask && validTarget) {
            return true;
        } else {
            log.write(log.TASK, fn, `${this.name} not valid, finish and return parent task we have one.`);
            this.finish();
            return this.parent ? this.parent.isValid() : false;
        }
    }

    moveToTarget(range = this.settings.targetRange) {
        if (this.options.moveOptions && !this.options.moveOptions.range) {
            this.options.moveOptions.range = range;
        }
        return this.creep.moveTo(this.targetPos, this.options.moveOptions);
        // return this.creep.travelTo(this.targetPos, this.options.moveOptions);
    }

    moveToNextPos() {
        if (this.options.nextPos) {
            let nextPos = utilities.derefRoomPosition(this.options.nextPos);
            return this.creep.moveTo(nextPos);
            // return this.creep.travelTo(nextPos);
        }
    }

    run() {
        const fn = "Task.run";
        log.write(log.TASK, fn, `${this.creep.name} running ${this.name} on ${this.target}`);
        if (this.creep.pos.inRangeTo(this.targetPos, this.settings.targetRange) && !this.creep.pos.isEdge) {
            log.write(log.TASK, fn, `${this.creep.name} in range of ${this.settings.targetRange} to ${this.target}`);
            let rc = this.work();
            log.write(log.TASK, fn, `${this.creep.name} work rc=${rc}`);
            if (this.settings.oneShot && rc === OK) {
                log.write(log.TASK, fn, `${this.creep.name} finished in one shot.`);
                this.finish();
            }
            return rc;
        } else if(this.creep.pos.isEdge) {
            if (this.creep.pos.x === 0) {
                return this.creep.move(RIGHT);
            } else if (this.creep.pos.x === 49) {
                return this.creep.move(LEFT);
            } else if (this.creep.pos.y === 0) {
                return this.creep.move(BOTTOM);
            } else if (this.creep.pos.y === 49) {
                return this.creep.move(TOP);
            } else {
                // nope
            }
        } else {
            log.write(log.TASK, fn, `${this.creep.name} not in range of ${this.settings.targetRange} to ${this.target}`);
            return this.moveToTarget();
        }
    }

    finish() {
        const fn = "Task.finish";
        log.write(log.TASK, fn, `${this.name} has been completed.`);
        this.moveToNextPos();
        if (this.creep) {
            log.write(log.TASK, fn, `${this.name} has been completed by ${this.creep.name}.`);
            this.creep.task = this.parent;
        } else {
            log.write(log.TASK|log.ERROR, fn, `${this.name} has no creep executing it.`)
        }
    }
}


class Tasks {
    static chain(tasks, setNextPos = true) {
        if (tasks.length === 0) {
            return null;
        }
        if (setNextPos) {
            for (let i = 0; i < tasks.length - 1; i++) {
                tasks[i].options.nextPos = tasks[i + 1].targetPos;
            }
        }
        let task = _.last(tasks);
        tasks = _.dropRight(tasks);
        for (let i = (tasks.length -1); i >= 0; i--) {
            task = task.fork(tasks[i]);
        }
        return task;
    }

    static attack(target, options = {}) {
        return new TaskAttack(target, options);
    }

    static attackController(target, options = {}) {
        return new TaskAttackController(target, options);
    }

    static build(target, options = {}) {
        return new TaskBuild(target, options);
    }

    static dismantle(target, options = {}) {
        return new TaskDismantle(target, options);
    }

    static drop(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        return new TaskDrop(target, resourceType, amount, options);
    }

    static getBoosted(target, boostType, partCount, options = {}) {
        return new TaskGetBoosted(target, boostType, partCount, options);
    }

    static getUnBoosted(target, options = {}) {
        return new TaskGetUnBoosted(target, options);
    }

    static getRenewed(target, options = {}) {
        return new TaskGetRenewed(target, options);
    }

    static goTo(target, options = {}) {
        return new TaskGoTo(target, options);
    }

    static goToRoom(target, options = {}) {
        return new TaskGoToRoom(target, options);
    }

    static harvest(target, options = {}) {
        return new TaskHarvest(target, options);
    }

    static heal(target, options = {}) {
        return new TaskHeal(target, options);
    }

    static pickup(target, options = {}) {
        return new TaskPickup(target, options);
    }

    static repair(target, options = {}) {
        return new TaskRepair(target, options);
    }

    static reserve(target, options = {}) {
        return new TaskReserve(target, options);
    }

    static signController(target, signature, options = {}) {
        return new TaskSignController(target, signature, options);
    }

    static transfer(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        return new TaskTransfer(target, resourceType, amount, options);
    }

    static transferAll(target, skipEnergy = false, options = {}) {
        return new TaskTransferAll(target, skipEnergy, options);
    }

    static ugprade(target, options = {}) {
        return new TaskUpgrade(target, options);
    }

    static withdraw(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        return new TaskWithdraw(target, resourceType, amount, options);
    }

    static withdrawAll(target, skipEnergy = false, options = {}) {
        return new TaskWithdrawAll(target, skipEnergy, options);
    }
}


class TaskAttack extends Task {

    constructor(target, options = {}) {
        super(TaskAttack.taskName, target, options);
        this.settings.targetRange = 3;
        this.settings.oneShot = true;
    }

    isValidTask() {
        const fn = "TaskAttack.isValidTask";
        const hasAttackPart = (this.creep.getActiveBodyparts(ATTACK) > 0 || this.creep.getActiveBodyparts(RANGED_ATTACK) > 0);
        log.write(log.TASK|log.ATTACK, fn, `hasAttackPart=${hasAttackPart}`);
        return hasAttackPart;
    }

    isValidTarget() {
        const fn = "TaskAttack.isValidTarget";
        const hasHits = (this.target && this.target.hits > 0);
        log.write(log.TASK|log.ATTACK, fn, `hasHits=${hasHits}`);
        return hasHits;
    }

    work() {
        const fn = "TaskAttack.work";
        // needed this some reason???
        if (!this.target) {
            log.write(log.ATTACK|log.ERROR, fn, `${this.creep.name} has no target in task execution.`);
            this.finish();
            return;
        }
        let attackReturn = 0;
        let rangedAttackReturn = 0;
        if (this.creep.getActiveBodyparts(ATTACK) > 0) {
            if (this.creep.pos.isNearTo(this.target)) {
                log.write(log.ATTACK, fn, `${this.creep.name} in range for melee attack.`);
                attackReturn = this.creep.attack(this.target);
            } else {
                log.write(log.ATTACK, fn, `${this.creep.name} not in range for melee attack.`);
                attackReturn = this.moveToTarget(1);
            }
            log.write(log.ATTACK, fn, `${this.creep.name} attackReturn=${attackReturn}`);
        }
        if (this.creep.pos.inRangeTo(this.target, 3) && this.creep.getActiveBodyparts(RANGED_ATTACK) > 0) {
            rangedAttackReturn = this.creep.rangedAttack(this.target);
            log.write(log.ATTACK, fn, `${this.creep.name} in range for ranged attack. rangedAttackReturn=${rangedAttackReturn}`);
        }
        if (attackReturn === OK && rangedAttackReturn === OK) {
            return OK;
        } else {
            if (attackReturn != OK) {
                return rangedAttackReturn;
            } else {
                return attackReturn;
            }
        }
    }
}
TaskAttack.taskName = "attack"

class TaskAttackController extends Task {

    constructor(target, options = {}) {
        super(TaskAttackController.taskName, target, options);
        this.settings.targetRange = 1;
    }

    isValidTask() {
        return this.creep.getActiveBodyparts(CLAIM) > 0;
    }

    isValidTarget() {
        return (this.target != null && ((this.target.reservation && this.target.reservation.username !== USERNAME) || (this.target.owner && this.target.owner.username !== USERNAME)));
    }

    work() {
        return this.creep.attackController(this.target);
    }
}
TaskAttackController.taskName = "attackController";

class TaskBuild extends Task {

    constructor(target, options = {}) {
        super(TaskBuild.taskName, target, options);
        this.settings.targetRange = 3;
    }

    isValidTask() {
        return !this.creep.isEmpty;
    }

    isValidTarget() {
        return this.target && this.target.my && this.target.progress < this.target.progressTotal;
    }

    work() {
        return this.creep.build(this.target);
    }
}
TaskBuild.taskName = "build";


class TaskDismantle extends Task {

    constructor(target, options = {}) {
        super(TaskDismantle.taskName, target, options);
    }

    isValidTask() {
        return (this.creep.getActiveBodyparts(WORK) > 0);
    }

    isValidTarget() {
        return this.target && this.target.hits > 0;
    }

    work() {
        return this.creep.dismantle(this.target);
    }
}
TaskDismantle.taskName = "dismantle";


class TaskDrop extends Task {

    constructor(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        if (target instanceof RoomPosition) {
            super(TaskDrop.taskName, {ref: "", pos: target}, options);
        } else {
            super(TaskDrop.taskName, {ref: "", pos: target.pos}, options);
        }
        this.settings.oneShot = true;
        this.settings.targetRange = 0;
        this.data.resourceType = resourceType;
        this.data.amount = amount;
    }

    isValidTask() {
        let amount = this.data.amount || 1;
        let resourcesTotal = this.creep.store.getUsedCapacity(this.data.resourceType) || 0;
        return resourcesTotal >= amount;
    }

    isValidTarget() {
        return true;
    }

    isValid() {
        let validTask = false;
        if (this.creep) {
            validTask = this.isValidTask();
        }
        if (validTask) {
            return true;
        } else {
            let isValid = false;
            if (this.parent) {
                isValid = this.parent.isValid();
            }
            this.finish();
            return isValid;
        }
    }

    work() {
        return this.creep.drop(this.data.resourceType, this.data.amount);
    }
}
TaskDrop.taskName = "drop"


class TaskGetBoosted extends Task {

    constructor(target, boostType, partCount, options = {}) {
        super(TaskGetBoosted.taskName, target, options);
        this.data.resourceType = boostType;
        this.data.amount = partCount;
        this.boostParts = {
            "UH": ATTACK,
            "UO": WORK,
            "KH": CARRY,
            "KO": RANGED_ATTACK,
            "LH": WORK,
            "LO": HEAL,
            "ZH": WORK,
            "ZO": MOVE,
            "GH": WORK,
            "GO": TOUGH,
        
            "UH2O": ATTACK,
            "UHO2": WORK,
            "KH2O": CARRY,
            "KHO2": RANGED_ATTACK,
            "LH2O": WORK,
            "LHO2": HEAL,
            "ZH2O": WORK,
            "ZHO2": MOVE,
            "GH2O": WORK,
            "GHO2": TOUGH,
        
            "XUH2O": ATTACK,
            "XUHO2": WORK,
            "XKH2O": CARRY,
            "XKHO2": RANGED_ATTACK,
            "XLH2O": WORK,
            "XLHO2": HEAL,
            "XZH2O": WORK,
            "XZHO2": MOVE,
            "XGH2O": WORK,
            "XGHO2": TOUGH,
        };
    }

    isValidTask() {
        let lifetime = _.any(this.creep.body, part => part.type === CLAIM) ? CREEP_CLAIM_LIFE_TIME : CREEP_LIFE_TIME;
        // boost new creeps only
        if (this.creep.ticksToLive && this.creep.ticksToLive < (0.9) * lifetime) {
            return false
        }
        let partCount = this.data.amount || this.creep.getActiveBodyparts(this.boostParts[this.data.resourceType]);
        return (this.creep.boostCounts[this.data.resourceType] || 0) < partCount;
    }

    isValidTarget() {
        return true;
    }

    work() {
        const fn = "TaskGetBoosted.work";
        let partCount = this.data.amount || this.creep.getActiveBodyparts(this.boostParts[this.data.resourceType]);
        if (this.target.mineralType == this.data.resourceType && this.target.mineralAmount >= LAB_BOOST_MINERAL * partCount && this.target.energy >= LAB_BOOST_ENERGY * partCount) {
            let rc = this.target.boostCreep(this.creep, this.data.amount);
            log.write(log.LAB, fn, `Boost attempted ${this.target} on ${this.creep.name} rc=${rc}`);
            if (rc === OK) {
                let labMemory = this.creep.room.memory.lab.boost[this.target.ref];
                log.write(log.LAB, fn, `Boost completed clearing boost state on ${this.target}`);
                labMemory.state = LAB_AVAILABLE;
                labMemory.resource = null;
                labMemory.amount = null;
            } else {
                log.write(log.ERROR, fn, `${this.creep.name} failed to get boosted rc=${rc}`);
            }
            return rc;
        }
    }
}
TaskGetBoosted.taskName = "getBoosted";


class TaskGetUnBoosted extends Task {
    constructor(target, options = {}) {
        super(TaskGetUnBoosted.taskName, target, options);
    }

    isValidTask() {
        return this.creep.isBoosted;
    }

    isValidTarget() {
        return true;
    }

    work() {
        const fn = "TaskGetUnBoosted.work";
        let rc = this.target.unboostCreep(this.creep);
        if (rc === OK) {
            let labMemory = this.creep.room.memory.lab.boost[this.target.ref];
            log.write(log.LAB, fn, `UnBoost completed clearing boost state on ${this.target}`);
            labMemory.state = LAB_AVAILABLE;
            labMemory.resource = null;
            labMemory.amount = null;
        } else {
            log.write(log.ERROR, fn, `${this.creep.name} failed to get unboosted rc=${rc}`);
        }
        return rc;
    }
}
TaskGetUnBoosted.taskName = "getUnBoosted";


class TaskGetRenewed extends Task {
    
    constructor(target, options = {}) {
        super(TaskGetRenewed.taskName, target, options);
    }

    isValidTask() {
        let hasClaimPart = _.filter(this.creep.body, (part) => part.type === CLAIM).length > 0;
        let lifetime = hasClaimPart ? CREEP_CLAIM_LIFE_TIME : CREEP_LIFE_TIME;
        return this.creep.ticksToLive !== undefined && this.creep.ticksToLive < 0.9 * lifetime;
    }

    isValidTarget() {
        return this.target.my;
    }

    work() {
        return this.target.renewCreep(this.creep);
    }
}
TaskGetRenewed.taskName = "getRenewed";


class TaskGoTo extends Task {

    constructor(target, options = {}) {
        if (utilities.hasPos(target)) {
            super(TaskGoTo.taskName, {ref: "", pos: target.pos}, options);
        } else {
            super(TaskGoTo.taskName, {ref: "", pos: target}, options);
        }
        if (options.targetRange) {
            this.settings.targetRange = options.targetRange;
        } else {
            this.settings.targetRange = 0;
        }
    }

    isValidTask() {
        return !this.creep.pos.inRangeTo(this.targetPos, this.settings.targetRange);
    }

    isValidTarget() {
        return true;
    }

    isValid() {
        let validTask = false;
        if (this.creep) {
            validTask = this.isValidTask();
        }

        if (validTask) {
            return true;
        } else {
            let isValid = false;
            if (this.parent) {
                isValid = this.parent.isValid();
            }
            this.finish();
            return isValid;
        }
    }

    work() {
        return OK;
    }
}
TaskGoTo.taskName = "goTo";


class TaskGoToRoom extends Task {

    constructor(roomName, options = {}) {
        super(TaskGoToRoom.taskName, {ref: "", pos: new RoomPosition(25, 25, roomName)}, options);
        this.settings.targetRange = 25;
    }

    isValidTask() {
        return !this.creep.pos.inRangeTo(this.targetPos, this.settings.targetRange);
    }

    isValidTarget() {
        return true;
    }

    isValid() {
        let validTask = false;
        if (this.creep) {
            validTask = this.isValidTask();
        }

        if (validTask) {
            return true;
        } else {
            let isValid = false;
            if (this.parent) {
                isValid = this.parent.isValid();
            }
            this.finish();
            return isValid;
        }
    }

    work() {
        return OK;
    }
}
TaskGoToRoom.taskName = "goToRoom";


class TaskHarvest extends Task {

    constructor(target, options = {}) {
        super(TaskHarvest.taskName, target, options);
    }

    isSource(object) {
        return object.energy != undefined;
    }

    isValidTask() {
        // TODO: Handle boosted work parts...
        // TODO: If on container and no link just keep going, resource will fall in container
        return !this.creep.isFull && this.creep.getActiveBodyparts(WORK) * 2 <= this.creep.store.getFreeCapacity();
    }

    isValidTarget() {
        if (this.isSource(this.target)) {
            return this.target.energy > 0;
        } else {
            this.target.mineralAmount > 0;
        }
    }

    work() {
        if (this.target instanceof Source || (this.target instanceof Mineral && this.creep.room.extractor.cooldown === 0)) {
            return this.creep.harvest(this.target);
        }
    }
}
TaskHarvest.taskName = "harvest";


class TaskHeal extends Task {

    constructor(target, options = {}) {
        super(TaskHeal.taskName, target, options);
        this.settings.targetRange = 3;
        this.settings.oneShot = true;
    }

    isValidTask() {
        return this.creep.getActiveBodyparts(HEAL) > 0;
    }

    isValidTarget() {
        return this.target && this.target.my && this.target.hits < this.target.hitsMax;
    }

    work() {
        if (this.creep.pos.isNearTo(this.target)) {
            return this.creep.heal(this.target);
        } else {
            this.moveToTarget(1);
        }
        return this.creep.rangedHeal(this.target);
    }
}
TaskHeal.taskName = "heal";


class TaskInvalid extends Task {

	constructor(target, options = {}) {
		super("INVALID", target, options);
	}

	isValidTask() {
		return false;
	}

	isValidTarget() {
		return false;
	}

	work() {
		return OK;
	}
}
TaskInvalid.taskName = "invalid";


class TaskPickup extends Task {

    constructor(target, options = {}) {
        super(TaskPickup.taskName, target, options);
        this.settings.oneShot = true;
    }

    isValidTask() {
        return !this.creep.isFull;
    }

    isValidTarget() {
        return this.target && this.target.amount > 0;
    }

    work() {
        return this.creep.pickup(this.target);
    }
}
TaskPickup.taskName = "pickup";


class TaskRepair extends Task {

    constructor(target, options = {}) {
        super(TaskRepair.taskName, target, options);
        this.settings.targetRange = 3;
    }

    isValidTask() {
        return this.creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0;
    }

    isValidTarget() {
        return this.target && this.target.hits < this.target.hitsMax;
    }

    work() {
        let result = this.creep.repair(this.target);
        if (this.target.structureType === STRUCTURE_ROAD) {
            let newHits = this.target.hits + this.creep.getActiveBodyparts(WORK) * REPAIR_POWER;
            if (newHits > this.target.hitsMax) {
                this.finish();
            }
        } else if (this.target.structureType === STRUCTURE_RAMPART) {
            if (this.target.hits >= this.target.room.barrierHealthPerRCL) {
                this.finish();
            }
        }
        return result;
    }
}
TaskRepair.taskName = "repair";


class TaskReserve extends Task {

    constructor(target, options = {}) {
        super(TaskReserve.taskName, target, options);
    }

    isValidTask() {
        return this.creep.getActiveBodyparts(CLAIM) > 0;
    }

    isValidTarget() {
        return (this.target != null && !this.target.owner && (this.target.reservation && this.target.reservation.username === USERNAME));
    }

    work() {
        return this.creep.reserveController(this.target);
    }
}
TaskReserve.taskName = "reserve";


class TaskSignController extends Task {

    constructor(target, signature = CONTROLLER_SIGNATURE, options = {}) {
        super(TaskSignController.taskName, target, options);
        this.data.signature = signature;
    }

    isValidTask() {
        return true;
    }

    isValidTarget() {
        return (!this.target.sign || this.target.sign.text !== this.data.signature);
    }

    work() {
        return this.creep.signController(this.target, this.data.signature);
    }
}
TaskSignController.taskName = "signController";


class TaskTransfer extends Task {

    constructor(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        super(TaskTransfer.taskName, target, options);
        this.settings.oneShot = true;
        this.data.resourceType = resourceType;
        this.data.amount = amount;
    }

    isValidTask() {
        let amount = this.data.amount || 1;
        let resourceTotal = this.creep.store.getUsedCapacity(this.data.resourceType) || 0;
        return resourceTotal >= amount;
    }

    isValidTarget() {
        const fn = "TaskTransfer.isValidTarget";
        let amount = this.data.amount || 1;
        let target = this.target;
        if (target instanceof Creep || utilities.isStoreStructure(target) || utilities.isEnergyStructure(target)) {
            return target.store.getFreeCapacity(this.data.resourceType) >= amount
        } else {
            log.write(log.ERROR|log.NOTIFY, fn, `Task transfer not implemented for ${target}`);
        }
        return false;
    }

    work() {
        return this.creep.transfer(this.target, this.data.resourceType, this.data.amount);
    }
}
TaskTransfer.taskName = "transfer";


class TaskTransferAll extends Task {

    constructor(target, skipEnergy = false, options = {}) {
        super(TaskTransferAll.taskName, target, options);
        this.data.skipEnergy = skipEnergy;
    }

    isValidTask() {
        for (let resourceType in this.creep.store) {
            if (this.data.skipEnergy && resourceType === RESOURCE_ENERGY) {
                continue;
            }
            let amountTotal = this.creep.store.getUsedCapacity(resourceType) || 0;
            if (amountTotal > 0) {
                return true;
            }
        }
        return false;
    }

    isValidTarget() {
        return this.target.store.getFreeCapacity() < this.target.store.getCapacity();
    }

    work() {
        for (let resourceType in this.creep.store) {
            if (this.data.skipEnergy && resourceType === RESOURCE_ENERGY) {
                continue;
            }
            let amountTotal = this.target.store.getFreeCapacity(resourceType) || 0;
            if (amountTotal > 0) {
                return this.creep.transfer(this.target, resourceType);
            }
        }
        return -1;
    }
}
TaskTransferAll.taskName = "transferAll";


class TaskUpgrade extends Task {

    constructor(target, options = {}) {
        super(TaskUpgrade.taskName, target, options);
        this.settings.targetRange = 3;
    }

    isValidTask() {
        return !this.creep.isEmpty;
    }

    isValidTarget() {
        return this.target && this.target.my;
    }

    work() {
        return this.creep.upgradeController(this.target);
    }
}
TaskUpgrade.taskName = "upgrade";


class TaskWithdraw extends Task {

    constructor(target, resourceType = RESOURCE_ENERGY, amount = undefined, options = {}) {
        super(TaskWithdraw.taskName, target, options);
        this.settings.oneShot = true;
        this.data.resourceType = resourceType;
        this.data.amount = amount;
    }

    isValidTask() {
        return !this.creep.isFull;
    }

    isValidTarget() {
        let amount = this.data.amount || 1;
        let target = this.target;
        if (target && target instanceof Creep || target instanceof Tombstone || utilities.isStoreStructure(target) || utilities.isEnergyStructure(target)) {
            return (target.store.getUsedCapacity(this.data.resourceType) || 0) >= amount;
        } else {
            return false;
        }
    }

    work() {
        return this.creep.withdraw(this.target, this.data.resourceType, this.data.amount);
    }
}
TaskWithdraw.taskName = "withdraw";


class TaskWithdrawAll extends Task {

    constructor(target, skipEnergy = false, options = {}) {
        super(TaskWithdrawAll.taskName, target, options);
        this.data.skipEnergy = skipEnergy;
    }

    isValidTask() {
        return !this.creep.isFull;
    }

    isValidTarget() {
        return this.target.store.getUsedCapacity() > 0;
    }

    work() {
        for (let resourceType in this.target.store) {
            if (this.data.skipEnergy && resourceType === RESOURCE_ENERGY) {
                continue;
            }
            let amountTotal = this.target.store.getUsedCapacity(resourceType) || 0;
            if (amountTotal > 0) {
                return this.creep.withdraw(this.target, resourceType);
            }
        }
        return -1;
    }
}
TaskWithdrawAll.taskName = "withdrawAll";


module.exports = {
    initializeTask: initializeTask,
    Tasks:Tasks,
    TaskAttack: TaskAttack,
    TaskAttackController: TaskAttackController,
    TaskBuild: TaskBuild,
    TaskDismantle: TaskDismantle,
    TaskDrop: TaskDrop,
    TaskInvalid: TaskInvalid,
    TaskGetBoosted: TaskGetBoosted,
    TaskGetUnBoosted: TaskGetUnBoosted,
    TaskGetRenewed: TaskGetRenewed,
    TaskGoTo: TaskGoTo,
    TaskGoToRoom: TaskGoToRoom,
    TaskHarvest: TaskHarvest,
    TaskHeal: TaskHeal,
    TaskPickup: TaskPickup,
    TaskRepair: TaskRepair,
    TaskReserve: TaskReserve,
    TaskSignController: TaskSignController,
    TaskTransfer: TaskTransfer,
    TaskTransferAll: TaskTransferAll,
    TaskUpgrade: TaskUpgrade,
    TaskWithdraw: TaskWithdraw,
    TaskWithdrawAll: TaskWithdrawAll,
}