class TargetCache {

    constructor() {
        this.targets = {};
        this.tick = Game.time;
    }

    cacheTargets() {
        this.targets = {};

        for (let name in Game.creeps) {
            let creep = Game.creeps[name];
            let task = creep.memory.task;
            while (task) {
                if (!this.targets[task._target.ref]) {
                    this.targets[task._target.ref] = [];
                }
                this.targets[task._target.ref].push(creep.name);
                task = task._parent;
            }
        }
    }

    static assert() {
        if (!(Game.TargetCache && Game.TargetCache.tick == Game.time)) {
            Game.TargetCache = new TargetCache();
            Game.TargetCache.build();
        }
    }

    build() {
        this.cacheTargets();
    }
}


module.exports = {
    TargetCache: TargetCache,
}