const base = require("./base");
const lab = require("./lab");
const log = require("./log");
const market = require("./market");
const outpost = require("./outpost");

const utilities = require("./utilities");


const COMPRESSIONS = {
    G: {
        reaction: RESOURCE_GHODIUM_MELT,
        one: RESOURCE_GHODIUM,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    O: {
        reaction: RESOURCE_OXIDANT,
        one: RESOURCE_OXYGEN,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    H: {
        reaction: RESOURCE_REDUCTANT,
        one: RESOURCE_HYDROGEN,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    U: {
        reaction: RESOURCE_UTRIUM_BAR,
        one: RESOURCE_UTRIUM,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    L: {
        reaction: RESOURCE_LEMERGIUM_BAR,
        one: RESOURCE_LEMERGIUM,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    K: {
        reaction: RESOURCE_KEANIUM_BAR,
        one: RESOURCE_KEANIUM,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    Z: {
        reaction: RESOURCE_ZYNTHIUM_BAR,
        one: RESOURCE_ZYNTHIUM,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    X: {
        reaction: RESOURCE_PURIFIER,
        one: RESOURCE_CATALYST,
        two: RESOURCE_ENERGY,
        output: 200,
        time: 20,
    },
    // "energy": {
    //     reaction: RESOURCE_BATTERY,
    //     one: RESOURCE_ENERGY,
    //     two: RESOURCE_ENERGY,
    //     output: 100,
    // }
}


class SpawnQueue {
    constructor(colony) {
        this.colony = colony;
        this.spawns = colony.room.find(FIND_MY_SPAWNS);
        this.queue = [];
    }

    addCreepToQueue(creepBody, creepName, creepOpts, creepPriority) {
        const fn = "SpawnQueue.add"
        log.write(log.SPAWN, fn, `${this.colony.name} adding ${creepOpts.memory.role} to spawn queue.`);
        this.queue.push({
            body: creepBody,
            name: creepName,
            opts: creepOpts,
            priority: creepPriority,
        });
    }

    get availableSpawnStructure() {
        for (const spawn of this.spawns) {
            if (!spawn.spawning && spawn.isActive()) {
                return spawn;
            }
        }
    }

    get hasSpawnRequest() {
        const fn = "SpawnQueue.hasSpawnRequest";
        if (this.queue.length > 0) {
            log.write(log.SPAWN, fn, `${this.colony.name} has ${this.queue.length} spawn requests.`);
            return true;
        }
        return false;
    }

    get highestPriorityCreepToSpawn() {
        const fn = "SpawnQueue.highestPriorityCreepToSpawn";
        this.sortQueue();
        const highestPrioritySpawn = this.queue[0];
        log.write(log.SPAWN, fn, `${this.colony.name} highest priority role is ${highestPrioritySpawn.opts.memory.role}`);
        return highestPrioritySpawn;
    }

    sortQueue() {
        this.queue.sort(function(a, b) {
            return (a.priority < b.priority ? -1 : 1)
        });
    }

    attemptToSpawnCreep(creepToSpawn, availableSpawnStructure) {
        const fn ="SpawnQueue.attemptToSpawnCreep";
        if (availableSpawnStructure) {
            let rc = availableSpawnStructure.spawnCreep(creepToSpawn.body, creepToSpawn.name, creepToSpawn.opts);
            log.write(log.SPAWN, fn, `${this.colony.name} creepBody=${creepToSpawn.body} creepOpts=${JSON.stringify(creepToSpawn.opts)} spawn=${availableSpawnStructure.name} rc=${rc}`);
        } else if (creepToSpawn.opts.memory.role === ROLE_BRUISER) {
            // TODO: Do we want to cancel a spawn? (energy is lost)
        }
    }

    checkToSpawnCreep() {
        const fn = "SpawnQueue.executeSpawn";
        if (this.hasSpawnRequest) {
            const creepToSpawn = this.highestPriorityCreepToSpawn;
            const availableSpawnStructure = this.availableSpawnStructure;
            this.attemptToSpawnCreep(creepToSpawn, availableSpawnStructure);
        } else {
            log.write(log.SPAWN, fn, `${this.colony.name} has no spawn requests.`);
        }
    }
}


class Colony {
    constructor(room) {
        this.name = room.name;
        this.room = room;
        this.rooms = [room];
        this.roomNames = [room.name];
        this.creeps = _.filter(Game.creeps, function(creep) { return creep.memory.colony === room.name });
        this.creepsByRole = _.groupBy(this.creeps, function(creep) { return creep.memory.role });
        this.spawns = this.room.spawns;
        this.spawnQueue = new SpawnQueue(this);
        this.colonyFlags = _.filter(Game.flags, function(flag) { return flag.memory.colony === room.name });
        this.attacked = false;
    }

    run() {
        this.outpostLogic();
        this.flagLogic();
        this.findNewOutpost();

        this.safeModeLogic();
        if (this.shouldBuildCheck) {
            this.buildCheck();
            this.roadLogic();
        }
        this.terminalLogic();
        this.defenseLogic();
        this.cpuLogic();
        this.spawnLogic();
        this.towerLogic();
        this.linkLogic();
        this.labLogic();
        // this.factoryLogic();
        this.visuals();
    }

    cpuLogic() {
        if (this.attacked || this.hostileCreeps.length > 0 || this.stopPixelGeneration) {
            generatePixel = -1;
        } else {
            if (generatePixel !== -1) {
                generatePixel = 1;
            }
        }
    }

    get stopPixelGeneration() {
        if (this.room.controller.level >= 3 && (this.room.controllerProgressPercentage <= 0.05 || this.room.controllerProgressPercentage >= 0.95)) {
            return true;
        } else {
            return false;
        }
    }

    get borderRoomsChecked() {
        this._borderRoomsChecked = true;
        for (let dir in this.room.memory.exits) {
            let roomName = this.room.memory.exits[dir];
            if (!(roomName in Memory.rooms) || (roomName in Memory.rooms && Memory.rooms[roomName].scoutTime === -1)) {
                this._borderRoomsChecked = false;
            }
        }
        return this._borderRoomsChecked;
    }

    get outposts() {
        if (!("outposts" in this.room.memory)) {
            this.room.memory.outposts = {};
        }
        if (!this._outposts) {
            this._outposts = [];
            for (let roomName in this.room.memory.outposts) {
                const newOutpost = new outpost.initializeOutpost(this.room.memory.outposts[roomName]);
                // TODO: Try to get rid of this.rooms for this.outpost
                if (newOutpost.room) {
                    this.rooms.push(newOutpost.room);
                }
                this.roomNames.push(roomName);
                this._outposts.push(newOutpost);
            }
        }
        return this._outposts;
    }

    outpostLogic() {
        const fn = "Colony.outpostLogic";
        for (let outpost of this.outposts) {
            if (outpost.room && outpost.room.controller.owner) {
                // Someone spawned in our outpost pre reservable
                delete Memory.rooms[this.name].outposts[outpost.name];
                delete Memory.rooms[outpost.name];
                continue;
            }
            outpost.run();
        }
    }

    flagLogic() {
        if (this.room.controller.level >= 2) {
            for (let flag of this.colonyFlags) {
                // Game.flags["attack:aaabbb"].memory = {"colony": "W7N4", "type": "attack", "stage": "spawning", "room": "W8N4"}
                if (flag.memory.type === "attack") {
                    let attackStage = flag.memory.stage;
                    let healers = _.filter(this.getCreepsByRole(ROLE_HEALER));
                    let attackers = _.filter(this.getCreepsByRole(ROLE_ATTACKER));
                    if (attackStage === "spawning" && healers.length === 1 && attackers.length === 1) {
                        flag.memory.stage = "active";
                    } else if (attackStage === "spawning" && healers.length === 0 && attackers.length === 0) {
                        this.requestCreepSpawn(ROLE_HEALER, SPAWN_PRIORITY.HEALER_CRITICAL, true);
                    } else if (attackStage === "spawning" && healers.length === 1 && attackers.length == 0) {
                        this.requestCreepSpawn(ROLE_ATTACKER, SPAWN_PRIORITY.ATTACKER_CRITICAL, true);
                    }

                    if (attackStage === "active" && healers.length === 0 && attackers.length === 0) {
                        //
                    }
                } else if (flag.memory.type === "tank") {
                    let tanks = _.filter(this.getCreepsByRole(ROLE_TANK));
                    if (tanks.length < 1) {
                        this.requestCreepSpawn(ROLE_TANK, SPAWN_PRIORITY.TANK_STANDARD, true);
                    }
                }
            }
        }
    }

    findNewOutpost() {
        const fn = "Colony.outpostLogic";
        if (this.room.memory.controllerLevel && OUTPOST_ALLOWED[this.room.memory.controllerLevel.toString()] > this.outposts.length) {
            let twoSourceRooms = [];
            let oneSourceRooms = [];
            for (let roomName in Memory.rooms) {
                let roomMemory = Memory.rooms[roomName];
                if (this.roomNames.includes(roomName)) {
                    continue;
                }
                if (roomMemory.outpost === true && roomMemory.sourcesCount === 2) {
                    twoSourceRooms.push(roomName);
                } else if (roomMemory.outpost === true && roomMemory.sourcesCount === 1) {
                    oneSourceRooms.push(roomName);
                }
            }
            // Verify we have been to all colony neighbors before deciding on an outpost.
            if (this.borderRoomsChecked) {
                let roomName = null;
                if (twoSourceRooms.length > 0) {
                    twoSourceRooms.sort(function(a,b) {
                        return Memory.rooms[a].sourcesDistance < Memory.rooms[b].sourcesDistance ? -1 : 1;
                    });
                    roomName = twoSourceRooms[0];
                } else if (oneSourceRooms.length > 0) {
                    oneSourceRooms.sort(function(a,b) {
                        return Memory.rooms[a].sourcesDistance < Memory.rooms[b].sourcesDistance ? -1 : 1;
                    });
                    roomName = oneSourceRooms[0];
                }
                if (roomName) {
                    this.room.memory.outposts[roomName] = {name: roomName, colony: this.name};
                }
            }
        }
    }

    get availableRamparts() {
        if (!this._availableRamparts) {
            this._availableRamparts = _.filter(this.room.ramparts, (r) => r.pos.isPassible())
        }
        return this._availableRamparts;
    }

    safeModeLogic() {
        const fn = "Colony.safeModeLogic";
        const attackEvents = this.room.logs[EVENT_ATTACK];
        if (_.size(attackEvents) === 0) {
            return;
        }

        for (let event of attackEvents) {
            const target = utilities.deref(event.data.targetId);
            if (target && target.my) {
                const enemy = utilities.deref(event.objectId);
                if (enemy) {
                    if (enemy.owner.username !== "Invader") {
                        if (target.isCriticalStructure) {
                            if (this.room.controller.safeModeCooldwon === undefined && this.room.controller.safeModeAvailable > 0) {
                                this.room.controller.activateSafeMode();
                                log.write(log.NOTIFY, fn, `${this.name} structure ${target.structureType} is under attack, activated safe mode!!!`);
                            }
                        }
                    }
                }
            }
        }
    }

    get shouldBuildCheck() {
        if (this.room.memory.shouldBuild) {
            this.room.memory.shouldBuild = false;
            return true;
        }

        if (!this.room.memory.controllerLevel) {
            this.room.memory.controllerLevel = this.room.controller.level;
            return true;
        }
        if (this.room.controller.level !== this.room.memory.controllerLevel) {
            this.room.memory.controllerLevel = this.room.controller.level;
            return true;
        }
        if (Game.time % 1500 === 0) {
            return true;
        }
        for (let room of this.rooms) {
            const destroyedEvents = _.filter(room.logs[EVENT_OBJECT_DESTROYED], (obj) => obj.data.type !== "creep");
            if (destroyedEvents.length > 0) {
                return true;
            }
        }

    }

    statusOutput() {
        let x = 5;
        let y = 3;
        let rV = this.room.visual;
        rV.text("Alice", x, y, {color: "#F8F8FF", font: 2, align: "center"});
        y++;
        y++;
        rV.text(`${Game.shard.name} ${this.name}`, x, y, {color: "#F8F8FF", font: 1, align: "center"});
        y++;
        y++;
        rV.text("Creeps", x, y, {color: "#F8F8FF", font: 1, align: "center"});
        y++;
        for (let role in this.creepsByRole) {
            let creeps = this.creepsByRole[role];
            rV.text(`${role}s ${creeps.length}`, x, y, {color: "#F8F8FF", align: "center"});
            y++;
        }
        y++;
        rV.text("Outposts", x, y, {color: "#F8F8FF", font: 1, align: "center"});
        y++;
        if (this.outposts.length > 0) {
            for (let outpost of this.outposts) {
                if (outpost.name === this.name) {
                    continue;
                }
                rV.text(outpost.name, x, y, {color: "#F8F8FF", align: "center"});
                y++;
            }
        } else {
            rV.text("None", x, y, {color: "#F8F8FF", align: "center"});
            y++
        }
        y++;
        rV.text("Energy", x, y, {color: "#F8F8FF", font: 1, align: "center"});
        y++;
        rV.text(`Surplus ${this.energySurplus}`, x, y, {color: "#F8F8FF", align: "center"});
        y++;
        rV.text(`Logistic ${this.unhandledLogistics}`, x, y, {color: "#F8F8FF", align: "center"})
        y++;
        y++;
        if (this.lab && this.lab.working) {
            rV.text("Lab", x, y, {color: "#F8F8FF", font: 1, align: "center"});
            y++;
            rV.text(this.lab.labTicksLeft, x, y, {color: "#F8F8FF", align: "center"});
            y++;
        }
        y++;
    }

    energyStorage() {
        const colonyRV = this.room.visual;
        for (let source of this.sources) {
            let rV = source.room.visual;
            if (source.container && !source.link) {
                rV.text(source.container.store.getUsedCapacity(RESOURCE_ENERGY), source.container.pos, {color: "#000000", backgroundColor: "#FFFA71"});
            }
        }
        if (this.room.mineral.container) {
            colonyRV.text(this.room.mineral.container.store.getUsedCapacity(this.room.mineral.mineralType), this.room.mineral.container.pos, {color: "#000000", backgroundColor: "#FFFFFF"});
        }
        if (this.rooms.length > 1) {
            for (let room of this.rooms) {
                if (room.name === this.name) {
                    continue;
                }
                let rV = room.visual;
                rV.text(room.controller.reservation.ticksToEnd, room.controller.pos, {color: "#000000", backgroundColor: "#FFFFFF"});
            }
        }
    }

    visuals() {
        this.statusOutput();
        // this.energyStorage();
    }

    factoryLogic() {
        // TODO: Move to class and rebuild based on memory.
        const fn = "Colony.factory";
        if (this.room.factory) {
            if (!this.room.memory.factoryNeeds) {
                this.room.memory.factoryNeeds = {
                    amount: 0,
                    output: 0,
                    state: "available", // available > requested > working > done
                    reaction: null,
                    one: null,
                    two: null,
                }
            }
            var factoryNeeds = this.room.memory.factoryNeeds
            const COMPACT_TOTAL = 24000 // 2x sell overage (12000)
            const resourceToCompact = [RESOURCE_HYDROGEN, RESOURCE_OXYGEN, RESOURCE_UTRIUM, RESOURCE_KEANIUM, RESOURCE_LEMERGIUM, RESOURCE_ZYNTHIUM, RESOURCE_GHODIUM, RESOURCE_CATALYST];
            if (factoryNeeds.state === "available") {
                for (let resource of resourceToCompact) {
                    const onHandAmount = this.resourceStorage(resource);
                    if (onHandAmount > COMPACT_TOTAL) {
                        log.write(log.FACTORY, fn, `${this.name} ${resource} levels high ${onHandAmount}, will compress!`);
                        factoryNeeds.state = "requested";
                        factoryNeeds.amount = 1000;
                        factoryNeeds.output = 200;
                        factoryNeeds.reaction = COMPRESSIONS[resource].reaction;
                        factoryNeeds.one = COMPRESSIONS[resource].one;
                        factoryNeeds.two = COMPRESSIONS[resource].two;
                    }
                }
            } else if (factoryNeeds.state === "requested") {
                if (this.room.factory.store.getUsedCapacity(factoryNeeds.one) >= factoryNeeds.amount && this.room.factory.store.getUsedCapacity(factoryNeeds.two) >= factoryNeeds.amount) {
                    log.write(log.FACTORY, fn, `${this.name} ready to start factory compression ${factoryNeeds.reaction}.`);
                    factoryNeeds.state = "working";
                }
            } else if (this.room.memory.factoryNeeds.state === "working") {
                this.room.factory.produce(factoryNeeds.reaction);
                if (this.room.factory.store.getUsedCapacity(factoryNeeds.one) == 0) {
                    log.write(log.FACTORY, fn, `${this.name} done with factory compression ${factoryNeeds.reaction}.`);
                    factoryNeeds.state = "done";
                }
            }
        }
    }

    get lab() {
        if (!this._lab) {
            if ("lab" in this.room.memory) {
                this._lab = lab.initializeLab(this.room.memory.lab, this);
            } else {
                this.room.memory.lab = {labOne: null, labTwo: null, state: "available", reaction: null, amount: null, boost: {}};
                this._lab = lab.initializeLab({labOne: null, labTwo: null, state: "available", reaction: null, amount: null, boost: {}}, this);
            }
        }
        return this._lab;
    }

    labLogic() {
        const fn = "Colony.labLogic";
        if (this.room.labs.length >= 3) {
            this.lab.run();
        }
    }

    get market() {
        return market.initializeMarket(this, this.room.terminal);
    }

    terminalLogic() {
        if (this.room.controller.level >= 6 && this.room.terminal) {
            this.market.run();
        }
    }

    resourceStorage(resourceRequest) {
        let total = 0;
        let storageLocations = [this.room.storage, this.room.terminal, ...this.creeps];
        // Think considering outputLabs storage causes a bug with filler
        // let storageLocations = [this.room.storage, this.room.terminal, ...this.lab.outputLabs, ...this.creeps];
        for (let location of storageLocations) {
            for (let resourceType in location.store) {
                if (resourceType === resourceRequest) {
                    total += location.store.getUsedCapacity(resourceType);
                }
            }
        }
        return total;
    }

    defenseLogic() {
        const fn = "Colony.defenseLogic";
        // TODO: If enemy is in main room and safe mode is on, ignore.
        this.attacked = false;
        for (let roomName in Memory.rooms) {
            let roomMemory = Memory.rooms[roomName]
            if (roomMemory.attacked) {
                this.attacked = true;
                break;
            }
        }
        if (this.hostileCreeps.length > 0 || this.attacked) {
            for (let creep of this.hostileCreeps) {
                log.write(log.ERROR, fn, `Creep ${creep.name} has ${creep.attackAmount} attack power!`);
                log.write(log.ERROR, fn, `Creep ${creep.name} has ${creep.healAmount} heal power!`);
            }
            let bruisers = this.getCreepsByRole(ROLE_BRUISER);
            if (bruisers.length < 1) {
                this.requestCreepSpawn(ROLE_BRUISER, SPAWN_PRIORITY.BRUISER_CRITICAL, true);
            }
        } else if (this.invaderCore) {
            let bruisers = this.getCreepsByRole(ROLE_BRUISER);
            if (bruisers.length < 1) {
                this.requestCreepSpawn(ROLE_BRUISER, SPAWN_PRIORITY.BRUISER_CRITICAL, true);
            }
        }
    }

    linkLogic() {
        if (this.room.spawnLink && this.room.spawnLink.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
            for (let source of this.room.sources) {
                if (source.link && source.link.store.getUsedCapacity(RESOURCE_ENERGY) >= 400) {
                    if (this.room.upgradeLink && this.room.upgradeLink.store.getFreeCapacity(RESOURCE_ENERGY) > 400) {
                        source.link.transferEnergy(this.room.upgradeLink);
                    } else {
                        source.link.transferEnergy(this.room.spawnLink);
                    }
                }
            }
        }
    }

    roadLogic() {
        if (this.room.controller.level >= 4 && this.room.storage) {
            this.room.verifyTransitRoad(this);
            for (let outpost of this.outposts) {
                outpost.verifyOutpostRoads();
            }
        }
    }

    towerLogic() {
        const fn = "Colony.towerLogic";
        let scouts = _.filter(this.room.enemyCreeps, (c) => c.body.length === 1);
        let hostileCreeps = this.room.hostileCreeps;
        let hurtCreeps = _.filter(this.creeps, (creep) => creep.hits < creep.hitsMax);
        let roadRepairs = this.room.roadRepairSites.sort(function(a,b) { return a.hits < b.hits ? -1 : 1});
        let repairSites = this.room.repairSites;
        let barrierRepairsStandard = _.filter(this.room.barrierRepairSites, (b) => b.hits < this.room.barrierHealthPerRCL);
        let barrierRepairsCritical = _.filter(this.room.barrierRepairSites, (b) => b.hits < 5000);
        let workers = this.getCreepsByRole(ROLE_WORKER);
        for (let tower of this.room.towers) {
            if (hostileCreeps.length > 0) {
                let target = tower.pos.findClosestByRange(hostileCreeps);
                if (!this.room.controller.safeMode) {
                    tower.attack(target);
                }
            } else if (barrierRepairsCritical.length > 0) {
                let target = tower.pos.findClosestByRange(barrierRepairsCritical);
                tower.repair(target);
            } else if (hurtCreeps.length > 0) {
                let target = tower.pos.findClosestByRange(hurtCreeps);
                tower.heal(target);
            } else if (roadRepairs.length > 0) {
                // tower.repair(roadRepairs[Math.floor(Math.random() * roadRepairs.length)]);
                let roadRepairsTotal = _.sum(this.roadRepairSites, (s) => s.hitsMax - s.hits);
                let canRepairTotal = workers[0].getActiveBodyparts(CARRY) * CARRY_CAPACITY * REPAIR_POWER * 0.75;
                for (const rR of roadRepairs) {
                    if (rR.hits < 2500) {
                        log.write(log.NOTIFY, fn, `Tower in ${this.name} repairing roads!`);
                        tower.repair(rR);
                    } else if (tower.pos.inRangeTo(rR, 5) && rR.hits <= 4200) {
                        tower.repair(rR);
                    } else if (roadRepairsTotal > (canRepairTotal * 2)) {
                        // tower.repair(rR);
                    }
                }
            } else if (workers.length < 1 && repairSites.length > 0) {
                if (this.storage) {
                    tower.repair(repairSites[0]);
                }
            } else if (this.room.controller.level < 4 || (this.room.storage && this.room.storage.store.getUsedCapacity(RESOURCE_ENERGY) > 2000 && barrierRepairsStandard.length > 0)) {
                barrierRepairsStandard.sort(function(a, b) {
                    return (a.hits < b.hits ? -1: 1)
                });
                for (let bRS of barrierRepairsStandard) {
                    if (this.storage && tower.pos.inRangeTo(bRS, 5)) {
                        tower.repair(bRS);
                        break;
                    }
                }
            } else if (scouts.length > 0) {
                let target = tower.pos.findClosestByRange(scouts);
                tower.attack(target);
            }
        }
    }

    buildCheck() {
        for (let i = 1; i <= this.room.controller.level; i++) {
            for (let structure in base.LAYOUT[`${i}`]) {
                for (let pos of base.LAYOUT[`${i}`][structure]) {
                    this.room.verifyBuild(structure, pos.x, pos.y);
                }
            }
        }
        if (this.room.controller.level >= 6 && this.room.mineral) {
            this.room.verifyBuild(STRUCTURE_EXTRACTOR, this.room.mineral.pos.x, this.room.mineral.pos.y, false);
            this.room.mineral.buildContainer();
        }
    }

    generateCreepName(role) {
        // return `${role}:${this.name}:${utilities.randomHex(2)}`;
        return `${role}${Game.time}`;
    }

    spawnLogic() {
        this.fillerSpawnLogic();
        this.harvesterSpawnLogic();
        this.minerSpawnLogic();
        this.scoutSpawnLogic();
        this.transportSpawnLogic();
        this.upgraderSpawnLogic();
        this.workerSpawnLogic();
        this.spawnQueue.checkToSpawnCreep();
    }

    get energySurplus() {
        var surplus = 0;
        if (this.room.storage) {
            surplus = -5000; // SAVINGS BUFFER
            surplus += this.storage.store.getUsedCapacity(RESOURCE_ENERGY) // Storage Energy
            surplus -= this.room.savingsPerRCL; // Subtract what we need for next RCL
        }

        return surplus;
    }

    get logistics() {
        return this.droppedEnergyTotal + this.sourceContainerStorage;
    }

    get spawnLink() {
        return this.room.spawnLink;
    }

    get invaderCore() {
        //invaderCore?
        this._invaderCore = null;
        for (let room of this.rooms) {
            let iC = room.find(FIND_HOSTILE_STRUCTURES, (s) => {s.structureType === STRUCTURE_INVADER_CORE});
            if (iC.length > 0) {
                this._invaderCore = iC[0];
            }
        }
        return this._invaderCore;
    }

    get enemyCreeps() {
        const fn = "Colony.enemyCreeps";
        this._enemyCreeps = [];
        for (let room of this.rooms) {
            if (room.enemyCreeps.length > 0) {
                log.write(log.INFO, fn, `${room.name} attacked, enemies ${room.enemyCreeps.length}`);
                for (let enemy of room.enemyCreeps) {
                    this._enemyCreeps.push(enemy);
                }
            }
        }
        return this._enemyCreeps;
    }

    get hostileCreeps() {
        const fn = "Colony.hostileCreeps";
        this._hostileCreeps = [];
        for (let room of this.rooms) {
            if (room.hostileCreeps.length > 0) {
                log.write(log.INFO, fn, `${room.name} attacked, enemies ${room.hostileCreeps.length}`);
                room.memory.attacked = true;
                for (let enemy of room.hostileCreeps) {
                    this._hostileCreeps.push(enemy);
                }
            } else {
                room.memory.attacked = false;
            }
        }
        return this._hostileCreeps;
    }

    get spawnEnergyAvailable() {
        let energy = 0;
        _.forEach(this.spawns, function(spawn) {
            if (spawn.isActive()) {
                energy += spawn.store.getUsedCapacity(RESOURCE_ENERGY);
            }
        });
        _.forEach(this.room.extensions, function(extension) {
            if (extension.isActive()) {
                energy += extension.store.getUsedCapacity(RESOURCE_ENERGY);
            }
        });
        return energy;
    }

    get spawnEnergyMax() {
        let energy = 0;
        _.forEach(this.spawns, function(spawn) {
            if (spawn.isActive()) {
                energy += spawn.store.getCapacity(RESOURCE_ENERGY);
            }
        });
        _.forEach(this.room.extensions, function(extension) {
            if (extension.isActive()) {
                energy += extension.store.getCapacity(RESOURCE_ENERGY);
            }
        });
        return energy;
    }

    get storage() {
        return this.room.storage;
    }

    get repairSites() {
        this._repairSites = [];
        for (let room of this.rooms) {
            for (let rS of room.repairSites) {
                this._repairSites.push(rS);
            }
        }
        return this._repairSites;
    }

    get roadRepairSites() {
        if (!this._roadRepairSites) {
            this._roadRepairSites = [];
            for (let room of this.rooms) {
                for (let rRS of room.roadRepairSites) {
                    this._roadRepairSites.push(rRS);
                }
            }
        }
        return this._roadRepairSites;
    }

    get constructionSites() {
        this._constructionSites = [];
        for (let room of this.rooms) {
            for (let cS of room.constructionSites) {
                this._constructionSites.push(cS);
            }
        }
        return this._constructionSites;
    }

    droppedEnergy() {
        this._droppedEnergy = [];
        for (let room of this.rooms) {
            for (let dE of room.droppedEnergy) {
                this._droppedEnergy.push(dE);
            }
        }
        return this._droppedEnergy;
    }

    get droppedEnergyTotal() {
        this._droppedEnergyTotal = 0;
        for (let room of this.rooms) {
            for (let dE of room.droppedEnergy) {
                this._droppedEnergyTotal += dE.amount;
            }
        }
        return this._droppedEnergyTotal;
    }

    get sources() {
        this._sources = [];
        for (let room of this.rooms) {
            for (let source of room.sources) {
                this._sources.push(source);
            }
        }
        return this._sources;
    }

    get sourceContainerStorage() {
        this._sourceContainerStorage = 0;
        for (let room of this.rooms) {
            this._sourceContainerStorage += room.sourceContainerStorage;
        }
        return this._sourceContainerStorage;
    }

    get tombstones() {
        this._tombstones = [];
        for (let room of this.rooms) {
            for (let tombstone of room.tombstones) {
                this._tombstones.push(tombstone);
            }
        }
        return this._tombstones;
    }

    getCreepsByRole(role) {
        return this.creepsByRole[role] || [];
    }

    getBodyCost(body) {
        let cost = 0;
        _.forEach(body, function(part) {
            cost += BODYPART_COST[part];
        });
        return cost;
    }

    getBodyCostType(body) {
        let cost = 0;
        _.forEach(body, function(part) {
            cost += BODYPART_COST[part.type];
        });
        return cost;
    }

    creepBodyIdeal(frameCost, body, type = false, maxSize = 0) {
        const fn = "Colony.creepBodyIdeal";
        let spawnBodyCost;
        if (type) {
            spawnBodyCost = this.getBodyCostType(body);
        } else {
            spawnBodyCost = this.getBodyCost(body);
        }
        log.write(log.SPAWN, fn, `${this.name} ideal check. body=${JSON.stringify(body)} frameCost=${frameCost} spawnBodyCost=${spawnBodyCost} spawnEnergyMax=${this.spawnEnergyMax}`);
        if (maxSize === spawnBodyCost) {
            return true;
        } else if (this.spawnEnergyMax === 300 && (this.spawnEnergyMax - frameCost) >= spawnBodyCost) {
            log.write(log.SPAWN, fn, `${this.name} body not ideal`)
            return false;
        } else if ((this.spawnEnergyMax - frameCost) > spawnBodyCost ) {
            log.write(log.SPAWN, fn, `${this.name} body not ideal`)
            return false;
        } else {
            log.write(log.SPAWN, fn, `${this.name} body is ideal`)
            return true;
        }
    }

    buildCreepBody(role, requireMaxEnergy) {
        const fn = "Colony.buildCreepBody";
        const fillers = this.getCreepsByRole(ROLE_FILLER);
        const frameCost = this.getBodyCost(CREEP_BODY[role]);
        let i = 0;
        let energyAvailable = this.spawnEnergyAvailable;
        let creepBody = [];
        log.write(log.SPAWN, fn, `${this.name} starting body generation. frameCost=${frameCost} energyAvailable=${energyAvailable}`)
        while (frameCost <= energyAvailable && i < CREEP_MAX_SIZE[role]) {
            creepBody = creepBody.concat(CREEP_BODY[role]);
            energyAvailable -= frameCost;
            i++;
        }

        log.write(log.SPAWN, fn, `${this.name} finished generating. body=${JSON.stringify(creepBody)}`);
        if (i === CREEP_MAX_SIZE[role]) {
            //
        } else if (fillers.length > 0 && requireMaxEnergy && this.spawnEnergyAvailable < this.spawnEnergyMax && !this.creepBodyIdeal(frameCost, creepBody)) {
            log.write(log.SPAWN, fn, `${this.name} have filler, wait to spawn better creep.`);
            return [];
        }
        return creepBody;
    }


    sortBody(a, b) {
        const PRIORITY = {
            "tough": 0,
            "work": 1,
            "attack": 2,
            "ranged_attack": 3,
            "carry": 4,
            "heal": 5,
            "claim": 6,
            "move": 7,
        }
        return (PRIORITY[a] < PRIORITY[b] ? -1 : 1);
    }


    requestCreepSpawn(role, priority, requireMaxEnergy, spawnOpts = {}) {
        const body = spawnOpts.body ? spawnOpts.body : this.buildCreepBody(role, requireMaxEnergy);
        const opts = spawnOpts.opts ? spawnOpts.opts : {memory: {colony: this.name, working: false, role: role, target: null, task: null}};
        const name = spawnOpts.name ? spawnOpts.name : this.generateCreepName(role);
        this.spawnQueue.addCreepToQueue(body.sort(this.sortBody), name, opts, priority);
    }

    fillerSpawnLogic() {
        const fn = "Colony.fillerSpawnLogic";
        let fillersMax = this.room.controller.level >= 7 ? 2 : 1;
        let fillers = this.getCreepsByRole(ROLE_FILLER);
        if (this.room.labs.length > 0) {
            let labHandler = false;
            for (let filler of fillers) {
                if (!("lab" in filler.memory)) {
                    filler.memory.lab = false;
                }
                if (filler.memory.lab) {
                    labHandler = true;
                    break;
                }
            }
            if (!labHandler && fillers.length > 0) {
                fillers[0].memory.lab = true;
            }
        }
        if (fillers.length === 0) {
            log.write(log.SPAWN, fn, `${this.name} spawn request for filler due to 0 filler present.`);
            this.requestCreepSpawn(ROLE_FILLER, SPAWN_PRIORITY.FILLER_STANDARD, false);
        } else if (fillers.length < fillersMax) {
            this.requestCreepSpawn(ROLE_FILLER, SPAWN_PRIORITY.FILLER_STANDARD, true);
        } else if (fillers.length === 1 && ((this.getBodyCostType(fillers[0].body) !== 1500 && this.spawnEnergyMax >= 1500) || !this.creepBodyIdeal(150, fillers[0].body, true, 1500))) {
            log.write(log.SPAWN, fn, `${this.name} spawn request for filler due to non ideal filler body.`);
            this.requestCreepSpawn(ROLE_FILLER, SPAWN_PRIORITY.FILLER_STANDARD, true);
        } else if (fillers.length === 1 && fillers[0].ticksToLive <= 250 && this.room.controller.level >= 5) {
            log.write(log.SPAWN, fn, `${this.name} spawn request for filler due to filler low ticks to live.`);
            if (this.spawnEnergyAvailable === this.spawnEnergyMax || this.spawnEnergyAvailable >= 1500) {
                this.requestCreepSpawn(ROLE_FILLER, SPAWN_PRIORITY.FILLER_STANDARD, true);
            } else {
                this.requestCreepSpawn(ROLE_FILLER, SPAWN_PRIORITY.FILLER_STANDARD, true);
            }
        }
    }

    get sourceWorkPowerNeeded() {
        const fn = "Colony.sourceWorkPowerNeeded";
        for (let source of this.sources) {
            let neighborSlots = source.pos.availableNeighbors(true).length - source.harvesters.length;
            if ((source.workPower < 6 && source.energyCapacity === 3000 || (source.workPower < 3 && source.energyCapacity === 1500)) && neighborSlots > 0) {
                log.write(log.SPAWN|log.HARVESTER, fn, `${this.name} spawning harvester due to work power needs for ${source}`);
                return source.ref;
            }
        }
    }

    harvesterSpawnLogic() {
        let harvesters = this.getCreepsByRole(ROLE_HARVESTER);
        let fillers = this.getCreepsByRole(ROLE_FILLER);
        let creepBody = [];

        if (this.spawnEnergyMax >= 800 && fillers.length > 0 && harvesters.length > 0 && this.spawnEnergyAvailable >= 800) {
            creepBody = [WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE];
        } else if (fillers.length > 0 && harvesters.length > 0) {
            creepBody = this.buildCreepBody(ROLE_HARVESTER, true);
        } else {
            creepBody = this.buildCreepBody(ROLE_HARVESTER, false);
        }

        if (harvesters.length === 0) {
            this.requestCreepSpawn(ROLE_HARVESTER, SPAWN_PRIORITY.HARVESTER_CRITICAL, true, {body: creepBody});
        } else if (harvesters.length < this.sources.length) { 
            for (let source of this.sources) {
                if (source.harvesters.length === 0) {
                    this.requestCreepSpawn(ROLE_HARVESTER, SPAWN_PRIORITY.HARVESTER_STANDARD, true, {body: creepBody, opts: {memory: {colony: this.name, working: false, role: ROLE_HARVESTER, target: source.ref, task: null}}});
                }
            }
        } else {
            let sourceRef = this.sourceWorkPowerNeeded;
            if (sourceRef) {
                this.requestCreepSpawn(ROLE_HARVESTER, SPAWN_PRIORITY.HARVESTER_LOW, true, {body: creepBody, opts: {memory: {colony: this.name, working: false, role: ROLE_HARVESTER, target: sourceRef, task: null}}});
            }
        }
    }

    minerSpawnLogic() {
        let miners = this.getCreepsByRole(ROLE_MINER);
        if (this.room.extractor && this.room.mineral.mineralAmount > 0 && this.room.mineral.container && miners.length < 1) {
            this.requestCreepSpawn(ROLE_MINER, SPAWN_PRIORITY.MINER_STANDARD, true);
        }
    }

    scoutSpawnLogic() {
        if (this.room.controller.level >= 2) {
            let scouts = this.getCreepsByRole(ROLE_SCOUT);
            let scoutInColonyRoom = _.find(scouts, (s) => s.room.name === this.name);
            if (this.room.controller.level < 8 && scouts.length < 1 && !scoutInColonyRoom) {
                this.requestCreepSpawn(ROLE_SCOUT, SPAWN_PRIORITY.SCOUT_STANDARD, false);
            }
        }
    }

    get transportHaulingCapicity() {
        let transports = this.getCreepsByRole(ROLE_TRANSPORT);
        let haulingCapacity = 0;
        for (let creep of transports) {
            haulingCapacity += creep.store.getCapacity();
        }
        return haulingCapacity;
    }

    get unhandledLogistics() {
        if (!this._unhandledLogistics) {
            this._unhandledLogistics = 0;
            for (let source of this.sources) {
                if (source.container) {
                    let incomingCarry = 0
                    let incomingTransportCreeps = source.container.targetedByRole(ROLE_TRANSPORT);
                    for (let transport of incomingTransportCreeps) {
                        incomingCarry += transport.store.getFreeCapacity();
                    }
                    this._unhandledLogistics += (source.container.store.getUsedCapacity() - incomingCarry);
                }
            }
            for (let energy of this.droppedEnergy(250)) {
                let incomingCarry = 0
                let incomingTransportCreeps = energy.targetedByRole(ROLE_TRANSPORT);
                for (let transport of incomingTransportCreeps) {
                    incomingCarry += transport.store.getFreeCapacity();
                }
                this._unhandledLogistics += (energy.amount - incomingCarry);
    
            }
        }

        return this._unhandledLogistics;
    }

    transportSpawnLogic() {
        let transports = this.getCreepsByRole(ROLE_TRANSPORT);
        let haulingCapacity = this.transportHaulingCapicity;
        let maxTransports = 7;
        // TODO: Simplify this...
        if (this.room.spawnContainer && transports.length < 1) {
            this.requestCreepSpawn(ROLE_TRANSPORT, SPAWN_PRIORITY.TRANSPORT_HIGH, true);
        } else if (this.room.spawnContainer && this.unhandledLogistics > (haulingCapacity * 2) && transports.length < maxTransports) {
            if (this.room.storage) {
                this.requestCreepSpawn(ROLE_TRANSPORT, SPAWN_PRIORITY.TRANSPORT_HIGH, true);
            } else if (!this.room.storage && this.unhandledLogistics > 3000) {
                this.requestCreepSpawn(ROLE_TRANSPORT, SPAWN_PRIORITY.TRANSPORT_STANDARD, true);
            } else {
                this.requestCreepSpawn(ROLE_TRANSPORT, SPAWN_PRIORITY.TRANSPORT_STANDARD, true);
            }
        } else if (this.room.spawnContainer && this.unhandledLogistics > (haulingCapacity * 1.5) && transports.length < maxTransports) {
            this.requestCreepSpawn(ROLE_TRANSPORT, SPAWN_PRIORITY.TRANSPORT_STANDARD, true);
        }
    }

    upgraderSpawnLogic() {
        let upgraders = this.getCreepsByRole(ROLE_UPGRADER);
        if (upgraders.length < 1 && this.room.storage && this.energySurplus >= -10000) {
            this.requestCreepSpawn(ROLE_UPGRADER, SPAWN_PRIORITY.UPGRADER_STANDARD, true);
        }
    }

    workerSpawnLogic() {
        const workers = this.getCreepsByRole(ROLE_WORKER);
        const constructionSites = this.constructionSites;
        const repairSites = this.repairSites;
        const roadRepairsTotal = _.sum(this.roadRepairSites, (s) => s.hitsMax - s.hits);
        const workerMax = 15;
        // 1 worker min or 1 worker per room.
        const workerMin = (this.outposts.length) > 0 ? this.outposts.length + 1 : 1;
        const dropsTotal = this.droppedEnergyTotal;
        const hasSavings = this.energySurplus > 1000;
        // TODO: Simplify this...
        // Have constructions sites
        if (constructionSites.length > 0 && (((!this.storage && this.room.spawnContainerFull) || dropsTotal > 1000 || hasSavings || (this.sourceContainerStorage > 1000 && this.getCreepsByRole(ROLE_TRANSPORT).length === 0) || workers.length === 0) && workers.length < workerMax)) {
            this.requestCreepSpawn(ROLE_WORKER, SPAWN_PRIORITY.WORKER_STANDARD, true);
        // NO CONSTRUCTION Pre Storage
        } else if (constructionSites.length === 0 && workers.length <= 30 && !this.storage && this.room.spawnContainerFull && ((this.room.controllerContainer && this.room.controllerContainer.store.getUsedCapacity(RESOURCE_ENERGY) > 1000) || workers.length < workerMax)) {
            this.requestCreepSpawn(ROLE_WORKER, SPAWN_PRIORITY.WORKER_STANDARD, true);
        // NO CONSTRUCTION huge surplus get a worker spawned
        } else if (constructionSites.length === 0 && this.room.hostileCreeps.length === 0 && ((this.room.controller.level < 8 && this.energySurplus >= 10000) || (this.room.controller.level < 4 && dropsTotal > 6000 && workers.length < workerMax))) {
            this.requestCreepSpawn(ROLE_WORKER, SPAWN_PRIORITY.WORKER_CRITICAL, true);
        // NO CONSTRUCTION utilize over surplus
        } else if (constructionSites.length === 0 && this.storage && hasSavings && workers.length < workerMax) {
            this.requestCreepSpawn(ROLE_WORKER, SPAWN_PRIORITY.WORKER_LOW, true);
        // have to get shit done
        } else if (repairSites.length > 0 && workers.length < workerMin) {
            this.requestCreepSpawn(ROLE_WORKER, SPAWN_PRIORITY.WORKER_CRITICAL, true);
        // spawn as many workers to drop surplus, as long as progress > 25%
        } else if (this.energySurplus > 5000 && workers.length < 20 && this.room.controller.level < 8 && this.room.controller.progress > (this.room.controller.progressTotal * 0.05)) {
            this.requestCreepSpawn(ROLE_WORKER, SPAWN_PRIORITY.WORKER_LOW, true);
        }
    }
}

module.exports = {
    Colony: Colony,
}