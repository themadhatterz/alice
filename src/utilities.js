function deref(ref) { // dereference any object from identifier; see ref in RoomObjects
    return Game.getObjectById(ref) || Game.flags[ref] || Game.creeps[ref] || Game.spawns[ref] || null;
}


function derefRoomPosition(protoPos) {
    return new RoomPosition(protoPos.x, protoPos.y, protoPos.roomName);
}


function isEnergyStructure(structure) {
    return structure.energy != undefined && structure.energyCapacity != undefined;
}


function isStoreStructure(structure) {
    return structure.store != undefined;
}


function randomHex(length) {
    const hexChars = "0123456789abcdef";
    let result = "";
    for (let i = 0; i < length; i++) {
        result += hexChars[Math.floor(Math.random() * hexChars.length)];
    }
    return result;
}


function hasPos(object) {
    return object.pos != undefined;
}


function ticksToTime(ticks) {
    let totalSeconds = ticks * AVG_TICK_DURATION;
    let hours = Math.floor(totalSeconds/3600);
    let minutes = Math.floor((totalSeconds % 3600)/60);
    let seconds = Math.floor((totalSeconds % 3600) % 60);
    return `${hours}h${minutes}m${seconds}s`;
}


module.exports = {
    deref: deref,
    derefRoomPosition: derefRoomPosition,
    isEnergyStructure: isEnergyStructure,
    isStoreStructure: isStoreStructure,
    randomHex: randomHex,
    hasPos: hasPos,
    ticksToTime: ticksToTime,
}