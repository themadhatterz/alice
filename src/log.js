const NOTIFY = 1 << 0;
const ERROR = 1 << 1;
const INFO = 1 << 2;
const COLONY = 1 << 3;
const CREEP = 1 << 4;
const TASK = 1 << 5;
const ATTACKER = 1 << 6;
const BRUISER = 1 << 7;
const FILLER = 1 << 8;
const HARVESTER = 1 << 9;
const HEALER = 1 << 10;
const MINER = 1 << 11;
const RESERVER = 1 << 12;
const SCOUT = 1 << 13;
const TRANSPORT = 1 << 14;
const UPGRADER = 1 << 15;
const WORKER = 1 << 16;
const GOTO = 1 << 17;
const SPAWN = 1 << 18;
const LAB = 1 << 19;
const VISUAL = 1 << 20;
const FACTORY = 1 << 21;
const ROAD = 1 << 22;
const PROFILE = 1 << 23;
const MARKET = 1 << 24;
const OUTPOST = 1 << 25;

const LOG_FLAG_STR_TO_INT = {
    "notify": NOTIFY,
    "error": ERROR,
    "info": INFO,
    "colony": COLONY,
    "creep": CREEP,
    "task": TASK,
    "attacker": ATTACKER,
    "bruiser": BRUISER,
    "filler": FILLER,
    "harvester": HARVESTER,
    "healer": HEALER,
    "miner": MINER,
    "reserver": RESERVER,
    "scout": SCOUT,
    "transport": TRANSPORT,
    "upgrader": UPGRADER,
    "worker": WORKER,
    "goto": GOTO,
    "spawn": SPAWN,
    "lab": LAB,
    "visual": VISUAL,
    "factory": FACTORY,
    "road": ROAD,
    "profile": PROFILE,
    "market": MARKET,
    "outpost": OUTPOST,
}


if (!(Memory.logFlags)) {
    Memory.logFlags = NOTIFY | ERROR | INFO;
}


function write(logFlag, fn, message) {
    const msg = `${fn}: ${message}`;
    if (logFlag & NOTIFY) {
        Game.notify(msg);
    }

    if (logFlag & Memory.logFlags) {
        console.log(msg);
    }
}


function flagOn(logFlag) {
    return logFlag && Memory.logFlags;
}


function toggleLog(strFlag) {
    let intFlag = LOG_FLAG_STR_TO_INT[strFlag];
    let enabledFlags = [];
    if (Memory.logFlags & intFlag) {
        Memory.logFlags -= intFlag;
    } else {
        Memory.logFlags += intFlag;
    }

    for (let [sF, iF] of Object.entries(LOG_FLAG_STR_TO_INT)) {
        if (Memory.logFlags & iF) {
            enabledFlags.push(sF)
        }
    }

    return enabledFlags;
}


module.exports = {
    flagOn: flagOn,
    toggleLog: toggleLog,
    write: write,
    LOG_FLAG_STR_TO_INT: LOG_FLAG_STR_TO_INT,
    NOTIFY: NOTIFY,
    ERROR: ERROR,
    INFO: INFO,
    COLONY: COLONY,
    CREEP: CREEP,
    TASK: TASK,
    ATTACKER: ATTACKER,
    BRUISER: BRUISER,
    FILLER: FILLER,
    HARVESTER: HARVESTER,
    HEALER: HEALER,
    MINER: MINER,
    RESERVER: RESERVER,
    SCOUT: SCOUT,
    TRANSPORT: TRANSPORT,
    UPGRADER: UPGRADER,
    WORKER: WORKER,
    GOTO: GOTO,
    SPAWN: SPAWN,
    LAB: LAB,
    VISUAL: VISUAL,
    FACTORY: FACTORY,
    ROAD: ROAD,
    PROFILE: PROFILE,
    MARKET: MARKET,
    OUTPOST: OUTPOST,
}