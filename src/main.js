const prototypes = require("./prototypes");
const colony = require("./colony");
const globals = require("./globals");
const log = require("./log");
const stats = require("./stats");
// const profiler = require("./screeps-profiler");

// profiler.enable();


function generatePixels() {
    const fn = "generatePixel";
    // Clear negative state for consideration of pixel to be generated.
    if (generatePixel === -1) {
        generatePixel = 0;
    }
    if (!("generatePixel" in Memory) && Game.cpu.bucket === 10000 && generatePixel === 1) {
        Game.cpu.generatePixel();
        log.write(log.INFO, fn, "Generating pixel.");
    }
}


function main() {
    const fn = "main";
    // Global Variable Check
    if (!("USERNAME" in global)) {
        log.write(log.INFO, fn, `Loading Globals`);
        globals.loadGlobals();
    }
    globals.calculateAverageTick();
    generatePixels();

    // Build Colony Classes
    Game.colonies = [];
    _.forEach(Game.rooms, function(room) {
        if (room.controller && room.controller.my) {
            log.write(log.COLONY, fn, `Initializing colony ${room.name}`);
            let newColony = new colony.Colony(room);
            Game.colonies.push(newColony);
        }
    });

    _.forEach(Game.colonies, function(c) {
        log.write(log.COLONY, fn, `Executing colony ${c.name}`);
        c.run();
    });

    // Execute Creep Roles
    _.forEach(Game.creeps, function(creep) {
        if (creep.isIdle) {
            log.write(log.CREEP, fn, `${creep.name} is idle, execute handler.`)
            creep.runFunction();
        } else {
            log.write(log.CREEP, fn, `${creep.name} already has a ${creep.task.taskName}`);
        }
    });

    // Execute Tasks
    _.forEach(Game.creeps, function(creep) {
        log.write(log.CREEP, fn, `${creep.name} starting task run function.`);
        if (creep.task) {
            log.write(log.CREEP, fn, `${creep.name} has a ${creep.task.taskName} and starting now.`);
            let rc = null;
            try {
                rc = creep.run();
            } catch(e) {
                log.write(log.NOTIFY|log.ERROR|log.CREEP, fn, `${creep.name} CREEP EXECUTION FAILURE:\n${e}`);
            }
            if (rc !== null) {
                log.write(log.CREEP, fn, `${creep.name} finished task with rc=${rc}`);
            }
        }
    });

    // Flag Cleanup
    _.forEach(Memory.flags, function(flagMemory, flagName) {
        if (!Game.flags[flagName]) {
            delete Memory.flags[flagName];
            log.write(log.INFO, fn, `Lowering ${flagName}.`);
        }
    });

    // Creep Memory Cleanup
    _.forEach(Memory.creeps, function(creepMemory, creepName) {
        if (!Game.creeps[creepName]) {
            delete Memory.creeps[creepName];
        }
    });

    stats.exportStats();
}


log.write(log.INFO, "global", `Script Reloaded`);
globals.loadGlobals(); // Force load of globals, could be new ones added
module.exports.loop = function () {
    // profiler.wrap(function() {
    //     main();
    // })
    main();
}