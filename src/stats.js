function exportStats() {
    if (!Memory.collectStats || Game.time % 15 > 0 ){
        return;
    }

    // Reset stats object
    Memory.stats = {
      gcl: {},
      rooms: {},
      cpu: {},
      creeps: {},
    };

    Memory.stats.time = Game.time;
    Memory.stats.credits = Game.market.credits;

    // Collect room stats
    for (let roomName in Game.rooms) {
        let room = Game.rooms[roomName];
        let isMyRoom = (room.controller ? room.controller.my : false);
        if (isMyRoom) {
            let roomStats = Memory.stats.rooms[roomName] = {};
            roomStats.storageEnergy           = (room.storage ? room.storage.store.energy : 0);
            roomStats.terminalEnergy          = (room.terminal ? room.terminal.store.energy : 0);
            roomStats.energyAvailable         = room.energyAvailable;
            roomStats.energyCapacityAvailable = room.energyCapacityAvailable;
            roomStats.controllerProgress      = room.controller.progress;
            roomStats.controllerProgressTotal = room.controller.progressTotal;
            roomStats.controllerLevel         = room.controller.level;
        }
    }

    // Collect GCL stats
    Memory.stats.gcl.progress      = Game.gcl.progress;
    Memory.stats.gcl.progressTotal = Game.gcl.progressTotal;
    Memory.stats.gcl.level         = Game.gcl.level;

    // Collect CPU stats
    Memory.stats.cpu.bucket        = Game.cpu.bucket;
    Memory.stats.cpu.limit         = Game.cpu.limit;
    Memory.stats.cpu.used          = Game.cpu.getUsed();

    // Creep Stats
    Memory.stats.creeps.count      = Object.keys(Game.creeps).length;
    Memory.stats.creeps.all        = {};
    for (let creepName in Game.creeps) {
        let creep = Game.creeps[creepName];
        if (!(creep.role in Memory.stats.creeps.all)) {
            Memory.stats.creeps.all[creep.role] = 0;
        }
        Memory.stats.creeps.all[creep.role] += 1;
    }
}

module.exports = {
    exportStats: exportStats,
};