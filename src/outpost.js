const log = require("./log");


function initializeOutpost(protoOutpost) {
    const name = protoOutpost.name;
    const colony = _.find(Game.colonies, (c) => c.name === protoOutpost.colony);
    return new Outpost(name, colony);
}


class Outpost {
    constructor(name, colony) {
        this.name = name;
        this.colony = colony;
        this.room = Game.rooms[name]; // will be undefined if no vision
    }

    get canSpawnClaimReserver() {
        return this.colony.spawnEnergyMax >= 1250;
    }

    verifyOutpostRoads() {
        if (this.room) {
            this.room.verifyOutpostRoads(this.colony);
        } else {
            // No vision ask to check rebuild next tick.
            this.colony.room.memory.shouldBuild = true;
        }
    }

    verifyReserved() {
        const fn = "Outpost.verifyReserved";
        let reserver = _.find(this.colony.getCreepsByRole(ROLE_RESERVER), (creep) => creep.memory.target === this.name);
        log.write(log.OUTPOST, fn, `${this.name} outpost of ${this.colony.name} found ${reserver} for reserver.`);
        if (!reserver) {
            const creepMemory = {memory: {colony: this.colony.name, working: false, role: ROLE_RESERVER, target: this.name, task: null}};
            let creepBody = [];
            if (this.canSpawnClaimReserver) {
                if (this.room) {
                    // if I can see the room see if we need claim parts
                    if (this.room.controller.reservation && this.room.controller.reservation.ticksToEnd >= 2000) {
                        creepBody = [MOVE];
                    } else {
                        creepBody = [MOVE, CLAIM, CLAIM];
                    }
                } else {
                    creepBody = [MOVE, CLAIM, CLAIM];
                }
            } else {
                creepBody = [MOVE];
            }
            this.colony.requestCreepSpawn(ROLE_RESERVER, SPAWN_PRIORITY.RESERVER_STANDARD, true, {body: creepBody, opts: creepMemory});
        }
    }

    run() {
        const fn = "Outpost.run";
        this.verifyReserved();
    }
}


module.exports = {
    initializeOutpost: initializeOutpost
}