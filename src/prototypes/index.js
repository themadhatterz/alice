let files = {
    roles: require("./creepRoles"),
    creep: require("./creep"),
    mineral: require("./mineral"),
    room: require("./room"),
    roomobject: require("./roomobject"),
    roomposition: require("./roomposition"),
    source: require("./source"),
    store: require("./store"),
}