const base = require("../base");
const log = require("../log");


const multipleList = [
    STRUCTURE_SPAWN, STRUCTURE_EXTENSION, STRUCTURE_ROAD, STRUCTURE_WALL,
    STRUCTURE_RAMPART, STRUCTURE_KEEPER_LAIR, STRUCTURE_PORTAL, STRUCTURE_LINK,
    STRUCTURE_TOWER, STRUCTURE_LAB, STRUCTURE_CONTAINER, STRUCTURE_POWER_BANK, STRUCTURE_RAMPART,
];


const singleList = [
    STRUCTURE_OBSERVER, STRUCTURE_POWER_SPAWN, STRUCTURE_EXTRACTOR, STRUCTURE_NUKER, STRUCTURE_FACTORY,
    // STRUCTURE_TERMINAL,   STRUCTURE_CONTROLLER,   STRUCTURE_STORAGE,
];


multipleList.forEach(function(type) {
    Object.defineProperty(Room.prototype, `${type}s`, {
        get: function() {
            if (!this[`_${type}s`]) {
                this[`_${type}s`] = this.find(FIND_STRUCTURES, {
                    filter: function(structure) {
                        return structure.structureType === type;
                    }
                });
            }
            return this[`_${type}s`];
        },
        configurable: true,
    });
});


singleList.forEach(function(type) {
    Object.defineProperty(Room.prototype, `${type}`, {
        get: function() {
            if (!this[`_${type}`]) {
                this[`_${type}`] = this.find(FIND_STRUCTURES, {
                    filter: function(structure) {
                        return structure.structureType === type;
                    }
                })[0];
            }
            return this[`_${type}`];
        },
        configurable: true,
    });
});


Object.defineProperty(Room.prototype, "visual", {
    get: function() {
        if (!this._visual) {
            this._visual = new RoomVisual(this.name);
        }
        return this._visual;
    }
});


Object.defineProperty(Room.prototype, "controllerProgressPercentage", {
    get: function () {
        if (!this._controllerProgressPercentage) {
            this._controllerProgressPercentage = (this.controller.progress / this.controller.progressTotal)
        }
        return this._controllerProgressPercentage;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "savingsPerRCL", {
    get: function() {
        if (!this._savingsPerRCL) {
            let nextBuildCost = 0;
            switch(this.controller.level) {
                // Only used once we have storage built right now.
                case 1:
                    nextBuildCost = 0;
                    break;
                case 2:
                    nextBuildCost = 0;
                    break;
                case 3:
                    nextBuildCost = 0;
                    break;
                case 4:
                    nextBuildCost = 35000 + 125000 + 50000; // new ramparts + little extra just because
                    break;
                case 5:
                    nextBuildCost = 300000 + 212500 + 50000;
                    break;
                case 6:
                    nextBuildCost = 305000 + 212500 + 50000;
                    break;
                case 7:
                    nextBuildCost = 468000 + 262500 + 50000;
                    break;
                case 8:
                    nextBuildCost = 0;
                    break;
            }
            this._savingsPerRCL = Math.floor(nextBuildCost * this.controllerProgressPercentage);
        }
        return this._savingsPerRCL;;
    },
    configurable: true,
});

Object.defineProperty(Room.prototype, "averageBarrierHealth", {
    get: function() {
        if (!this._averageBarrierHealth) {
            this._averageBarrierHealth = 0;
            for (let rampart of this.ramparts) {
                this._averageBarrierHealth += rampart.hits;
            }
            this._averageBarrierHealth = (this._averageBarrierHealth / this.ramparts.length)
            if (!this._averageBarrierHealth) {
                this._averageBarrierHealth = 0;
            }
        }
        return this._averageBarrierHealth;
    },
    configurable: false
});

Object.defineProperty(Room.prototype, "barrierHealthPerRCL", {
   get: function() {
        if (!this._barrierHealthPerRCL) {
            switch(this.controller.level) {
                case 1:
                    this._barrierHealthPerRCL = 5000 + Math.floor(2000 * this.controllerProgressPercentage);
                    break;
                case 2:
                    this._barrierHealthPerRCL = 7000 + Math.floor(3000 * this.controllerProgressPercentage);
                    break;
                case 3:
                    this._barrierHealthPerRCL = 10000 + Math.floor(90000 * this.controllerProgressPercentage);
                    break;
                case 4:
                    this._barrierHealthPerRCL = 100000 + Math.floor(150000 * this.controllerProgressPercentage);
                    break;
                case 5:
                    this._barrierHealthPerRCL = 250000 + Math.floor(750000 * this.controllerProgressPercentage);
                    break;
                case 6:
                    this._barrierHealthPerRCL = 1000000 + Math.floor(4000000 * this.controllerProgressPercentage);
                    break;
                case 7:
                    this._barrierHealthPerRCL = 5000000 + Math.floor(10000000 * this.controllerProgressPercentage);
                    break;
                case 8:
                    this._barrierHealthPerRCL = 15000000;
                    break;
           }
       }
       return this._barrierHealthPerRCL;
   },
   configurable: true,
});


Object.defineProperty(Room.prototype, "barriers", {
    get: function() {
        if (!this._barriers) {
            this._barriers = this.find(FIND_STRUCTURES, {filter: function(s) { return s.structureType === STRUCTURE_RAMPART }});
        }
        return this._barriers;
    }
});


Room.prototype.isBuilt = function(structureType, x, y, anchor = true) {
    if (anchor) {
        x = this.anchor.x + x;
        y = this.anchor.y + y;
    }
    var structuresAt = _.filter(this.lookForAt(LOOK_STRUCTURES, x, y), (structure) => structure.structureType === structureType);
    if (structuresAt.length > 0) {
        return structuresAt[0];
    } else {
        return false;
    }
}


Room.prototype.underConstruction = function(structureType, x, y, anchor = true) {
    if (anchor) {
        x = this.anchor.x + x;
        y = this.anchor.y + y;
    }
    var constructionSites = _.filter(this.lookForAt(LOOK_CONSTRUCTION_SITES, x, y), (structure) => structure.structureType === structureType);
    if (constructionSites.length > 0) {
        return constructionSites[0];
    } else {
        return false;
    }
}


Room.prototype.isBaseRoad = function(x, y) {
    const anchorx = x - this.memory.anchor.x;
    const anchory = y - this.memory.anchor.y;
    const roadPosition = new RoomPosition(x, y, this.name);
    const baseCenterPosition = new RoomPosition(this.memory.anchor.x + -4, this.memory.anchor.y, this.name);
    for (let baseRoad of base.LAYOUT["4"]["road"]) {
        if (baseRoad.x === anchorx && baseRoad.y === anchory) {
            return true;
        }
    }
    if (baseCenterPosition.getRangeTo(roadPosition) <= 4) {
        return true;
    }
}


Room.prototype.buildRoadPath = function(pathRef) {
    for (let pos of this.memory.path[pathRef]) {
        let room = Game.rooms[pos.roomName];
        if (room) {
            if (!room.underConstruction(STRUCTURE_ROAD, pos.x, pos.y, false) && !room.isBuilt(STRUCTURE_ROAD, pos.x, pos.y, false)) {
                room.createConstructionSite(pos.x, pos.y, STRUCTURE_ROAD);
            }
        }
    }
}


Room.prototype.generateRoadPath = function(pathRef, fromPos, toPos, colony) {
    // delete Memory.rooms["W42N47"].path["577b92e90f9d51615fa475bb"]; delete Memory.rooms["W42N47"].path["577b92e90f9d51615fa475bd"]; delete Memory.rooms["W42N47"].path["577b92e90f9d51615fa475bc"];
    // Memory.rooms["W42N48"].shouldBuild = true;
    // Game.rooms["W42N47"].find(FIND_MY_CONSTRUCTION_SITES).forEach(function(cs) {cs.remove()});
    let pathFinderResult = PathFinder.search(
        fromPos, toPos,
        {
            plainCost: 4,
            swampCost: 6,
            roomCallback: function(roomName) {
                const room = Game.rooms[roomName];
                const costs = new PathFinder.CostMatrix;

                const terrain = Game.map.getRoomTerrain(roomName);
                for (let y = 0; y < 50; ++y) {
                    for (let x = 0; x < 50; ++x) {
                        let xy = terrain.get(x, y);
                        if ((x !== 0 && y !== 0 && x !== 49 && y !== 49) && xy === TERRAIN_MASK_WALL) {
                            costs.set(x, y, 50);
                        } else if (xy === TERRAIN_MASK_WALL) {
                            costs.set(x, y, 255);
                        } else if (xy === TERRAIN_MASK_SWAMP) {
                            costs.set(x, y, 6);
                        } else if (xy === 0) { // 0 is plain
                            costs.set(x, y, 4);
                        }
                    }
                }

                // prefer roads
                if (room) {
                    room.find(FIND_STRUCTURES).forEach(function(s) {
                        if (s.structureType === STRUCTURE_ROAD) {
                            costs.set(s.pos.x, s.pos.y, 1);
                            if (roomName === "W42N47" && s.pos.x === 35 && s.pos.y === 13) {
                                console.log("a");
                            }
                        } else if (s.structureType === STRUCTURE_CONTAINER) {
                            costs.set(s.pos.x, s.pos.y, 7);
                        } else if (s.structureType !== STRUCTURE_RAMPART || !s.my) {
                            costs.set(s.pos.x, s.pos.y, 255);
                        }
                    });
                    // prefer roads to be
                    room.find(FIND_MY_CONSTRUCTION_SITES).forEach(function(cS) {
                        if (cS.structureType === STRUCTURE_ROAD) {
                            costs.set(cS.pos.x, cS.pos.y, 1);
                            if (roomName === "W42N47" && cS.pos.x === 35 && cS.pos.y === 13) {
                                console.log("b");
                            }
                        }
                    });
                    // planned roads that might not be built yet
                    if (room.memory.path) {
                        for (let pathref in room.memory.path) {
                            for (let pos of room.memory.path[pathref]) {
                                if (pos.roomName === roomName) {
                                    costs.set(pos.x, pos.y, 1);
                                    if (roomName === "W42N47" && pos.x === 35 && pos.y === 13) {
                                        console.log("c");
                                    }
                                }
                            }
                        }
                    }
                }
                // for (let y = 0; y < 50; ++y) {
                //     for (let x = 0; x < 50; ++x) {
                //         if (roomName == "W42N47" && room) {
                //             let c = costs.get(x, y);
                //             switch(c) {
                //                 case 1:
                //                     room.visual.circle(x, y, {fill: "#00fc2f"});
                //                     break;
                //                 case 4:
                //                     room.visual.circle(x, y, {fill: "#0011fc"});
                //                     break;
                //                 case 6:
                //                     room.visual.circle(x, y, {fill: "#e5ff00"});
                //                     break;
                //                 case 7:
                //                     room.visual.circle(x, y, {fill: "#a052ab"});
                //                     break;
                //                 case 50:
                //                     room.visual.circle(x, y, {fill: "#ff9900"});
                //                     break;
                //                 case 255:
                //                     room.visual.circle(x, y, {fill: "#ff0000"});
                //                     break;
                //                 default:
                //                     console.log(`cost ${c} not colored`);
                //             }
                //         }
                //     }
                // }
                return costs;
            }
        }
    )

    // for (pos of pathFinderResult.path) {
        // console.log(pos);
    // }

    if (!("path" in this.memory)) {
        this.memory.path = {};
    }
    this.memory.path[pathRef] = [];

    for (let pos of pathFinderResult.path) {
        if (this.name === colony.name && this.isBaseRoad(pos.x, pos.y)) {
            break;
        }
        this.memory.path[pathRef].push(pos);
    }
}


Room.prototype.verifyTransitRoad = function(colony) {
    const fn = "Room.verifyTransitRoad";
    if (!("path" in this.memory)) {
        this.memory.path = {};
    }
    
    if (!this.memory.path[this.controller.ref]) {
        log.write(log.ROAD, fn, `${this.name} building controller path for the first time.`);
        this.generateRoadPath(this.controller.ref, this.controller.pos, this.storage.pos, colony);
    }
    this.buildRoadPath(this.controller.ref);

    for (let source of this.sources) {
        if (!this.memory.path[source.ref]) {
            log.write(log.ROAD, fn, `${this.name} building ${source} path for the first time.`);
            this.generateRoadPath(source.ref, source.pos, this.storage.pos, colony);
        }
        this.buildRoadPath(source.ref);
    }

    if (this.extractor) {
        if (!this.memory.path[this.extractor.ref]) {
            log.write(log.ROAD, fn, `${this.name} building ${this.extractor} path for the first time.`);
            this.generateRoadPath(this.extractor.ref, this.extractor.pos, this.storage.pos, colony);
        }
        this.buildRoadPath(this.extractor.ref);
    }
}


Room.prototype.verifyOutpostRoads = function(colony) {
    const fn = "Room.verifyOutpostRoads";
    if (!("path" in this.memory)) {
        this.memory.path = {};
    }

    if (!this.memory.path[this.controller.ref]) {
        log.write(log.ROAD, fn, `${colony.name} building ${this.controller} path for the first time.`);
        this.generateRoadPath(this.controller.ref, this.controller.pos, colony.storage.pos, colony);
    }
    this.buildRoadPath(this.controller.ref);

    for (let source of this.sources) {
        if (!this.memory.path[source.ref]) {
            log.write(log.ROAD, fn, `${colony.name} building ${source} path for the first time.`);
            this.generateRoadPath(source.ref, source.pos, colony.storage.pos, colony);
        }
        this.buildRoadPath(source.ref);
    }
}


Room.prototype.verifyRampart = function(x, y) {
    if (this.controller.level >= 4) {
        let rampartAt = _.filter(this.lookForAt(LOOK_STRUCTURES, x, y), (s) => s.structureType === STRUCTURE_RAMPART);
        if (rampartAt.length === 0) {
            this.createConstructionSite(x, y, STRUCTURE_RAMPART);
        }
    }
}


Room.prototype.verifyBuild = function(structureType, x, y, anchor = true) {
    if (anchor) {
        x = this.anchor.x + x;
        y = this.anchor.y + y;
        this.verifyRampart(this.anchor.x, this.anchor.y);
    }
    let structuresAt = _.filter(this.lookForAt(LOOK_STRUCTURES, x, y), (structure) => structure.structureType === structureType);
    let constructionSitesAt = [];
    for (let cS of this.constructionSites) {
        if (cS.structureType === structureType && cS.pos.x === x && cS.pos.y === y) {
            constructionSitesAt.push(cS);
        }
    }
    if (structuresAt.length === 0 && constructionSitesAt.length === 0) {
        if (structureType === STRUCTURE_SPAWN) {
            let spawnNumber = (this.find(FIND_MY_SPAWNS).length + 1);
            var spawnName = `${this.name}_${spawnNumber}`;
        } else {
            var spawnName = null;
        }
        this.createConstructionSite(x, y, structureType, spawnName);
    }
    if (structureType === STRUCTURE_ROAD && this.controller.level < 5) {
        return;
    } else {
        this.verifyRampart(x, y);
    }
}


Room.prototype.verifyFlag = function(x, y, flagType) {
    let flagsAt = _.filter(this.lookForAt(LOOK_FLAGS, x, y), (flag) => flag.memory.type === flagType);
    if (flagsAt.length > 0) {
        return true;
    }
}


Object.defineProperty(Room.prototype, 'logs', {
    get: function() {
        if (!this._logs) {
            this._logs = _.groupBy(this.getEventLog(), e => e.event);
        }
        return this._logs
    },
	configurable: true,
})


Object.defineProperty(Room.prototype, "anchor", {
    get: function() {
        if (!this._anchor) {
            if (!this.memory.anchor) {
                let spawn1 = this.find(FIND_MY_SPAWNS)[0];
                this.memory.anchor = {x: spawn1.pos.x, y: spawn1.pos.y};
            }
            this._anchor = this.memory.anchor;
        }
        return this._anchor;
    }
});


Object.defineProperty(Room.prototype, "mineral", {
    get: function() {
        if (!this._mineral) {
            this._mineral = this.find(FIND_MINERALS)[0];
        }
        return this._mineral;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "sources", {
    get: function() {
        if (!this._sources) {
            this._sources = this.find(FIND_SOURCES);
        }
        return this._sources;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "sourceContainerStorage", {
    get: function() {
        if (!this._sourceContainerStorage) {
            this._sourceContainerStorage = 0;
            for (let source of this.sources) {
                if (source.container) {
                    this._sourceContainerStorage += source.container.store.getUsedCapacity(RESOURCE_ENERGY);
                }
            }
        }
        return this._sourceContainerStorage;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "spawnLink", {
    get: function() {
        if (!this._spawnLink) {
            let spawnLinkPos = base.LAYOUT["6"]["link"][0];
            let spawnLink = this.isBuilt(STRUCTURE_LINK, spawnLinkPos.x, spawnLinkPos.y);
            if (spawnLink) {
                this._spawnLink = spawnLink;
            } else {
                this._spawnLink = null;
            }
        }
        return this._spawnLink;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "spawnContainer", {
    get: function() {
        if (!this._spawnContainer) {
            let spawnContainerPos = base.LAYOUT["2"]["container"][0];
            let spawnContainer = this.isBuilt(STRUCTURE_CONTAINER, spawnContainerPos.x, spawnContainerPos.y);
            if (spawnContainer) {
                this._spawnContainer = spawnContainer;
            } else {
                this._spawnContainer = null;
            }
        }
        return this._spawnContainer;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "spawnContainerFull", {
    get: function() {
        if (!this._spawnContainerFull) {
            if (this.spawnContainer) {
                if (this.spawnContainer.store.getFreeCapacity(RESOURCE_ENERGY) === 0) {
                    this._spawnContainerFull = true;
                } else {
                    this._spawnContainerFull = false;
                }
            } else {
                this._spawnContainerFull = false;
            }
        }
        return this._spawnContainerFull;
    },
    configurable: true,
});


Room.prototype.verifyControllerRamparts = function() {
    for (let aN of this.controller.pos.availableNeighbors(true)) {
        this.verifyRampart(aN.x, aN.y);
    }
}


Object.defineProperty(Room.prototype, "controllerContainerPos", {
    get: function() {
        if (!this._controllerContainerPos) {
            let availableNeighbors = this.controller.pos.availableNeighbors(true);
            if (availableNeighbors.length > 0) {
                this._controllerContainerPos = this.spawns[0].pos.findClosestByRange(availableNeighbors);
            } else {
                this._controllerContainerPos = null;
            }
        }
        return this._controllerContainerPos;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "controllerContainer", {
    get: function() {
        if (!this._controllerContainer) {
            let containers = this.controller.pos.findInRange(FIND_STRUCTURES, 3, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
            let constructionSites = this.controller.pos.findInRange(FIND_MY_CONSTRUCTION_SITES, 3, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
            if (containers.length > 0) {
                this._controllerContainer = containers[0];
                this.verifyControllerRamparts();
            } else if (constructionSites.length === 0) {
                let containerPosition = this.controllerContainerPos;
                if (containerPosition) {
                    this.verifyBuild(STRUCTURE_CONTAINER, containerPosition.x, containerPosition.y, false);
                    this.verifyRampart(containerPosition.x, containerPosition.y);
                    this._controllerContainer = null;
                }
            } else {
                this._controllerContainer = null;
            }
        }
        return this._controllerContainer;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "upgradeLinkPos", {
    get: function() {
        if (!this._upgradeLinkPos) {
            if (this.controllerContainer) {
                let availableNeighbors = this.controllerContainer.pos.availableNeighbors(true);
                for (let neighbor of availableNeighbors) {
                    if (neighbor.inRampart) {
                        this._upgradeLinkPos = neighbor;
                        break;
                    }
                }
                if (!this._upgradeLinkPos) {
                    this._upgradeLinkPos = availableNeighbors[0];
                }
            } else {
                this._upgradeLinkPos = null;
            }
        }
        return this._upgradeLinkPos;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "upgradeLink", {
    get: function() {
        if (!this._upgradeLink) {
            if (this.controllerContainer && this.controller.level >= 7) {
                let links = this.controllerContainer.pos.findInRange(FIND_MY_STRUCTURES, 1, {filter: function(structure) {return structure.structureType === STRUCTURE_LINK}});
                let constructionSites = this.controller.pos.findInRange(FIND_MY_CONSTRUCTION_SITES, 1, {filter: function(structure) {return structure.structureType === STRUCTURE_LINK}});
                if (links.length > 0) {
                    this._upgradeLink = links[0];
                } else if (constructionSites.length === 0) {
                    let linkPos = this.upgradeLinkPos;
                    if (linkPos) {
                        this.verifyBuild(STRUCTURE_LINK, linkPos.x, linkPos.y, false);
                        this._upgradeLink = null;
                    }
                } else {
                    this._upgradeLink = null;
                }
            }
        }
        return this._upgradeLink;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "flags", {
    get: function() {
        if (!this._flags) {
            this._flags = this.find(FIND_FLAGS);
        }
        return this._flags;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "constructionSites", {
    get: function() {
        if (!this._constructionSites) {
            this._constructionSites = this.find(FIND_MY_CONSTRUCTION_SITES);
            this._constructionSites.sort(function(a, b) {
                let aP = BUILD_PRIORITY.indexOf(a.structureType);
                let bP = BUILD_PRIORITY.indexOf(b.structureType);
                return (aP < bP ? -1: 1)
            });
        }
        return this._constructionSites;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "barrierRepairSites", {
    get: function() {
        if (!this._barrierRepairSites) {
            this._barrierRepairSites = _.filter(this.barriers, (b) => b.hits < this.barrierHealthPerRCL);
        }
        return this._barrierRepairSites;
    }
});


Object.defineProperty(Room.prototype, "roadRepairSites", {
    get: function() {
        let DNR = ["62ec169703981433395e2580", "62ec0fa6a837a71c934d2777", "62ec27eac6f4d432d89c49b2", "62ec282da837a7a5fb4d2fab", "62ec287e90a20947c3368ab6", "62ec28ab30aa7e5f640fece4", "62ec28d397421a42dac6d49f", "62ec291e99125fe9a99d539d", "62ec31a72746ebf7837b1ad0"];
        this._repairRoadSites = this.find(FIND_STRUCTURES, {
            filter: function(structure) {
                console.log(structure.id, DNR.indexOf(structure.id));
                return structure.structureType === STRUCTURE_ROAD &&
                        structure.hits < structure.hitsMax && 
                        structure.targetedBy.length === 0 &&
                        !DNR.indexOf(structure.id);
            }
        });
        console.log(this._repairRoadSites.length);
        return this._repairRoadSites;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "repairSites", {
    get: function() {
        if (!this._repairSites) {
            // TODO: Walls/Ramparts
            this._repairSites = this.find(FIND_STRUCTURES, {
                filter: function(structure) {
                    return structure.structureType !== STRUCTURE_WALL &&
                           structure.structureType !== STRUCTURE_RAMPART &&
                           structure.structureType !== STRUCTURE_ROAD &&
                           structure.structureType !== STRUCTURE_INVADER_CORE &&
                           structure.hits < structure.hitsMax;
                }
            });
        }
        return this._repairSites;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "tombstones", {
    get: function() {
        if (!this._tombstones) {
            this._tombstones = this.find(FIND_TOMBSTONES) || [];
        }
        return this._tombstones;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "ruins", {
    get: function() {
        if (!this._ruins) {
            this._ruins = this.find(FIND_RUINS) || [];
        }
        return this._ruins;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "drops", {
    get: function() {
        if (!this._drops) {
            this._drops = _.groupBy(this.find(FIND_DROPPED_RESOURCES), (resource) => resource.resourceType);
        }
        return this._drops;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "droppedEnergy", {
    get: function() {
        return this.drops[RESOURCE_ENERGY] || [];
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "droppedPower", {
    get() {
        return this.drops[RESOURCE_POWER] || [];
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "droppedResource", {
    get: function() {
        if (!this._droppedResource) {
            this._droppedResource = [];
            for (let resource of this.find(FIND_DROPPED_RESOURCES)) {
                if (resource.resourceType !== RESOURCE_ENERGY) {
                    this._droppedResource.push(resource);
                }
            }
        }
        return this._droppedResource;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "fillableStructures", {
    get: function() {
        if (!this._fillableStructures) {
            let fillOptions = this.spawns.concat(this.extensions);
            this._fillableStructures = _.filter(fillOptions, (option) => option.store.getFreeCapacity(RESOURCE_ENERGY) > 0 && option.targetedByRole(ROLE_FILLER).length === 0);
        }
        return this._fillableStructures;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "enemyCreeps", {
    get: function() {
        if (!this._enemyCreeps) {
            this._enemyCreeps = [];
            let allEnemyCreeps = this.find(FIND_HOSTILE_CREEPS);
            for (let creep of allEnemyCreeps) {
                if (ALLIANCE.includes(creep.owner.username)) {
                    continue;
                }
                this._enemyCreeps.push(creep);
            }
        }
        return this._enemyCreeps;
    },
    configurable: true,
});


Object.defineProperty(Room.prototype, "hostileCreeps", {
    get: function() {
        if (!this._hostileCreeps) {
            const attackParts = [ATTACK, RANGED_ATTACK, HEAL, CLAIM, WORK];
            this._hostileCreeps = [];
            for (let creep of this.enemyCreeps) {
                for (let part of creep.body) {
                    if (attackParts.includes(part.type)) {
                        this._hostileCreeps.push(creep);
                        break;
                    }
                }
            }
        }
        return this._hostileCreeps;
    }
});