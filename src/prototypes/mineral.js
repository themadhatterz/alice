Object.defineProperty(Mineral.prototype, "containerPosition", {
    get: function() {
        if (!this._containerPosition) {
            let availableNeighbors = this.pos.availableNeighbors(ignoreCreeps = true);
            let spawn1 = Game.spawns[`${this.room.name}_1`];
            let bestSpot = spawn1.pos.findClosestByPath(availableNeighbors);
            this._containerPosition = bestSpot;
        }
        return this._containerPosition;
    }
});


Object.defineProperty(Mineral.prototype, "container", {
    get: function() {
        if (!this._container) {
            let containers = this.pos.findInRange(FIND_STRUCTURES, 1, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
            if (containers.length > 0) {
                this._container = containers[0];
                this.room.verifyRampart(containers[0].pos.x, containers[0].pos.y);
            } else {
                this._container = null;
            }
        }
        return this._container;
    },
    configurable: true,
});


Mineral.prototype.buildContainer = function() {
    let containers = this.pos.findInRange(FIND_STRUCTURES, 1, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
    let constructionSites = this.pos.findInRange(FIND_MY_CONSTRUCTION_SITES, 1, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
    if (containers.length > 0 || constructionSites.length > 0) {
        // already built
    } else if (constructionSites.length === 0) {
        let containerPosition = this.containerPosition;
        this.room.verifyBuild(STRUCTURE_CONTAINER, containerPosition.x, containerPosition.y, false);
    }
}