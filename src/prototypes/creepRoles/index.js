let roleFiles = {
    attacker: require("./attacker"),
    bruiser: require("./bruiser"),
    filler: require("./filler"),
    harvester: require("./harvester"),
    healer: require("./healer"),
    miner: require("./miner"),
    reserver: require("./reserver"),
    scout: require("./scout"),
    tank: require("./tank"),
    transport: require("./transport"),
    upgrader: require("./upgrader"),
    worker: require("./worker"),
}