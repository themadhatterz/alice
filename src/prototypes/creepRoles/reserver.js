const task = require("../../task");
const utilities = require("../../utilities");


Creep.prototype.reserverRun = function() {
    if (this.room.name !== this.memory.target) {
        this.task = task.Tasks.goToRoom(this.memory.target);
    } else if (this.room.name === this.memory.target) {
        if (this.getActiveBodyparts(CLAIM) > 0) {
            if (!this.room.controller.sign || (this.colony.room.controller.sign.text !== CONTROLLER_SIGNATURE && this.colony.room.controller.sign.text !== SCREEPS_NOVICE_SIGNATURE)) {
                this.task = new task.TaskSignController(this.room.controller);
            } else {
                if (this.room.controller.reservation && this.room.controller.reservation.username && this.room.controller.reservation.username !== USERNAME) {
                    this.task = task.Tasks.attackController(this.room.controller);
                } else {
                    this.task = task.Tasks.reserve(this.room.controller);
                }
            }
        } else {
            this.task = task.Tasks.goTo(this.room.controller);
        }
    }
}

