const log = require("../../log");
const task = require("../../task");


Object.defineProperty(Creep.prototype, "needsToughBoost", {
    get: function() {
        for (let boost in this.boostCounts) {
            if (BOOST_TYPES["tough"].includes(boost)) {
                return false;
            }
        }
        return true;
    },
    configurable: true,
});



Creep.prototype.attackerRun = function() {
    const fn = "Creep.attackerRun";
    if (this.needsToughBoost && this.colony.lab.outputLabAvailable) {
        log.write(log.ATTACKER, fn, `${this.name} needs boosting, check what is available.`);
        let options = this.colony.lab.resourceBoostAvailable("tough");
        for (let [resource, amount] of Object.entries(options)) {
            if ((amount/LAB_BOOST_MINERAL) >= this.getActiveBodyparts(TOUGH)) {
                let lab = this.colony.lab.boostRequest(resource, this.getActiveBodyparts(TOUGH) * LAB_BOOST_MINERAL);
                log.write(log.ATTACKER, fn, `Boosting with ${resource} at ${lab}`);
                this.task = task.Tasks.getBoosted(lab, resource, this.getActiveBodyparts(TOUGH));
                return;
            }
        }
    }
    let flag = _.filter(this.colony.colonyFlags, (flag) => flag.memory.type === "attack")[0];
    if (flag) {
        let healer = this.colony.getCreepsByRole(ROLE_HEALER)[0];
        if (this.room.name !== flag.memory.room) {
            // TODO: fight back in transit in case we are attacked
            // this.task = new task.TaskGoToRoom(flag.memory.room);
            if (healer.room.name === this.room.name && !this.pos.isNearTo(healer)) {
                return;
            } else {
                this.moveTo(flag);
            }
        } else {
            let defenseCreeps = _.filter(this.room.enemyCreeps, function(creep) {
                let attack = creep.getActiveBodyparts(ATTACK);
                let rangedAttack = creep.getActiveBodyparts(RANGED_ATTACK);
                if (attack|| rangedAttack) {
                    return creep;
                }
            });
            let target = null;
            let towersWithEnergy = _.filter(this.room.towers, (tower) => !tower.my && tower.store.getUsedCapacity(RESOURCE_ENERGY) > 0);
            let towersWithoutEnergy = _.filter(this.room.towers, (tower) => !tower.my && tower.store.getUsedCapacity(RESOURCE_ENERGY) === 0);
            let spawns = _.filter(this.room.spawns, (spawn) => !spawn.my);
            let structures = _.filter(this.room.find(FIND_HOSTILE_STRUCTURES), (s) => s.structureType !== STRUCTURE_CONTROLLER);
            if (towersWithEnergy.length > 0) {
                target = this.pos.findClosestByPath(towersWithEnergy);
            } else if (defenseCreeps.length > 0) {
                target = this.pos.findClosestByPath(defenseCreeps);
            } else if (towersWithoutEnergy.length > 0) {
                target = this.pos.findClosestByPath(towersWithoutEnergy);
            } else if (spawns.length > 0) {
                target = this.pos.findClosestByPath(spawns);
            } else if (this.room.enemyCreeps.length > 0) {
                target = this.pos.findClosestByPath(this.room.enemyCreeps);
            } else if (structures.length > 0) {
                target = this.pos.findClosestByPath(structures);
            }
            if (target) {
                this.task = new task.TaskAttack(target);
            }
        }
    }
}


