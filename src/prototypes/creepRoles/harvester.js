const caching = require("../../caching");
const log = require("../../log");
const task = require("../../task");


Creep.prototype.harvesterRun = function() {
    const fn = "Creep.harvesterRun";
    log.write(log.HARVESTER, fn, `${this.name} full=${this.isFull}`);
    // Double check there isn't now a better option.
    if (this.target && this.target.workPower > 6) {
        log.write(log.HARVESTER, fn, `${this.name} double checking for unattended source.`);
        let unattendedSources = _.filter(this.colony.sources, function(source) { return source.harvesters.length === 0})
        if (unattendedSources.length > 0) {
            this.target = this.pos.findClosestTargetByPath(unattendedSources);
            log.write(log.HARVESTER, fn, `${this.name} found better source ${this.target}`);
        }
    }

    if (!this.isFull && (this.getActiveBodyparts(WORK) * 2) <= this.store.getFreeCapacity()) {
        if (this.target) {
            log.write(log.HARVESTER, fn, `${this.name} already has target ${this.target}`);
            // If link, clear container to cut out any transport trips.
            if (this.target.room.name === this.colony.name && this.room.name === this.colony.name && this.room.spawnLink && this.target.link && this.target.link.store.getFreeCapacity(RESOURCE_ENERGY) > 0 && this.target.container && this.target.container.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                log.write(log.HARVESTER, fn, `${this.name} has link but container has energy, empty it.`);
                this.task = task.Tasks.withdraw(this.target.container, RESOURCE_ENERGY);
            } else {
                log.write(log.HARVESTER, fn, `${this.name} begin harvesting ${this.target}`);
                this.task = task.Tasks.harvest(this.target)
            }
        } else {
            let unattendedSources = _.filter(this.colony.sources, function(source) { return source.targetedBy.length === 0})
            if (unattendedSources.length > 0) {
                this.target = this.pos.findClosestTargetByPath(unattendedSources);
                log.write(log.HARVESTER, fn, `${this.name} found unattended sources, ${this.target} is closest by path.`);
                this.task = task.Tasks.harvest(this.target);
            } else {
                log.write(log.HARVESTER, fn, `${this.name} no unattended sources.`);
                for (let source of this.colony.sources) {
                    if (source.workPower < 6 && source.pos.availableNeighbors().length > 0) {
                        log.write(log.HARVESTER, fn, `${this.name} chose ${source} as best option.`);
                        this.target = source;
                        this.task = task.Tasks.harvest(this.target);
                    } else {
                        for (let flag of this.colony.colonyFlags) {
                            if (flag.memory.type === "outpost" && !flag.room) {
                                this.task = task.Tasks.goToRoom(flag.memory.room);
                                return;
                            }
                        }
                        // log.write(log.ERROR|log.HARVESTER, fn, `${this.name} FIX ME`);
                    }
                }
            }
        }
    } else {
        log.write(log.HARVESTER, fn, `${this.name} is full, find something to do with energy.`);

        // Remote harvesters build and repair container.
        if (this.room.name !== this.colony.name) {
            let containerConstructions = this.room.find(FIND_MY_CONSTRUCTION_SITES, {filter: function(s) { return s.structureType === STRUCTURE_CONTAINER}});
            for (let containerConstruction of containerConstructions) {
                if (this.pos.inRangeTo(containerConstruction, 3)) {
                    this.task = task.Tasks.build(containerConstruction);
                    return;
                }
            }
            if (this.target && this.target.container && this.target.container.hits < this.target.container.hitsMax) {
                this.task = task.Tasks.repair(this.target.container);
                return;
            }
        }

        if (this.target && this.target.room.name === this.colony.name && this.room.spawnLink && this.target.link && this.target.link.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
            log.write(log.HARVESTER, fn, `${this.name} put it in the link.`);
            this.task = task.Tasks.transfer(this.target.link, RESOURCE_ENERGY)
        } else if (this.target && this.target.container && this.target.harvesters.length === 1 && !this.pos.isEqualTo(this.target.container)) {
            log.write(log.HARVESTER, fn, `${this.name} is the only harvester here but not on container, reposition.`);
            this.moveTo(this.target.container);
        } else if (this.target && this.target.container && this.target.container.store.getFreeCapacity() > this.store.getUsedCapacity()) {
            log.write(log.HARVESTER, fn, `${this.name} put it in the container.`);
            this.task = task.Tasks.transfer(this.target.container, RESOURCE_ENERGY)
        } else {
            log.write(log.HARVESTER, fn, `${this.name} put it on the floor.`);
            this.task = task.Tasks.drop(this.pos, RESOURCE_ENERGY);
        }
    }

}