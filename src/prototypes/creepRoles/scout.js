const log = require("../../log");
const task = require("../../task");


Creep.prototype.highwayAnalysis = function() {
    const fn = "Creep.highwayAnalysis";
    const deposits = this.room.find(FIND_DEPOSITS);
    if (deposits.length > 0) {
        log.write(log.SCOUT, fn, `${this.name} found commodity in ${this.room.name}`);
        this.room.memory.depositStatus = true;
        this.room.memory.depositDetails = {};
        const depositPath = PathFinder.search(deposits[0].pos, this.colony.room.spawns[0], {maxRooms: 64, maxOps: 8000});
        if (!depositPath.incomplete) {
            this.room.memory.depositDetails.distance = depositPath.path.length;
        } else {
            this.room.memory.depositDetails.distance = 0;
        }
        this.room.memory.depositDetails.time = Game.time;
    } else {
        this.room.memory.depositStatus = false;
        this.room.memory.depositDetails = {
            time: 0,
            distance: 0,
        };
    }

    if (this.room.powerBanks.length > 0) {
        log.write(log.SCOUT, fn, `${this.name} found powerBank in ${this.room.name}`);
        this.room.memory.powerStatus = true;
        this.room.memory.powerDetails = {};
        const powerPath = PathFinder.search(this.room.powerBanks[0].pos, this.colony.room.spawns[0], {maxRooms: 64, maxOps: 8000});
        if (!powerPath.incomplete) {
            this.room.memory.powerDetails.distance = powerPath.path.length;
        } else {
            this.room.memory.powerDetails.distance = 0;
        }
        this.room.memory.powerDetails.time = Game.time;
    } else {
        this.room.memory.powerStatus = false;
        this.room.memory.powerDetails = {
            distance: 0,
            time: 0,
        }
    }
}


Creep.prototype.outpostAnalysis = function() {
    const fn = "Creep.outpostAnalysis";
    if (!this.room.memory.outpost && this.room.controller && !this.room.controller.my && !this.room.controller.owner && !(this.room.controller.reservation && !this.room.controller.reservation.username === USERNAME)) {
        for (let source of this.room.sources) {
            const sourcePathFinder = PathFinder.search(source.pos, {pos: this.colony.room.spawns[0].pos});
            log.write(log.SCOUT, fn, `${this.name} found source in ${this.room.name} length ${sourcePathFinder.path.length}`);
            if (!sourcePathFinder.incomplete && sourcePathFinder.path.length <= 100) {
                log.write(log.SCOUT|log.NOTIFY, fn, `${this.name} found outpost viable room ${this.room.name} length to source ${sourcePathFinder.path.length}`);
                // TODO: Mark room in memory as viable, let colony decide which of found rooms is best.
                this.room.memory.outpost = true;
                this.room.memory.sourcesCount = this.room.sources.length;
                this.room.memory.sourcesDistance = sourcePathFinder.path.length;
                if (this.room.mineral) {
                    this.room.memory.mineral = this.room.mineral.mineralType;
                } else {
                    this.room.memory.mineral = false
                }
                break;
            }
        }
    }
}


Creep.prototype.analyzeRoom = function() {
    const fn = "Creep.analyzeRoom";
    this.room.memory.scoutTime = Game.time;
    if (!(this.room.name in Memory.rooms)) {
        Memory.rooms[this.room.name] = {};
    }
    if (!("exits" in this.room.memory)) {
        this.room.memory.exits = Game.map.describeExits(this.room.name);
    }
    if (this.room.controller && this.room.controller.owner && (this.room.controller.owner !== USERNAME || !ALLIANCE.includes(this.room.controller.owner))) {
        this.room.memory.hostile = true;
    // TODO: Should probably just make goTo and goToRoom avoid invader creeps somehow by overriding move to target in task
    } else if (this.room.keeperLairs.length > 0) {
        this.room.memory.hostile = true;
    } else {
        this.room.memory.hostile = false;
    }

    this.room.memory.hostiles = this.room.hostileCreeps.length;

    const roomRegex = /[EW](\d+)[NS](\d+)/mg;
    const match = roomRegex.exec(this.room.name);
    if (match[1] % 10 === 0 || match[2] % 10 === 0) {
        this.highwayAnalysis();
    } else {
        this.outpostAnalysis();
    }
}


Creep.prototype.scoutRun = function() {
    const fn = "Creep.scoutRun";
    log.write(log.SCOUT, fn, `${this.name} in ${this.room.name}`);
    if (!this.currentRoom) {
        this.currentRoom = this.room.name;
    }

    this.notifyWhenAttacked(false);
    this.analyzeRoom();

    // TODO: This can absoultely still break.
    let oldestReportRoom = null;
    let oldestReportTime = 0;
    for (let [dir, room] of Object.entries(this.room.memory.exits)) {
        let roomPath = Game.map.findRoute(this.room.name, room);
        if (roomPath[roomPath.length - 1].exit != dir) {
            log.write(log.SCOUT, fn, `${this.name} excluding ${room} due to exit blocked.`);
            // exit blocked
            continue;
        }
        if (!(room in Memory.rooms)) {
            Memory.rooms[room] = {scoutTime: -1};
        }
        if (Memory.rooms[room].hostile && (Game.time - Memory.rooms[room].scoutTime) < 30000) {
            // Skip enemy rooms unless we haven't seen them in a while.
            log.write(log.SCOUT, fn, `${this.name} excluding ${room} last known as hostile and visited recently.`);
            continue;
        }
        log.write(log.SCOUT, fn, `${this.name} checking room ${room}`);
        if (!(room in Memory.rooms)) {
            log.write(log.SCOUT, fn, `${this.name} ${room} wasn't in memory.`);
            oldestReportRoom = room;
            oldestReportTime = 1;
        } else if (oldestReportTime === 0 || (Memory.rooms[room].scoutTime < oldestReportTime)) {
            oldestReportRoom = room;
            oldestReportTime = Memory.rooms[room].scoutTime;
            log.write(log.SCOUT, fn, `${this.name} oldestRoom room ${room} time ${oldestReportTime}`);
        }
    }
    if (!oldestReportRoom) {
        log.write(log.SCOUT, fn, `${this.name} no room chosen! ${this.room}`);
        this.task = task.Tasks.goToRoom(this.colony.room.name);
    } else {
        log.write(log.SCOUT, fn, `${this.name} going to ${oldestReportRoom}`);
        this.task = task.Tasks.goToRoom(oldestReportRoom);
    }
    // this.task = task.Tasks.goToRoom("W32N30");
}