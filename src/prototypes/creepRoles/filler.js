const log = require("../../log");
const task = require("../../task");


Object.defineProperty(Creep.prototype, "labNeed", {
    get: function() {
        // if (this.room.memory.labNeeds && this.room.memory.labNeeds.state === "requested" || this.room.memory.labNeeds.state === "done") {
        //     return true;
        // }
        if (this.colony.lab && (this.colony.lab.requested || this.colony.lab.state.done || this.colony.lab.boosting)) {
            return true;
        }

    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "factoryNeed", {
    get: function() {
        if (this.room.memory.factoryNeeds && (this.room.memory.factoryNeeds.state === "requested" || this.room.memory.factoryNeeds.state === "done")) {
            return true;
        }
    },
    configurable: true,
});


Creep.prototype.fillerFactoryNeeds = function() {
    const fn = "Creep.fillerFactoryNeeds";
    if (this.lab && this.factoryNeed) {
        var factoryNeeds = this.room.memory.factoryNeeds;
        log.write(log.FILLER|log.FACTORY, fn, `${this.name} checking for factory needs.`);
        if (this.room.memory.factoryNeeds.state === "done") {
            log.write(log.FILLER|log.FACTORY, fn, `${this.name} emptying ${factoryNeeds.output} of ${factoryNeeds.reaction} from factory.`);
            this.task = task.Tasks.transfer(this.room.terminal, factoryNeeds.reaction, factoryNeeds.output);
            this.task.fork(task.Tasks.withdraw(this.room.factory, factoryNeeds.reaction, factoryNeeds.output));
            this.room.memory.factoryNeeds = {
                amount: 0,
                output: 0,
                state: "available", // available > requested > working > done
                reaction: null,
                one: null,
                two: null,
            }
            return true;
        } else if (this.room.memory.factoryNeeds.state === "requested") {
            if (this.store.getUsedCapacity() > 0) {
                log.write(log.FILLER|log.FACTORY, fn, `${this.name} not enough space for factory request, empty contents to storage.`);
                this.task = task.Tasks.transferAll(this.room.storage);
                return true;
            }
            const oneNeedAmount = factoryNeeds.amount - this.room.factory.store.getUsedCapacity(factoryNeeds.one);
            const twoNeedAmount = factoryNeeds.amount - this.room.factory.store.getUsedCapacity(factoryNeeds.two);
            if (oneNeedAmount > 0 || twoNeedAmount > 0) {
                this.task = task.Tasks.transferAll(this.room.factory);
                if (oneNeedAmount > 0) {
                    this.task.fork(task.Tasks.withdraw(this.room.terminal, factoryNeeds.one, oneNeedAmount));
                    log.write(log.FILLER|log.FACTORY, fn, `${this.name} picking up ${oneNeedAmount} of ${factoryNeeds.one} for factory.`);
                }
                if (twoNeedAmount > 0) {
                    const spaceLeft = this.store.getCapacity() - oneNeedAmount;
                    const twoGetAmount = twoNeedAmount > spaceLeft ? spaceLeft : twoNeedAmount
                    this.task.fork(task.Tasks.withdraw(this.room.terminal, factoryNeeds.two, twoGetAmount));
                    log.write(log.FILLER|log.FACTORY, fn, `${this.name} picking up ${twoGetAmount} of ${factoryNeeds.two} for factory.`);
                }
                return true;
            }
        }
    }
    return false;
}


Creep.prototype.fillerBoostNeeds = function() {
    const fn = "Creep.fillerBoostNeeds";
    if (this.lab && this.colony.lab) {
        log.write(log.FILLER|log.LAB, fn, `${this.name} checking for outputLab boosting needs.`)
        for (let labObj of this.colony.lab.outputLabs) {
            if (labObj.boosting) {
                log.write(log.FILLER|log.LAB, fn, `${this.name} found lab in boosting state.`);
                for (let resource in labObj.lab.store) {
                    if (resource !== labObj.resource && resource !== RESOURCE_ENERGY) {
                        log.write(log.FILLER|log.LAB, fn, `${this.name} found other resources inside ${labObj.lab}, will empty.`);
                        this.task = task.Tasks.transfer(this.colony.room.terminal, resource);
                        this.task.fork(task.Tasks.withdraw(labObj.lab, resource));
                        if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                            this.task.fork(task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY));
                        }
                        return true;
                    }
                }
                log.write(log.FILLER|log.LAB, fn, `${this.name} checking to fill in relevant resource in ${labObj.lab}.`);
                let hasAmount = labObj.lab.store.getUsedCapacity(labObj.resource);
                log.write(log.FILLER|log.LAB, fn, `${this.name} ${labObj.lab} has ${hasAmount} and needs total of ${labObj.amount}`)
                if (hasAmount < labObj.amount) {
                    let amountNeeded = labObj.amount - hasAmount;
                    log.write(log.FILLER|log.LAB, fn, `${this.name} ${labObj.lab} needs ${amountNeeded} of ${labObj.resource}`);
                    this.task = task.Tasks.transfer(labObj.lab, labObj.resource, amountNeeded);
                    this.task.fork(task.Tasks.withdraw(this.colony.room.terminal, labObj.resource, amountNeeded));
                    if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                        this.task.fork(task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY));
                    }
                    return true;
                }
            } else if (labObj.unboosting) {
                // TODO: 
                log.write(log.FILLER|log.LAB, fn, `${this.name} found lab in unboosting state, nothing to do.`);
            }
        }

        if (this.room.droppedResource.length > 0) {
            let target = this.pos.findClosestByPath(this.room.droppedResource);
            if (this.pos.inRangeTo(target, 15)) {
                log.write(log.FILLER|log.LAB, fn, `${this.name} picking up dropped resource ${target}`);
                this.task = task.Tasks.transferAll(this.colony.room.terminal);
                this.task.fork(task.Tasks.pickup(target));
                if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                    this.task.fork(task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY));
                }
                return true;
            }

        }
    }
    return false;
}


Creep.prototype.fillerLabHandling = function() {
    const fn = "Creep.fillerLabHandling";
    if (this.colony.lab && !this.colony.lab.working) {
        log.write(log.FILLER|log.LAB, fn, `${this.name} checking to empty output labs.`);
        const reactionCompounds = [
                                 "ZK", "UL", "G", "OH", // Base 
                                 "UH", "UO", "KH", "KO", "LH", "LO", "ZH", "ZO", "GH", "GO", // T1
                                 "UH2O", "UHO2", "KH2O", "KHO2", "LH2O", "LHO2", "ZH2O", "ZHO2", "GH2O", "GHO2", // T2
                                 "XUH2O", "XUHO2", "XKH2O", "XKHO2", "XLH2O", "XLHO2", "XZH2O", "XZHO2", "XGH2O", "XGHO2" // T3
                                ];
        for (let labObj of this.colony.lab.outputLabs) {
            for (let resource in labObj.lab.store) {
                if (reactionCompounds.includes(resource) && labObj.lab.store.getUsedCapacity(resource) > 0 && this.store.getFreeCapacity() > 0) {
                    log.write(log.FILLER|log.LAB, fn, `${this.name} will empty ${resource} from output labs`);
                    if (!this.task) {
                        this.task = task.Tasks.transfer(this.colony.room.terminal, resource);
                    }
                    this.task.fork(task.Tasks.withdraw(labObj.lab, resource));
                    if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                        this.task.fork(task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY));
                    }
                }
            }
        }

        if (this.task) {
            return true;
        }
    }
    if (this.colony.lab && this.colony.lab.requested) {
        if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            this.task = task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY);
            return;
        }
        log.write(log.FILLER|log.LAB, fn, `${this.name} labs in requested state`);
        let labOne = this.colony.lab.labOne;
        let labTwo = this.colony.lab.labTwo;
        let resourceOne = this.colony.lab.resourceOne;
        let resourceTwo = this.colony.lab.resourceTwo;
        let labOneResourceNeedAmount = this.colony.lab.amount - labOne.store.getUsedCapacity(resourceOne);
        let labTwoResourceNeedAmount = this.colony.lab.amount - labTwo.store.getUsedCapacity(resourceTwo);
        log.write(log.FILLER|log.LAB, fn, `${this.name} labOne=${labOneResourceNeedAmount} resource=${resourceOne}`);
        log.write(log.FILLER|log.LAB, fn, `${this.name} labTwo=${labTwoResourceNeedAmount} resource=${resourceTwo}`);
        // RESOURCE ONE
        if (labOneResourceNeedAmount > 0 && this.store.getUsedCapacity(resourceOne) === 0) {
            log.write(log.FILLER|log.LAB, fn, `${this.name} picking up ${resourceOne} for labOne`);
            if (labOneResourceNeedAmount > this.store.getFreeCapacity()) {
                labOneResourceNeedAmount = this.store.getFreeCapacity();
                log.write(log.FILLER|log.LAB, fn, `${this.name} order for lab one exceeds capacity, reduce to ${labOneResourceNeedAmount}`);
            }
            this.task = task.Tasks.transfer(labOne, resourceOne, labOneResourceNeedAmount);
            this.task.fork(task.Tasks.withdraw(this.colony.room.terminal, resourceOne, labOneResourceNeedAmount));
            if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                this.task.fork(task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY));
            }
            return true;
        } else if (labTwoResourceNeedAmount > 0 && this.store.getUsedCapacity(resourceTwo) === 0) {
            log.write(log.FILLER|log.LAB, fn, `picking up ${resourceTwo} for labTwo`);
            if (labTwoResourceNeedAmount > this.store.getCapacity()) {
                labTwoResourceNeedAmount = this.store.getFreeCapacity();
                log.write(log.FILLER|log.LAB, fn, `${this.name} order for labTwo exceeds capacity, reduce to ${labTwoResourceNeedAmount}`);
            }
            this.task = task.Tasks.transfer(labTwo, resourceTwo, labTwoResourceNeedAmount);
            this.task.fork(task.Tasks.withdraw(this.colony.room.terminal, resourceTwo, labTwoResourceNeedAmount));
            if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                this.task.fork(task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY));
            }
            return true;
        } else {
            log.write(log.ERROR|log.FILLER|log.LAB, fn, `${this.name} lab in request but nothing for filler to do.`);
        }
    }
    return false;
}


Creep.prototype.fillerNotWorking = function() {
    const fn = "Creep.fillerNotWorking";

    if (this.lab) {
        if (this.fillerBoostNeeds() || this.fillerLabHandling() || this.fillerFactoryNeeds()) {
            return;
        }
    }
    let rechargeTarget = this.rechargeTarget();
    if (rechargeTarget) {
        log.write(log.FILLER, fn, `${this.name} found recharge target ${rechargeTarget}.`);
        if (rechargeTarget.structureType === STRUCTURE_CONTAINER || rechargeTarget.structureType === STRUCTURE_STORAGE || rechargeTarget.structureType === STRUCTURE_LINK || rechargeTarget instanceof Ruin || rechargeTarget.deathTime) {
            this.task = new task.TaskWithdraw(rechargeTarget, RESOURCE_ENERGY);
        } else {
            this.task = new task.TaskPickup(rechargeTarget, RESOURCE_ENERGY);
        }
    } else {
        // TODO: maybe this happens?
        if (this.colony.room.terminal && this.colony.room.terminal.store.getUsedCapacity(RESOURCE_ENERGY) >= 10000) {
            this.task = new task.TaskWithdraw(this.colony.room.terminal, RESOURCE_ENERGY);
        } else {
            log.write(log.INFO, fn, `${this.name} did not find a recharge target.`);
        }
        
    }

}


Creep.prototype.fillerWorking = function() {
    const fn = "Creep.fillerWorking";
    if (this.lab) {
        if (this.fillerBoostNeeds() || this.fillerLabHandling() || this.fillerFactoryNeeds()) {
            return;
        }
    }
    let structuresNeeding = this.room.fillableStructures;
    let spawnNeeds = null;
    if (structuresNeeding.length > 0) {
        spawnNeeds = this.pos.findClosestByPath(structuresNeeding);
    }
    let towerUrgent = _.filter(this.room.towers, (tower) => tower.store.getUsedCapacity(RESOURCE_ENERGY) < 100);
    let towerNormal = _.filter(this.room.towers, (tower) => tower.store.getUsedCapacity(RESOURCE_ENERGY) < 800);
    let spawnContainer = this.room.spawnContainer;
    let spawnLinkOverHalf = this.room.spawnLink && this.room.spawnLink.store.getFreeCapacity(RESOURCE_ENERGY) <= 400;
    let factoryNeeds = this.room.factory && this.room.factory.store.getUsedCapacity(RESOURCE_ENERGY) <= 5000;
    let labs = _.filter(this.room.labs, (lab) => lab.store.getFreeCapacity(RESOURCE_ENERGY) > 1000);
    if (towerUrgent.length > 0) {
        this.task = new task.TaskTransfer(this.pos.findClosestByRange(towerUrgent), RESOURCE_ENERGY);
    } else if (labs.length > 0) {
        let lab = this.pos.findClosestByRange(labs);
        this.task = new task.TaskTransfer(lab, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${lab}`);
    } else if (spawnNeeds && !(structuresNeeding.length === 1 && structuresNeeding[0] === this.room.spawns[0] && this.room.spawns[0].store.getUsedCapacity() > 200)) {
        this.task = new task.TaskTransfer(spawnNeeds, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${spawnNeeds}`);
    } else if (this.room.terminal && this.room.terminal.store.getUsedCapacity(RESOURCE_ENERGY) < 6000) {
        this.task = new task.TaskTransfer(this.room.terminal, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${this.room.terminal}`);
    } else if (towerNormal.length > 0) {
        let tower = this.pos.findClosestByRange(towerNormal)
        this.task = new task.TaskTransfer(tower, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${tower}`);
    } else if (factoryNeeds) {
        this.task = new task.TaskTransfer(this.room.factory, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${this.room.factory}`);
    } else if (spawnContainer && spawnContainer.store.getUsedCapacity(RESOURCE_ENERGY) < 2000) {
        this.task = new task.TaskTransfer(spawnContainer, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${spawnContainer}`);
    } else if (spawnLinkOverHalf && this.store.getFreeCapacity() > 0) {
        this.task = new task.TaskWithdraw(this.room.spawnLink, RESOURCE_ENERGY);
        log.write(log.FILLER, fn, `${this.name} fill target chosen target=${this.room.spawnLink}`);
    } 
    // else if (this.inRangeTo(this.rooms.spawns[0], 1)) {
    //     this.task = new task.TaskGoTo(this.room.spawns[0], {"targetRange": 1});
    // }
}


Creep.prototype.fillerRenew = function() {
    const fn = "Creep.fillerRenew";
    let closestSpawn = this.pos.findClosestByRange(this.room.spawns, (spawn) => !spawn.Spawning);
    if (!this.pos.isNearTo(closestSpawn)) {
        log.write(log.FILLER, fn, `${this.name} move to spawn to renew.`);
        this.moveTo(closestSpawn);
    } else {
        let currentBodyCost = this.colony.getBodyCostType(this.body);
        log.write(log.FILLER, fn, `${this.name} energyMax=${this.colony.spawnEnergyAvailable === this.colony.spawnEnergyMax} ideal=${this.colony.creepBodyIdeal(150, this.body, true)}`);
        if (currentBodyCost !== 1500 && this.colony.spawnEnergyAvailable === this.colony.spawnEnergyMax && !this.colony.creepBodyIdeal(150, this.body, true)) {
            log.write(log.FILLER, fn, `${this.name} get recycled.`);
            closestSpawn.recycleCreep(this);
        } else {
            log.write(log.FILLER, fn, `${this.name} get renewed.`);
            closestSpawn.renewCreep(this);
            this.transfer(closestSpawn, RESOURCE_ENERGY);
        }
    }
}


Creep.prototype.fillerNewRun = function() {
    const fn = "Creep.fillerNewRun";
    let spawnLinkOverHalf = this.room.spawnLink && this.room.spawnLink.store.getFreeCapacity(RESOURCE_ENERGY) <= 400;
    if (this.isFull) {
        this.working = true;
    } else if (this.isEmpty) {
        this.working = false;
    }

    log.write(log.FILLER, fn, `${this.name} working=${this.working}`)
    if (!this.working) {
        this.fillerNotWorking();
    } else {
        this.fillerWorking();
    }

    if (log.flagOn(log.FILLER) && this.task && this.task.target) {
        // log.write(log.FILLER, fn, `${this.name} marking target ${this.task.target}`);
        let rV = this.room.visual;
        rV.circle(this.task.target.pos, {fill: "#FF0000"});
    }

    if (!this.task && this.lab) {
        log.write(log.FILLER, fn, `${this.name} no task yet, check if lab/factory became ready, or renew.`);
        if (this.lab && (this.labNeed || this.factoryNeed)) {
            log.write(log.FILLER, fn, `${this.name} lab/factory in requested state or boosting, get rid of energy.`);
            this.task = task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY);
        } else if (spawnLinkOverHalf && this.ticksToLive >= 750) {
            log.write(log.FILLER, fn, `${this.name} spawn link filling and not risk of expiring, get rid of energy.`);
            this.task = task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY);
        } else if (this.lab || this.room.controller.level >= 7) {
            this.fillerRenew();
        }
    } else if (!this.task) {
       if (this.room.spawnLink && this.room.spawnLink.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
           this.task = task.Tasks.transfer(this.room.storage, RESOURCE_ENERGY);
           this.task.fork = task.Tasks.withdraw(this.room.spawnLink, RESOURCE_ENERGY);
       } else if (this.lab || this.room.controller.level >= 7) {
            this.fillerRenew();
       }
    }
}