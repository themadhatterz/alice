const log = require("../../log");
const task = require("../../task");


Object.defineProperty(Creep.prototype, "needsBuildBoost", {
    get: function() {
        for (let boost in this.boostCounts) {
            if (BOOST_TYPES["build"].includes(boost)) {
                return false;
            }
        }
        return true;
    },
    configurable: true,
});


Creep.prototype.workerRun = function() {
    const fn = "Creep.workerRun";

    if (this.colony.lab.outputLabAvailable && this.room.controller.level >= 7 && this.ticksToLive >= 1350 && this.colony.constructionSites >= 1 && this.needsBuildBoost) {
        log.write(log.WORKER|log.LAB, fn, `${this.name} needs boosting, check what is available.`);
        let options = this.colony.lab.resourceBoostAvailable("build");
        for (let [resource, amount] of Object.entries(options)) {
            if ((amount/LAB_BOOST_MINERAL) >= this.getActiveBodyparts(WORK)) {
                let lab = this.colony.lab.boostRequest(resource, this.getActiveBodyparts(WORK) * LAB_BOOST_MINERAL);
                if (lab) {
                    log.write(log.WORKER|log.LAB, fn, `Boosting with ${resource} at ${lab}`);
                    this.task = task.Tasks.getBoosted(lab, resource, this.getActiveBodyparts(WORK));
                    return;
                } else {
                    log.write(log.WORKER|log.LAB, fn, `${this.name} no lab available for boosting.`);
                }
            }
        }
    }

    if (this.isEmpty) {
        let rechargeTarget = this.rechargeTarget();
        if (rechargeTarget) {
            log.write(log.WORKER, fn, `${this.name} is empty and found recharge target ${rechargeTarget}`);
            if (rechargeTarget.structureType === STRUCTURE_CONTAINER || rechargeTarget.structureType === STRUCTURE_STORAGE || rechargeTarget.structureType === STRUCTURE_LINK || rechargeTarget instanceof Ruin || rechargeTarget.deathTime) {
                this.task = task.Tasks.withdraw(rechargeTarget, RESOURCE_ENERGY);
            } else {
                this.task = task.Tasks.pickup(rechargeTarget, RESOURCE_ENERGY);
            }
        } else {
            log.write(log.INFO|log.WORKER, fn, `${this.name} is empty and did not find a recharge target.`);
        }
    } else {
        let canRepairTotal = this.getActiveBodyparts(CARRY) * CARRY_CAPACITY * REPAIR_POWER * 0.75;
        let roadRepairsTotal = _.sum(this.colony.roadRepairSites, (s) => s.hitsMax - s.hits);
        log.write(log.WORKER, fn, `road status ${roadRepairsTotal} > ${canRepairTotal}`);

        let untargetedRepairsStandard = [];
        let untargetedRepairsUrgent = [];
        for (let repairSite of this.colony.repairSites) {
            if (repairSite.targetedBy.length === 0) {
                if ((repairSite.hits * 2) < repairSite.hitsMax) {
                    untargetedRepairsUrgent.push(repairSite);
                } else if ((repairSite.hitsMax - repairSite.hits) > (canRepairTotal * 0.75)) {
                    untargetedRepairsStandard.push(repairSite);
                }
            }
        }

        let barrierRepairs = [];
        for (let barrierRepairSite of this.room.barrierRepairSites) {
            if (barrierRepairSite.targetedBy.length === 0) {
                if ((this.room.barrierHealthPerRCL - barrierRepairSite.hits) >= (canRepairTotal * 0.75)) {
                    barrierRepairs.push(barrierRepairSite);
                }
            }
        }
        let barrierRepairsCritical = _.filter(this.room.barrierRepairSites, (b) => b.hits < 5000);
        log.write(log.WORKER, fn, `barrierRepairSites=${barrierRepairs.length} barrierRepairsCritical=${barrierRepairsCritical.length}`);

        if (this.colony.room.controller.ticksToDowngrade < 5000) {
            this.task = task.Tasks.ugprade(this.colony.room.controller);
            log.write(log.WORKER, fn, `${this.name} controller downgrade imminent.`);
        } else if (barrierRepairsCritical.length > 0) {
            let repairTarget = this.pos.findClosestTargetByPath(barrierRepairsCritical);
            this.task = task.Tasks.repair(repairTarget);
            log.write(log.WORKER, fn, `${this.name} critical barrier repiar target ${repairTarget}`);
        } else if (untargetedRepairsUrgent.length > 0) {
            let repairTarget = this.pos.findClosestTargetByPath(untargetedRepairsUrgent);
            this.task = task.Tasks.repair(repairTarget);
            log.write(log.WORKER, fn, `${this.name} urgent repair target found ${repairTarget}.`);
        } else if (this.room.hostileCreeps.length === 0 && this.colony.constructionSites.length > 0) {
            let groupedByPriority = _.groupBy(this.colony.constructionSites, function(cS) {return BUILD_PRIORITY.indexOf(cS.structureType)});
            // feels like a hack but works, groups by priority and gets the first key in object which should be highest priority, then finds closest.
            let constructTarget = this.pos.findClosestTargetByPath(groupedByPriority[Object.keys(groupedByPriority)[0]]);
            this.task = task.Tasks.build(constructTarget);
            log.write(log.WORKER, fn, `${this.name} constructing ${constructTarget}.`);
        } else if (this.room.hostileCreeps.length === 0 && untargetedRepairsStandard.length > 0) {
            let repairTarget = this.pos.findClosestTargetByPath(untargetedRepairsStandard);
            this.task = task.Tasks.repair(repairTarget);
            log.write(log.WORKER, fn, `${this.name} standard repair target found ${repairTarget}.`);
        } else if (this.room.hostileCreeps.length === 0 && roadRepairsTotal >= canRepairTotal ) {
            let closestRoad = null;
            let repairTasks = [];
            let allColonyRoadRepairSites = this.colony.roadRepairSites;
            log.write(log.WORKER, fn, `${this.name} roads ready for repair backlog before ${allColonyRoadRepairSites.length}.`);
            while (allColonyRoadRepairSites.length > 0 && canRepairTotal > 0 && repairTasks.length < 30) {
                if (closestRoad) {
                    fromLocation = closestRoad.pos;
                } else {
                    // Start at lowest health road.
                    // NEW WAY
                    allColonyRoadRepairSites.sort(function(a, b) {
                        return (a.hits < b.hits ? -1: 1)
                    });
                    fromLocation = allColonyRoadRepairSites[0].pos;
                    log.write(log.WORKER, fn, `Starting road repairs at ${fromLocation}`);
                    // OLD WAY CLOSEST ROAD fromLocation = this.pos;
                }
                closestRoad = fromLocation.findClosestTargetByPath(allColonyRoadRepairSites);
                let indexOf = allColonyRoadRepairSites.indexOf(closestRoad);
                allColonyRoadRepairSites.splice(indexOf, 1);
                repairTasks.unshift(task.Tasks.repair(closestRoad));
                if (closestRoad && closestRoad.hitsMax) {
                    canRepairTotal -= (closestRoad.hitsMax - closestRoad.hits) / REPAIR_POWER;
                } else {
                    log.write(log.ERROR|log.NOTIFY, fn, `${this.name} closest null or no hitsMax ${JSON.stringify(closestRoad)}`);
                }
            }
            log.write(log.WORKER, fn, `${this.colony.name} road repair backlog after ${allColonyRoadRepairSites.length}`);
            this.task = task.Tasks.chain(repairTasks);
        } else if (barrierRepairs.length > 0) {
            barrierRepairs.sort(function(a, b) {
                return (a.hits < b.hits ? -1: 1)
            });
            let repairTarget = barrierRepairs[0];
            this.task = task.Tasks.repair(repairTarget);
            log.write(log.WORKER, fn, `${this.name} barrier repair needed ${repairTarget}`);
        } else {
            if (!this.colony.room.controller.sign || (this.colony.room.controller.sign.text !== CONTROLLER_SIGNATURE && this.colony.room.controller.sign.text !== SCREEPS_NOVICE_SIGNATURE)) {
                this.task = task.Tasks.signController(this.colony.room.controller);
            } else {
                this.task = task.Tasks.ugprade(this.colony.room.controller);
            }
            log.write(log.WORKER, fn, `${this.name} nothing to do, upgrade controller.`);
        }
    }
}