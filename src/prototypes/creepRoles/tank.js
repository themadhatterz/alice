const task = require("../../task");


Creep.prototype.tankRun = function() {
    let flag = _.filter(this.colony.colonyFlags, (flag) => flag.memory.type === "tank")[0];
    if (flag) {
        if (this.room.name !== flag.memory.room && this.hits === this.hitsMax) {
            this.task = task.Tasks.goTo(flag);
        } else {
            if (this.hits * 2 <= this.hitsMax) {
                this.task = task.Tasks.goTo(new RoomPosition(48, 27, "E16S21"));
            }
        }
    }
}


