const log = require("../../log");
const task = require("../../task");


Object.defineProperty(Creep.prototype, "needsUpgradeBoost", {
    get: function() {
        for (let boost in this.boostCounts) {
            if (BOOST_TYPES["upgrade"].includes(boost)) {
                return false;
            }
        }
        return true;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "ticksToEmpty", {
    get: function() {
        if (!this._ticksToEmpty) {
            const workParts = this.getActiveBodyparts(WORK);
            const energyCapacity = this.store.getCapacity();
            this._ticksToEmpty = Math.floor(energyCapacity / workParts);
        }
        return this._ticksToEmpty;
    },
    configurable: true,
});



Creep.prototype.upgraderRun = function() {
    const fn = "Creep.upgraderRun";

    if (this.colony.lab.outputLabAvailable && (this.room.controller.level === 6 || this.room.controller.level === 7) && this.ticksToLive >= 1350 && this.needsUpgradeBoost) {
        log.write(log.UPGRADER|log.LAB, fn, `${this.name} needs boosting, check what is available.`);
        let options = this.colony.lab.resourceBoostAvailable("upgrade");
        for (let [resource, amount] of Object.entries(options)) {
            if ((amount/LAB_BOOST_MINERAL) >= this.getActiveBodyparts(WORK)) {
                let lab = this.colony.lab.boostRequest(resource, this.getActiveBodyparts(WORK) * LAB_BOOST_MINERAL);
                if (lab) {
                    log.write(log.UPGRADER|log.LAB, fn, `Boosting with ${resource} at ${lab}`);
                    this.task = task.Tasks.getBoosted(lab, resource, this.getActiveBodyparts(WORK));
                    return;
                } else {
                    log.write(log.UPGRADER|log.LAB, fn, `${this.name} no lab available for boosting.`);
                }
            }
        }
    }

    // TODO: This doesn't work even though I logged these and both say TRUE TRUE
    if (this.isBoosted && (this.ticksToLive < ((this.pos.findPathTo(this.room.spawns[0]).length * 2) + (this.ticksToEmpty)))) {
        log.write(log.UPGRADER, fn, `wut`);
        let lab = this.colony.lab.unboostRequest();
        if (lab) {
            log.write(log.UPGRADER|log.LAB, fn, `Unboosting at ${lab}`);
            this.task = task.Tasks.getUnBoosted(lab);
            return;
        } else {
            log.write(log.UPGRADER|log.LAB, fn, `${this.name} no lab available for unboosting.`);
        }
    }

    if (this.isEmpty) {
        if (this.colony.room.controller.level >= 7 && this.colony.room.upgradeLink && this.colony.room.upgradeLink.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            this.task = new task.TaskWithdraw(this.colony.room.upgradeLink, RESOURCE_ENERGY);
        } else if (this.colony.room.controllerContainer && this.colony.room.controllerContainer.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            this.task = new task.TaskWithdraw(this.colony.room.controllerContainer, RESOURCE_ENERGY);
        } else {
            let rechargeTarget = this.rechargeTarget();
            if (rechargeTarget) {
                if (rechargeTarget.structureType === STRUCTURE_CONTAINER || rechargeTarget.structureType === STRUCTURE_STORAGE || rechargeTarget.structureType === STRUCTURE_LINK || rechargeTarget.deathTime) {
                    this.task = new task.TaskWithdraw(rechargeTarget, RESOURCE_ENERGY);
                } else {
                    this.task = new task.TaskPickup(rechargeTarget, RESOURCE_ENERGY);
                }
            }
        }
    } else {
        if (!this.colony.room.controller.sign || (this.colony.room.controller.sign.text !== CONTROLLER_SIGNATURE && this.colony.room.controller.sign.text !== SCREEPS_NOVICE_SIGNATURE)) {
            this.task = new task.TaskSignController(this.colony.room.controller);
        } else if (this.colony.room.controllerContainer && !this.pos.isEqualTo(this.colony.room.controllerContainer)) {
            this.moveTo(this.colony.room.controllerContainer);
        } else {
            if (!this.pos.inRangeTo(this.room.controller, 1)) {
                this.moveTo(this.room.controller);
            } else {
                this.task = new task.TaskUpgrade(this.colony.room.controller);
            }
        }
    }
}