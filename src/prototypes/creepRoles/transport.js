const log = require("../../log");
const task = require("../../task");


Creep.prototype.transportIdlePosition = function() {
    const fn = "Creep.transportIdlePosition";
    if (this.room.name !== this.colony.room.name) {
        this.moveTo(this.colony.room.spawns[0]);
    } else {
        const transports = this.colony.getCreepsByRole(ROLE_TRANSPORT).sort(function(a,b) {return a.tickToLive < b.tickToLive ? -1 : 1});
        const positions = [
            {x: -10, y:  0},
            {x:  0,  y: -4},
            {x:  0,  y:  4},
            {x:  2,  y:  0},
            {x: -4,  y: -6},
            {x: -4,  y:  6},
            {x: -8,  y: -4},
            {x: -8,  y:  4},
        ]
        const transportIndex = transports.indexOf(this);
        const x = this.colony.room.anchor.x + positions[transportIndex].x;
        const y = this.colony.room.anchor.y + positions[transportIndex].y;
        if (this.pos.x !== x || this.pos.y !== y) {
            this.moveTo(this.colony.room.anchor.x + positions[transportIndex].x, this.colony.room.anchor.y + positions[transportIndex].y)
        }
    }
}


Creep.prototype.transporterRun = function() {
    const fn = "Creep.transporterRun";
    if (!this.halfFull && !(this.store.getUsedCapacity() > this.store.getUsedCapacity(RESOURCE_ENERGY))) {
        // TODO: long term we might store stuff in storage that isn't energy. For now keep it all in terminal.
        if (this.colony.room.terminal && this.colony.room.storage.store.getUsedCapacity(RESOURCE_ENERGY) < this.colony.room.storage.store.getUsedCapacity()) {
            for (let resource in this.colony.room.storage.store) {
                if (resource === RESOURCE_ENERGY) {
                    continue;
                }
                if (!this.task) {
                    this.task = task.Tasks.withdraw(this.colony.room.storage, resource);
                } else {
                    this.task.fork(task.Tasks.withdraw(this.colony.room.storage, resource));
                }
            }
            log.write(log.TRANSPORT, fn, `${this.name} transfering mineral/compounds to terminal`);
            return;
        }
        if (this.room.droppedResource.length > 0) {
            let target = this.pos.findClosestByPath(this.room.droppedResource)
            this.task = new task.TaskPickup(target, target.mineralType);
            log.write(log.TRANSPORT, fn, `${this.name} picking up dropped resource same type as room mineral.`);
            return;
        }
        var pick = null;
        let pickupOptions = this.colony.droppedEnergy();
        let sourceContainers = [];
        // Terminal Energy Overage (we bought some energy);
        if (this.colony.room.terminal && this.colony.room.terminal.store.getUsedCapacity(RESOURCE_ENERGY) >= 10000) {
            pickupOptions.push(this.colony.room.terminal);
        }

        for (let source of this.colony.sources) {
            if (source.container && source.container.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                sourceContainers.push(source.container);
                pickupOptions.push(source.container);
            }
        }
        if (this.room.extractor && this.room.mineral.container && this.room.mineral.container.targetedByRole(ROLE_TRANSPORT).length === 0 && (this.room.mineral.container.store.getUsedCapacity() > this.store.getFreeCapacity() || this.room.mineral.container.store.getUsedCapacity() >= this.store.getFreeCapacity())) {
            this.task = task.Tasks.withdrawAll(this.room.mineral.container);
            log.write(log.TRANSPORT, fn, `${this.name} picking up from extraction site.`);
            return;
        }
        if (this.colony.tombstones.length > 0) {
            for (let tombstone of this.colony.tombstones) {
                // TODO: Is this broken?
                if (tombstone.store.getUsedCapacity() > 0 && tombstone.targetedByRole(ROLE_TRANSPORT).length === 0) {
                    this.task = task.Tasks.withdrawAll(tombstone);
                    return;
                }
            }
        }

        if (this.room.ruins.length > 0) {
            let ruinsWithEnergy = _.filter(this.room.ruins, (r) => r.store.getUsedCapacity(RESOURCE_ENERGY) > 0);
            for (let rWE of ruinsWithEnergy) {
                pickupOptions.push(rWE);
            }
        }

        let closeOptions = [];
        for (let option of pickupOptions) {
            if (this.pos.inRangeTo(option, 3)) {
                closeOptions.push(option);
            }
        }
        if (closeOptions.length > 0) {
            pick = this.pos.findClosestTargetByPath(closeOptions);
            log.write(log.TRANSPORT, fn, `${this.name} found close option ${pick}`);
        }

        let filterOptions = [];
        for (let eO of pickupOptions) {
            let amount = eO.amount || eO.store.getUsedCapacity();
            let incomingCarry = 0;
            for (let creep of eO.targetedBy) {
                incomingCarry += creep.store.getFreeCapacity();
            }
            let leftover = amount - incomingCarry;
            if (eO instanceof Resource || leftover >= this.store.getFreeCapacity() * 0.75) {
                filterOptions.push({obj: eO, leftover: leftover});
                log.write(log.TRANSPORT, fn, `${this.name} adding option ${eO}, leftover was ${leftover}`);
            } else {
                log.write(log.TRANSPORT, fn, `${this.name} ignoring options ${eO}, leftover was ${leftover}`);
            }
        }

        if (!pick && filterOptions.length > 0) {
            // TODO: Transport should favor resource on ground at container.
            filterOptions.sort(function(a,b) {return a.leftover > b.leftover ? -1 : 1});
            pick = filterOptions[0].obj;
            log.write(log.TRANSPORT, fn, `${this.name} found a filtered option ${pick}`);
        } else if (!pick && filterOptions.length === 0) {
            log.write(log.TRANSPORT, fn, `${this.name} has no pick and there are no options.`);
            let untargetedSourceContainers = [];
            for (let container of sourceContainers) {
                if (container.targetedByRole(ROLE_TRANSPORT).length === 0 && container.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                    untargetedSourceContainers.push(container);
                }
            }
            if (untargetedSourceContainers.length > 1) {
                untargetedSourceContainers.sort(function(a,b) {return a.store.getUsedCapacity(RESOURCE_ENERGY) > b.store.getUsedCapacity(RESOURCE_ENERGY) ? -1 : 1});
            }
            if (untargetedSourceContainers.length > 0) {
                pick = untargetedSourceContainers[0];
                log.write(log.TRANSPORT, fn, `${this.name} heading to untargeted container ${untargetedSourceContainers[0]} with most energy currently.`);
            } else {
                log.write(log.TRANSPORT, fn, `${this.name} there are no untargeted containers currently.`);
            }
        }

        if (pick) {
            if (pick.amount) {
                this.task = new task.TaskPickup(pick);
            } else {
                this.task = new task.TaskWithdraw(pick, RESOURCE_ENERGY);
            }
        } else {
            // Empty SpawnLink
            if (this.colony.room.spawnLink && this.colony.room.spawnLink.store.getUsedCapacity(RESOURCE_ENERGY) >= 400) {
                this.task = task.Tasks.transfer(this.colony.storage);
                this.task.fork(task.Tasks.withdraw(this.colony.room.spawnLink, RESOURCE_ENERGY));
            // Go to any untargeted container.
            } else {
                this.transportIdlePosition();
            }
        }
    } else {
        if (this.colony.room.spawnContainer && this.colony.room.spawnContainer.store.getFreeCapacity(RESOURCE_ENERGY) > 1000 && this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            this.task = new task.TaskTransfer(this.colony.room.spawnContainer, RESOURCE_ENERGY);
        } else if (this.colony.room.controllerContainer && this.colony.room.controllerContainer.targetedByRole(ROLE_TRANSPORT).length === 0 && this.colony.room.controllerContainer.store.getFreeCapacity(RESOURCE_ENERGY) > 1000 && this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            this.task = new task.TaskTransfer(this.colony.room.controllerContainer, RESOURCE_ENERGY);
        } else if (!this.isEmpty) {
            if (!this.colony.room.terminal) {
                if (!this.colony.room.storage) { 
                    // Can be a lot of transports sitting around sometimes.
                    if (this.colony.room.spawnContainer.store.getFreeCapacity(RESOURCE_ENERGY) === 0) {
                        let spawnNeeds = this.pos.findClosestTargetByPath(this.room.fillableStructures);
                        if (spawnNeeds) {
                            this.task = task.Tasks.transfer(spawnNeeds, RESOURCE_ENERGY);
                        } else {
                            if (this.colony.room.controllerContainer && this.colony.room.controllerContainer.store.getFreeCapacity(RESOURCE_ENERGY) > 1000 && this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                                this.task = new task.TaskTransfer(this.colony.room.controllerContainer, RESOURCE_ENERGY);
                            } else {
                                this.transportIdlePosition();
                            }
                        }
                    } else {
                        this.task = task.Tasks.transfer(this.colony.room.spawnContainer, RESOURCE_ENERGY);
                    }
                } else if (this.colony.room.storage) {
                    this.task = task.Tasks.transferAll(this.colony.storage);
                } else {
                    this.transportIdlePosition();
                }
            } else if (this.colony.room.terminal && this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                this.task = new task.TaskTransfer(this.colony.storage, RESOURCE_ENERGY);
            } else if (this.colony.room.terminal) {
                let skipEnergy = true;
                this.task = new task.TaskTransferAll(this.colony.room.terminal, skipEnergy);
            }
        } else {
            if (!this.pos.inRangeTo(this.room.spawns[0], 1)) {
                this.moveTo(this.room.spawns[0]);
            } else if (!this.room.spawns[0].Spawning) {
                let transporters = this.colony.getCreepsByRole(ROLE_TRANSPORT);
                if (transporters.length > 1) {
                    this.room.spawns[0].recycleCreep(this);
                } else {
                    this.room.spawns[0].renewCreep(this);
                }
            }
        }
    }
}