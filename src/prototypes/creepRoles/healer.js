const task = require("../../task");


Creep.prototype.healerRun = function() {
    let flag = _.filter(this.colony.colonyFlags, (flag) => flag.memory.type === "attack")[0];
    let attacker = this.colony.getCreepsByRole(ROLE_ATTACKER)[0];
    if (flag && flag.memory.stage === "spawning" && !attacker) {
        // wait for attacker to spawn
        this.moveTo(this.room.anchor.x + -4, this.room.anchor.y + -6);
    } else if (flag && attacker && flag.memory.stage === "active") {
        this.moveTo(attacker);
        if (this.hits < this.hitsMax) {
            this.task = new task.TaskHeal(this);
        } else if (attacker.hits < attacker.hitsMax) {
            this.task = new task.TaskHeal(attacker);
        }
    }
}