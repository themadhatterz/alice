const log = require("../../log");
const task = require("../../task");


Creep.prototype.bruiserRun = function() {
    const fn = "Creep.bruiserRun";
    // Move this to happen each tick regardless in room prototype probably.
    if (!("healing" in this.memory)) {
        this.memory.healing = false;
    }
    if (this.room.hostileCreeps.length === 0) {
        this.room.memory.attacked = false;
    }

    let enemies = this.colony.hostileCreeps;
    let attackParts = 0;
    attackParts += this.getActiveBodyparts(RANGED_ATTACK);
    attackParts += this.getActiveBodyparts(ATTACK);
    // have attack parts or in a room with towers
    log.write(log.BRUISER, fn, `${this.name} aP=${attackParts} healing=${this.memory.healing} inColony=${this.room.name === this.colony.room.name}`);
    if (!this.memory.healing && attackParts > 0) {
        log.write(log.BRUISER, fn, `${this.name} looking for enemy to attack.`);
        if (enemies.length > 0) {
            if (enemies[0].owner.username !== "Invader") {
                this.memory.idle = true;
            } else {
                this.memory.idle = false;
            }
            this.target = this.pos.findClosestTargetByPath(enemies);
            log.write(log.BRUISER, fn, `${this.name} attacking ${this.target}`);
            this.task = new task.TaskAttack(this.target);
        } else if (this.colony.invaderCore) {
            this.target = this.colony.invaderCore;
            log.write(log.BRUISER, fn, `${this.name} attacking invader core.`);
            this.task = new task.TaskAttack(this.colony.invaderCore);
        } else {
            let attackedRoom = false;
            for (let roomName in Memory.rooms) {
                let roomMemory = Memory.rooms[roomName]
                if (roomMemory.attacked) {
                    attackedRoom = true;
                    this.task = new task.TaskGoToRoom(roomName);
                    break;
                }
            }
            if (!attackedRoom && !this.memory.idle) {
                if (this.room.name !== this.colony.room.name) {
                    this.moveTo(this.colony.room.spawns[0]);
                } else if (!this.pos.inRangeTo(this.colony.room.spawns[0], 1)) {
                    this.moveTo(this.room.spawns[0]);
                } else if (!this.room.spawns[0].Spawning) {
                    this.room.spawns[0].recycleCreep(this);
                }
            }
        }
    // retreat to get healed
    } else if (this.memory.healing) {
        log.write(log.BRUISER, fn, `${this.name} retreating to get healed.`);
        if (this.hits === this.hitsMax) {
            this.memory.healing = false;
        }
    } else {
        log.write(log.BRUISER, fn, `${this.name} need heals, go to colony room.`);
        this.memory.healing = true;
        this.task = new task.TaskGoToRoom(this.colony.room.name);
    }
}
