const task = require("../../task");


Creep.prototype.minerRun = function() {
    if (this.isEmpty) {
        if (!this.target) {
            this.target = this.room.mineral;
        }
        if (this.target.mineralAmount === 0) {
            let closestSpawn = this.pos.findClosestByPath(this.colony.room.spawns, (spawn) => !spawn.Spawning);
            if (!this.pos.isNearTo(closestSpawn)) {
                this.moveTo(closestSpawn);
            } else {
                closestSpawn.recycleCreep(this);
            }
        } else {
            this.task = new task.TaskHarvest(this.target);
        }
    } else {
        if (this.target.container && !this.pos.isEqualTo(this.target.container)) {
            this.moveTo(this.target.container);
        } else if (this.target.container && this.target.container.store.getFreeCapacity() > this.store.getUsedCapacity()) {
            this.task = new task.TaskTransfer(this.target.container, this.target.mineralType);
        } else if (!this.target.container) {
            // container destroyed?
            this.target.buildContainer();
        }
    }
}