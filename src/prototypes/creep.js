const log = require("../log");
const utilities = require("../utilities");
const task = require("../task");
const targetCache = require("../caching");


Object.defineProperty(Creep.prototype, "colony", {
    get: function() {
        const fn = "Creep.colony";
        if (!this._colony) {
            for (let colony of Game.colonies) {
                if (this.memory.colony === colony.name) {
                    this._colony = colony;
                    break;
                }
            }
            if (!this._colony) {
                log.write(log.ERROR|log.NOTIFY, fn, `${this.name} is an orphan.`);
                this._colony = null;
            }
        }
        return this._colony;
    },
    configurable: true,
})


Object.defineProperty(Creep.prototype, "task", {
    get: function() {
        if (!this._task) {
            let protoTask = this.memory.task;
            this._task = protoTask ? task.initializeTask(protoTask, this.room.name) : null;
        }
        return this._task;
    },
    set: function(task) {
        targetCache.TargetCache.assert();
        let oldProtoTask = this.memory.task;
        if (oldProtoTask) {
            let oldRef = oldProtoTask._target.ref;
            if (Game.TargetCache.targets[oldRef]) {
                Game.TargetCache.targets[oldRef] = _.remove(Game.TargetCache.targets[oldRef], name => name === this.name);
            }
        }
        this.memory.task = task ? task.proto : null;
        if (task) {
            if (task.target) {
                if (!Game.TargetCache.targets[task.target.ref]) {
                    Game.TargetCache.targets[task.target.ref] = [];
                }
                Game.TargetCache.targets[task.target.ref].push(this.name);
            }
            task.creep = this;
            this._task = task;
        }
        // this._task = null;
    },
    configurable: true,
});


Creep.prototype.run = function() {
    const fn = "Creep.run";
    const noFlee = [ROLE_ATTACKER, ROLE_BRUISER, ROLE_HEALER, ROLE_SCOUT, ROLE_FILLER];
    const attackParts = [ATTACK, RANGED_ATTACK];
    if (!noFlee.includes(this.role) && this.room.memory.attacked && !this.pos.inRampart) {
        let needToFlee = false;
        for (let hostile of this.room.hostileCreeps) {
            for (let part of hostile.body) {
                if (attackParts.includes(part.type)) {
                    needToFlee = true;
                    break;
                }
            }
        }
        if (needToFlee) {
            let ramparts = this.colony.availableRamparts;
            let closestRampart = this.pos.findClosestTargetByPath(ramparts);
            // Drop energy to flee faster!
            if (closestRampart) {
                if (this.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                    this.drop(RESOURCE_ENERGY);
                    return;
                }
                this.memory.flee = true;
                this.task = task.Tasks.goTo(closestRampart.pos);
            }
        }
    // Flee Creep but consider if we can go back.
    } else if (!noFlee.includes(this.role) && this.memory.flee && !this.pos.inRampart) {
        let attackEnded = false;
        let outpostCount = _.filter(this.colony.colonyFlags, (f) => f.memory.type === "outpost");
        if (outpostCount.length > 0 && (this.colony.rooms.length - 1) < outpostCount.length) {
            // missing vision in one room
            attackEnded = false;
        } else {
            // Don't think this is necessary but meh.
            for (let room of this.colony.rooms) {
                if (room.memory.attacked === false) {
                    attackEnded = true;
                } else {
                    attackEnded = false;
                    break;
                }
            }
        }
        if (attackEnded) {
            this.memory.flee = false;
            this.task = null;
        }
    }

    if (!noFlee.includes(this.role) && this.hits < this.hitsMax && !this.room.healerPresent && this.colony.room.controller.level >= 3) {
        log.write(log.INFO|log.CREEP, fn, `${this.name} is damaged no healers present, go to colony for healing.`);
        this.task = task.Tasks.goToRoom(this.colony.room.name);
    }

    if (this.task) {
        return this.task.run();
    }
}


Object.defineProperty(Creep.prototype, "role", {
    get: function() {
        const fn = "Creep.role"
        if (!this._role) {
            if (!this.memory.role) {
                log.write(log.ERROR|log.CREEP, fn, `${this.name} has no role?\n${JSON.stringify(this.memory)}`);
                this._role = null;
            } else {
                this._role = this.memory.role;
            }
        }
        return this._role;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "working", {
    get: function() {
        const fn = "Creep.working"
        if (!this._working) {
            if (!("working" in this.memory)) {
                log.write(log.ERROR|log.CREEP, fn, `${this.name} has no working?\n${JSON.stringify(this.memory)}`);
                this.memory.working = null;
            }
            this._working = this.memory.working;
        }
        return this._working;
    },
    set: function(status) {
        this._working = status;
        this.memory.working = status;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "lab", {
    get: function() {
        const fn = "Creep.lab"
        if (!this._lab) {
            if (!this.memory.lab) {
                this.memory.lab = false;
            }
            this._lab = this.memory.lab;
        }
        return this._lab;
    },
    set: function(status) {
        this._lab = status;
        this.memory.lab = status;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "hasValidTask", {
    get: function() {
        return this.task && this.task.isValid();
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "isIdle", {
    get: function() {
        return !this.hasValidTask;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "halfFull", {
    get: function() {
        if (!this._halfFull) {
            let usedCapacity = this.store.getUsedCapacity();
            let halfCapacity = this.store.getCapacity() / 2;
            if (usedCapacity >= halfCapacity) {
                this._halfFull = true;
            } else {
                this._halfFull = false;
            }
        }
        return this._halfFull;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "isFull", {
    get: function() {
        if (!this._isFull) {
            this._isFull = this.store.isFull;
        }
        return this._isFull;
    }
});


Object.defineProperty(Creep.prototype, "isEmpty", {
    get: function() {
        if (!this._isEmpty) {
            this._isEmpty = this.store.isEmpty;
        }
        return this._isEmpty;
    }
});


Object.defineProperty(Creep.prototype, "isBoosted", {
    get: function() {
        this._isBoosted = false;
        for (let bodyObj of this.body) {
            if (bodyObj.boost !== undefined) {
                this._isBoosted = true;
                break;
            }
        }
        return this._isBoosted;
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "target", {
    get: function() {
        if (!this._target) {
            if (this.memory.target) {
                this._target = utilities.deref(this.memory.target);
            } else {
                this.memory.target = null;
                this._target = null;
            }
        }
        return this._target;
    },
    set: function(target) {
        this._target = target;
        if (target) {
            this.memory.target = target.ref;
        } else {
            this.memory.target = null;
        }
        
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "scoutTarget", {
    get: function() {
        if (!this._scoutTarget) {
            if (this.memory.scoutTarget) {
                this._scoutTarget = this.memory.scoutTarget;
            } else {
                this.memory.scoutTarget = null;
                this._scoutTarget = null;
            }
        }
        return this._scoutTarget;
    },
    set: function(scoutTarget) {
        this._scoutTarget = scoutTarget;
        if (scoutTarget) {
            this.memory.scoutTarget = scoutTarget;
        } else {
            this.memory.scoutTarget = null;
        }
        
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "currentRoom", {
    get: function() {
        if (!this._currentRoom) {
            if (this.memory.currentRoom) {
                this._currentRoom = this.memory.currentRoom;
            } else {
                this.memory.currentRoom = null;
                this._currentRoom = null;
            }
        }
        return this._currentRoom;
    },
    set: function(currentRoom) {
        this._currentRoom = currentRoom;
        if (currentRoom) {
            this.memory.currentRoom = currentRoom;
        } else {
            this.memory.currentRoom = null;
        }
        
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "direction", {
    get: function() {
        if (!this._direction) {
            if (this.memory.direction) {
                this._direction = this.memory.direction
            } else {
                this.memory.direction = null;
                this._direction = null;
            }
        }
        return this._direction;
    },
    set: function(direction) {
        this._direction = direction;
        if (direction) {
            this.memory.direction = direction;
        } else {
            this.memory.direction = null;
        }
        
    },
    configurable: true,
});


Object.defineProperty(Creep.prototype, "boostCounts", {
    get: function() {
        return _.countBy(this.body, part => part.boost);
    },
    configurable: true,
});


Creep.prototype.runFunction = function() {
    const fn = "Creep.runFunction";
    switch(this.role) {
        case ROLE_HARVESTER:
            this.harvesterRun();
            break;
        case ROLE_UPGRADER:
            this.upgraderRun();
            break;
        case ROLE_FILLER:
            this.fillerNewRun();
            break;
        case ROLE_WORKER:
            this.workerRun();
            break;
        case ROLE_TRANSPORT:
            this.transporterRun();
            break;
        case ROLE_SCOUT:
            this.scoutRun();
            break;
        case ROLE_MINER:
            this.minerRun();
            break;
        case ROLE_RESERVER:
            this.reserverRun();
            break;
        case ROLE_BRUISER:
            this.bruiserRun();
            break;
        case ROLE_ATTACKER:
            this.attackerRun();
            break;
        case ROLE_HEALER:
            this.healerRun();
            break;
        case ROLE_TANK:
            this.tankRun();
            break;
        default:
            log.write(log.NOTIFY, fn, `${this.name} has no role!`);
            break;
    }

} 


Creep.prototype.rechargeTarget = function() {
    let energyOptions = this.colony.droppedEnergy(this.store.getFreeCapacity());
    if (this.role === ROLE_WORKER && ((this.colony.room.controller.level > 1 && this.colony.room.controller.level < 4))) {
        if (this.colony.room.controllerContainer && this.colony.room.controllerContainer.store.getUsedCapacity(RESOURCE_ENERGY) >= 300 && this.colony.room.controllerContainer.pos.availableNeighbors().length > 1) {
            energyOptions.push(this.colony.room.controllerContainer);
        }
    }
    for (let source of this.colony.sources) {
        if (source.container) {
            if (source.container.store.getUsedCapacity(RESOURCE_ENERGY) > 50) {
                energyOptions.push(source.container)
            }
        }
    }
    if (this.colony.tombstones.length > 0) {
        for (let tombstone of this.colony.tombstones) {
            if (tombstone.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                energyOptions.push(tombstone);
            }
        }
    }

    if (this.room.ruins.length > 0) {
        for (let ruin of this.room.ruins) {
            if (ruin.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                energyOptions.push(ruin);
            }
        }
    }

    let storageMinimum = this.role === ROLE_FILLER ? 1000 : 2000
    if (this.colony.storage && this.colony.storage.store.getUsedCapacity(RESOURCE_ENERGY) > storageMinimum && this.colony.storage.pos.availableNeighbors().length > 0) {
        energyOptions.push(this.colony.storage);
    }

    if (this.role === ROLE_FILLER) {
        // let transports = this.colony.getCreepsByRole(ROLE_TRANSPORT);
        if (this.room.spawnLink && this.room.spawnLink.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            return this.room.spawnLink;
        }
        // TODO: Filler leaves to transport if nothing in base to fill. Maybe don't if transports exist?
        if (this.room.fillableStructures.length > 0 && this.room.spawnContainer && this.room.spawnContainer.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            if (!this.room.storage || (this.room.storage && this.room.storage.store.getUsedCapacity(RESOURCE_ENERGY) < 5000)) {
                energyOptions.push(this.room.spawnContainer);
            } 
        }
    }
    let closeOptions = []
    for (let option of energyOptions) {
        if (option && this.pos.inRangeTo(option, 5)) {
            if (option.amount && option.amount > this.store.getFreeCapacity()) {
                closeOptions.push(option)
            } else if (option.store && option.store.getUsedCapacity(RESOURCE_ENERGY) > this.store.getFreeCapacity()) {
                closeOptions.push(option)
            }
        }
    }
    if (closeOptions.length > 0) {
        return this.pos.findClosestTargetByPath(closeOptions);
    }
    let filterOptions = [];
    for (let eO of energyOptions) {
        let amount = eO.amount || eO.store.getUsedCapacity();
        let targetedBy = eO.targetedBy;
        let incomingCarry = 0;
        for (let creep of targetedBy) {
            incomingCarry += creep.store.getFreeCapacity();
        }
        if (incomingCarry < amount) {
            filterOptions.push(eO);
        }
    }
    if (filterOptions.length > 0) {
        if (this.role === ROLE_FILLER) {
            return this.pos.findClosestByPath(filterOptions);
        } else {
            return this.pos.findClosestTargetByPath(filterOptions);
        }
    }
}

actionsMatrix = {
    [WORK] : [
        'harvest', 'build', 'repair','dismantle','upgradeController'
    ],
    [ATTACK] : [
        'attack'
    ], 
    [RANGED_ATTACK] : [
        'rangedAttack', 'rangedMassAttack'
    ],
    [HEAL] : [
        'heal', 'rangedHeal'
    ],
    [CARRY] : [
        'capacity'
    ],
    [MOVE] : [
        'fatigue'
    ],
    [TOUGH] : [
        'damage'
    ]
}

const actionsAmountMatrix = {
    'harvest' : HARVEST_POWER, 
    'build': BUILD_POWER, 
    'repair': REPAIR_POWER,
    'dismantle': DISMANTLE_POWER,
    'upgradeController': UPGRADE_CONTROLLER_POWER,
    'attack': ATTACK_POWER,
    'rangedAttack': RANGED_ATTACK_POWER, 
    'rangedMassAttack': RANGED_ATTACK_POWER,
    'heal' : HEAL_POWER, 
    'rangedHeal': RANGED_HEAL_POWER,
    'capacity': CARRY_CAPACITY,
    'fatigue' : 1,
    'damage': 100
}


// for each in the actions matrix, calcuate. example creep.attackAmount
for (const type in actionsMatrix) {
    actionsMatrix[type].forEach(function(action){
        Object.defineProperty(Creep.prototype, action+'Amount', {
            get: function(){
                const fn = `Creep.${action}Amount`;
                log.write(log.ERROR, fn, `Calculating ${action} for ${this.name}`);
                if (!this['_'+action+'Amount']) {
                    this['_'+action+'Amount'] = 0;
                    for (const b of this.body) {
                        if (!b.hits || type !== b.type) continue;
                        let multiplier = !b.boost ? 1 : BOOSTS[b.type][b.boost][action];
                        this['_'+action+'Amount'] += (actionsAmountMatrix[action] * multiplier);
                    }
                }
                return this['_'+action+'Amount']
            },
            enumerable: false,
            configurable: true,
        });
    });
}

Object.defineProperty(Creep.prototype, 'damagethreat', {
    get: function() {
        if (!this._damagethreat) {
            this._damagethreat = 0 //initialize
            //if it is safemode, we can't take damage, or if we are in a rampart
            if (this.pos.inRampart || (this.room.controller && this.room.controller.safeMode && this.room.controller.owner && this.room.controller.owner.username === this.owner.username)) {
                return this._damagethreat
            }

            //find hostile towers
            const towers = this.room.find(this.room.towers, { filter: s => s.store[RESOURCE_ENERGY] > 0 && s.isActive() && this.owner.username !== s.owner.username})

            //get each towers damage amount
            _.forEach(towers, t => {
                this._damagethreat += t.Amount(TOWER_POWER_ATTACK, this)
            })
            //find hostile creeps
            const creeps = this.pos.findInRange(FIND_CREEPS, 3, { filter: s => this.owner.username !== s.owner.username})

            for (const c of creeps) {
                const range = c.pos.getRangeTo(this)
                //we need to worry about attack parts and ranged attack parts
                if (range <= 1) this._damagethreat += c.attackAmount ? i.attackAmount : 0
                if (range <= 3) this._damagethreat += c.ranged_attackAmount ? i.ranged_attackAmount: 0
            }
            this._damagethreat = this._damagethreat * this.damageAmount //multiply buy our damage amount (aka boosted tought parts)
        }
        return this._damagethreat
    },
    configurable: true,
});

Object.defineProperty(Creep.prototype, 'healcapacity', {
    get: function() {
        if (this._healcapacity === undefined) {
            this._healcapacity = 0 //initialize

            //if it is safemode, we can't heal...
            if (this.room.controller && this.room.controller.safeMode && this.room.controller.owner && this.room.controller.owner.username !== this.owner.username) {
                return this._healcapacity
            }
            //get all creeps in healing range of this one (this array includes this creep)
            const allies = this.pos.findInRange(FIND_CREEPS, 3, {
                filter: s => s.owner.username === this.owner.username 
            })
            for (let i of allies) this._healcapacity += this.pos.getRangeTo(i) <= 1 ? i.healAmount : i.rangedHealAmount
            //find towers
            const towers = this.room.find(this.room.towers, { filter: s => s.store[RESOURCE_ENERGY] > 0 && s.isActive() && this.owner.username === s.owner.username})

            _.forEach(towers, i => {
                this._healcapacity += i.Amount(TOWER_POWER_HEAL, this)
            })
        }
        return this._healcapacity
    },
    configurable: true,
});