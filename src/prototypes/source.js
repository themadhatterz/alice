Object.defineProperty(Source.prototype, "containerPosition", {
    get: function() {
        if (!this._containerPosition) {
            let spawn1 = Game.spawns[`${this.room.name}_1`];
            let bestSpot = this.pos.findPathTo(spawn1, {ignoreCreeps: true}).shift();
            this._containerPosition = bestSpot;
        }
        return this._containerPosition;
    }
});


Object.defineProperty(Source.prototype, "container", {
    get: function() {
        if (!this._container) {
            let containers = this.pos.findInRange(FIND_STRUCTURES, 2, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
            let constructionSites = this.pos.findInRange(FIND_MY_CONSTRUCTION_SITES, 2, {filter: function(structure) {return structure.structureType === STRUCTURE_CONTAINER}});
            if (containers.length > 0) {
                this._container = containers[0];
                for (let pos of this.pos.availableNeighbors(true)) {
                    this.room.verifyRampart(pos.x, pos.y)
                }
            } else if (constructionSites.length === 0) {
                let containerPosition = this.containerPosition;
                this.room.verifyBuild(STRUCTURE_CONTAINER, containerPosition.x, containerPosition.y, false);
                this._container = null;
            } else {
                this._container = null;
            }
        }
        return this._container;
    },
    configurable: true,
});


Object.defineProperty(Source.prototype, "linkPosition", {
    get: function() {
        if (!this._linkPosition) {
            let availableNeighbors = this.pos.availableNeighbors(ignoreCreeps = true);
            if (availableNeighbors.length === 0) {
                let harvesterNearby = this.pos.findInRange(FIND_MY_CREEPS, 1, {filter: function(creep) { return creep.memory.role === ROLE_HARVESTER}})[0];
                if (harvesterNearby) {
                    availableNeighbors = harvesterNearby.pos.availableNeighbors(ignoreCreeps = true);
                }
            }
            if (availableNeighbors.length > 0) {
                let withinOne = null;
                for (let pos of availableNeighbors) {
                    let harvesterInRange = pos.findInRange(FIND_MY_CREEPS, 1);
                    if (harvesterInRange.length > 0) {
                        withinOne = pos;
                        break;
                    }
                }
                let bestSpot = withinOne;
                this._linkPosition = bestSpot;
            } else {
                this._linkPosition = null;
            }
        }
        return this._linkPosition;
    }
});


Object.defineProperty(Source.prototype, "link", {
    get: function() {
        if (!this._link) {
            let links = this.pos.findInRange(FIND_STRUCTURES, 2, {filter: function(structure) {return structure.structureType === STRUCTURE_LINK}});
            let constructionSites = this.pos.findInRange(FIND_MY_CONSTRUCTION_SITES, 2, {filter: function(structure) {return structure.structureType === STRUCTURE_LINK}});
            if (links.length > 0) {
                this._link = links[0];
                this.room.verifyRampart(links[0].pos.x, links[0].pos.y);
            } else if (constructionSites.length === 0) {
                let linkPosition = this.linkPosition;
                if (linkPosition) {
                    this.room.verifyBuild(STRUCTURE_LINK, linkPosition.x, linkPosition.y, anchor=false);
                }
                this._link = null;
            } else {
                this._link = null;
            }
        }
        return this._link;
    },
    configurable: true,
})


Object.defineProperty(Source.prototype, "workPower", {
    get: function() {
        if (!this._workPower) {
            this._workPower = 0;
            for (let creep of this.harvesters) {
                this._workPower += creep.getActiveBodyparts(WORK);
            }
        }
        return this._workPower;
    },
    configurable: true,
});


Object.defineProperty(Source.prototype, "harvesters", {
    get: function() {
        if (!this._harvesters) {
            this._harvesters = _.filter(Game.creeps, (creep) => creep.role === ROLE_HARVESTER && creep.target === this);
        }
        return this._harvesters;
    }
});