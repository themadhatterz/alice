Object.defineProperty(Store.prototype, "isEmpty", {
    get: function() {
        if (!this._isEmpty) {
            this._isEmpty = this.getUsedCapacity() === 0;
        }
        return this._isEmpty;
    },
    configurable: true,
});


Object.defineProperty(Store.prototype, "isFull", {
    get: function() {
        if (!this._isFull) {
            this._isFull = this.getFreeCapacity() === 0;
        }
        return this._isFull;
    }
});