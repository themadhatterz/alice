let targetCache = require("../caching");


Object.defineProperty(RoomObject.prototype, "ref", {
    get: function () {
        return this.id || this.name || "";
    },
    configurable: true,
});


Object.defineProperty(RoomObject.prototype, "targetedBy", {
    get: function () {
        targetCache.TargetCache.assert();
        return _.map(Game.TargetCache.targets[this.ref], name => Game.creeps[name]);
    },
    configurable: true,
});


RoomObject.prototype.targetedByRole = function(role) {
    targetCache.TargetCache.assert();
    this._targetedByRole = [];
    for (let creep of this.targetedBy) {
        if (creep.memory.role === role) {
            this._targetedByRole.push(creep);
        }
    }
    return this._targetedByRole;
}


Object.defineProperty(RoomObject.prototype, "isCriticalStructure", {
    get: function() {
        if (!this._isCriticalStructure) {
            this._isCriticalStructure = this.structureType === STRUCTURE_SPAWN || 
                                        this.structureType === STRUCTURE_CONTROLLER ||
                                        this.structureType === STRUCTURE_TOWER ||
                                        this.structureType === STRUCTURE_STORAGE ||
                                        this.structureType === STRUCTURE_EXTENSION ||
                                        this.structureType === STRUCTURE_LAB ||
                                        this.structureType === STRUCTURE_TERMINAL ||
                                        this.structureType === STRUCTURE_POWER_SPAWN
        }
        return this._isCriticalStructure;
    },
    configurable: true,
});