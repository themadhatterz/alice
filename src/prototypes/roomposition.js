RoomPosition.prototype.findClosestTargetByPath = function(targets) {
    // TODO: out of room target length (findPathTo returns path to room exit only)
    let inRoomTargets = [];
    let outRoomTargets = [];
    for (let target of targets) {
        let roomName = null;
        if (target instanceof RoomPosition) {
            roomName = target.roomName;
        } else {
            roomName = target.room.name;
        }
        if (this.roomName === roomName) {
            inRoomTargets.push(target);
        } else {
            outRoomTargets.push(target);
        }
    }
    if (inRoomTargets.length > 0) {
        return this.findClosestByPath(inRoomTargets);
    } else {
        return outRoomTargets[0];
    }
}


Object.defineProperty(RoomPosition.prototype, "neighbors", {
    get: function () {
        let adjPos = [];
        for (let dx of [-1, 0, 1]) {
            for (let dy of [-1, 0, 1]) {
                if (!(dx == 0 && dy == 0)) {
                    let x = this.x + dx;
                    let y = this.y + dy;
                    if (0 < x && x < 49 && 0 < y && y < 49) {
                        adjPos.push(new RoomPosition(x, y, this.roomName));
                    }
                }
            }
        }
        return adjPos;
    }
});


RoomPosition.prototype.isPassible = function (ignoreCreeps = false) {
    let terrain = Game.map.getRoomTerrain(this.roomName);
    if (terrain.get(this.x, this.y) === TERRAIN_MASK_WALL) {
        return false;
    }
    if (ignoreCreeps === false && this.lookFor(LOOK_CREEPS).length > 0)
        return false;
    let impassibleStructures = _.filter(this.lookFor(LOOK_STRUCTURES), function (s) {
        return this.structureType != STRUCTURE_ROAD &&
                s.structureType != STRUCTURE_CONTAINER &&
                !(s.structureType == STRUCTURE_RAMPART && (s.my ||
                                                            s.isPublic));
    });
    return impassibleStructures.length == 0;
};


RoomPosition.prototype.availableNeighbors = function (ignoreCreeps = false) {
    return _.filter(this.neighbors, pos => pos.isPassible(ignoreCreeps));
};


Object.defineProperty(RoomPosition.prototype, "isEdge", {
    get: function () {
        if (!this._isEdge) {
            this._isEdge = this.x == 0 || this.x == 49 || this.y == 0 || this.y == 49;
        }
        return this._isEdge;
    },
});


Object.defineProperty(RoomPosition.prototype, 'inRampart' , {
    get: function() {
        if (!this._inRampart) {
            this._inRampart = _.filter(this.lookFor(LOOK_STRUCTURES), (s) => s.structureType === STRUCTURE_RAMPART).length > 0;
        }
        
        return this._inRampart
    },
    configurable: true,
});