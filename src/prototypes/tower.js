const log = require("../log");


StructureTower.prototype.Amount = function (type, target) { //need to pass the type (TOWER_POWER_ATTACK, TOWER_POWER_HEAL or TOWER_POWER_REPAIR) and target
    let range = this.pos.getRangeTo(target)
    let amount = type;
    if(range > TOWER_OPTIMAL_RANGE) {
        if(range > TOWER_FALLOFF_RANGE) {
            range = TOWER_FALLOFF_RANGE;
        }
        amount -= amount * TOWER_FALLOFF * (range - TOWER_OPTIMAL_RANGE) / (TOWER_FALLOFF_RANGE - TOWER_OPTIMAL_RANGE);
    }
    [PWR_OPERATE_TOWER, PWR_DISRUPT_TOWER].forEach(power => {
        const effect = this.Effects[power];
        if (effect) {
            amount *= POWER_INFO[power].effect[effect.level-1];
        }
    });
    amount = Math.floor(amount);
    
    return amount
}