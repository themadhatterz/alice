const log = require("./log");

// TODO: MIN/MAX Price based on some logic.
const MIN_SELL_PRICE = 0.01;
const MAX_BUY_PRICE = 100;
const BUY_UNDER_LEVEL = 6000;
const SELL_OVER_LEVEL = 9000;
const MINERALS = {
    [RESOURCE_HYDROGEN]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
    [RESOURCE_OXYGEN]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
    [RESOURCE_UTRIUM]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
    [RESOURCE_KEANIUM]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
    [RESOURCE_LEMERGIUM]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
    [RESOURCE_ZYNTHIUM]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
    [RESOURCE_CATALYST]: {
        BUY: BUY_UNDER_LEVEL,
        SELL: SELL_OVER_LEVEL,
        MIN: MIN_SELL_PRICE,
        MAX: MAX_BUY_PRICE,
    },
};


function initializeMarket(colony, terminal) {
    const fn = "initializeMarket";
    return new Market(colony, terminal);
}


class Market {
    constructor(colony, terminal) {
        this.colony = colony;
        this.room = colony.room;
        this.terminal = terminal;
        this.dealsExecuted = 0;
    }

    get energy() {
        return this.terminal.store.getUsedCapacity(RESOURCE_ENERGY);
    }

    hasEnergyForDeal(amount, order) {
        return this.energy >= Game.market.calcTransactionCost(amount, order.roomName, this.room.name);
    }

    buy(resource, amount) {
        // TODO: Create orders if not urgent to avoid paying the energy cost to make the deal.
        const fn = "Market.buy";
        log.write(log.MARKET, fn, `${this.room.name} getting orders to buy ${amount} of ${resource}`);
        const orders = Game.market.getAllOrders({type: ORDER_SELL, resourceType: resource}).sort(function(a,b) { return a.price < b.price ? -1 : 1});
        for (const order of orders) {
            if (amount <= 0) {
                break;
            }
            const toBuy = order.amount > amount ? amount : order.amount;
            amount -= toBuy;
            // TODO: Get average prices to determine min/max
            if (order.price <= MINERALS[resource].MAX &&this.hasEnergyForDeal(toBuy, order)) {
                log.write(log.MARKET, fn, `${this.room.name} buying ${toBuy} of ${resource} at ${order.price}`);
                Game.market.deal(order.id, toBuy, this.room.name);
                this.dealsExecuted += 1;
            }
        }
    }

    sell(resource, amount) {
        // TODO: Create orders if not urgent to avoid paying the energy cost to make the deal.
        const fn = "Market.sell";
        log.write(log.MARKET, fn, `${this.room.name} getting orders to sell ${amount} of ${resource}`);
        const orders = Game.market.getAllOrders({type: ORDER_BUY, resourceType: resource}).sort(function(a,b) { return a.price > b.price ? -1 : 1});
        for (const order of orders) {
            if (amount <= 0) {
                break;
            }
            const toSell = order.amount > amount ? amount : order.amount;
            amount -= toSell;
            // TODO: Get average prices to determine min/max
            if (order.price >= MINERALS[resource].MIN && this.hasEnergyForDeal(toSell, order)) {
                log.write(log.MARKET, fn, `${this.room.name} selling ${toSell} of ${resource} at ${order.price}`);
                Game.market.deal(order.id, toSell, this.room.name);
                this.dealsExecuted += 1;
            }
        }
    }

    baseMineralHandling() {
        const fn = "Market.baseMineralHandling";
        for (const mineral in MINERALS) {
            // Can only execute 10 deals per tick. Needs to be global eventually.
            if (this.dealsExecuted === 10) {
                break;
            }
            const resourceData = MINERALS[mineral];
            const storageTotal = this.colony.resourceStorage(mineral);
            if (storageTotal < resourceData.BUY) {
                const shortage = resourceData.BUY - storageTotal;
                this.buy(mineral, shortage);
            } else if (storageTotal > resourceData.SELL) {
                const overage = storageTotal - resourceData.SELL;
                this.sell(mineral, overage);
            }
        }
    }

    gameResourcesHandling() {
        const fn = "Market.gameResourcesHandling";
        // Sell Pixels $$$
        let pixelCount = Game.resources.pixel;
        if (pixelCount > 0) {
            const orders = Game.market.getAllOrders({type: ORDER_BUY, resourceType: PIXEL}).sort(function(a,b) { return a.price > b.price ? -1 : 1});
            for (const order of orders) {
                if (pixelCount <= 0) {
                    break;
                }
                const toSell = order.amount >= Game.resources.pixel ? Game.resources.pixel : order.amount;
                pixelCount -= toSell;
                log.write(log.MARKET, fn, `${this.room.name} selling ${toSell} of PIXEL at ${order.price}`);
                Game.market.deal(order.id, toSell);
            }
        }
    }

    run() {
        const fn = "Market.run";
        if (this.terminal.cooldown === 0 && this.energy >= 1000) {
            // TODO: Add logic to buy energy if colony is in defecit to much this.colony.energySurplus
            // TODO: It should be a createOrder call, deal execution pays the energy fee which would defeat the purpose
            // TODO: And/or calculate the effective price of the transfer
            this.baseMineralHandling();
            if (this.dealsExecuted === 0) {
                this.gameResourcesHandling();
            }
        }
    }
}


module.exports = {
    initializeMarket: initializeMarket,
}