const base = require("./base");
const colony = require("./colony");
const log = require("./log");
const utilities = require("./utilities");

const COMPOUND_BASE_DEFAULT = 6000;
const COMPOUND_T1_DEFAULT = 1000;
const COMPOUND_T2_DEFAULT = 2000;
const COMPOUND_T3_DEFAULT = 6000;
const REACTIONS = {
    [RESOURCE_ZYNTHIUM_KEANITE]: {
        keep: COMPOUND_BASE_DEFAULT,
        one: RESOURCE_ZYNTHIUM,
        two: RESOURCE_KEANIUM,
        time: 5, 
    },
    [RESOURCE_UTRIUM_LEMERGITE]: {
        keep: COMPOUND_BASE_DEFAULT,
        one: RESOURCE_UTRIUM,
        two: RESOURCE_LEMERGIUM,
        time: 5,
    },
    [RESOURCE_GHODIUM]: {
        keep: COMPOUND_BASE_DEFAULT,
        one: RESOURCE_ZYNTHIUM_KEANITE,
        two: RESOURCE_UTRIUM_LEMERGITE,
        time: 5,
    },
    [RESOURCE_UTRIUM_HYDRIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_UTRIUM,
        two: RESOURCE_HYDROGEN,
        time: 10,
    },
    [RESOURCE_UTRIUM_OXIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_UTRIUM,
        two: RESOURCE_OXYGEN,
        time: 10,
    },
    [RESOURCE_KEANIUM_HYDRIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_KEANIUM,
        two: RESOURCE_HYDROGEN,
        time: 10,
    },
    [RESOURCE_KEANIUM_OXIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_KEANIUM,
        two: RESOURCE_OXYGEN,
        time: 10,
    },
    [RESOURCE_LEMERGIUM_HYDRIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_LEMERGIUM,
        two: RESOURCE_HYDROGEN,
        time: 15,
    },
    [RESOURCE_LEMERGIUM_OXIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_LEMERGIUM,
        two: RESOURCE_OXYGEN,
        time: 10,
    },
    [RESOURCE_ZYNTHIUM_HYDRIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_ZYNTHIUM,
        two: RESOURCE_HYDROGEN,
        time: 20,
    },
    [RESOURCE_ZYNTHIUM_OXIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_ZYNTHIUM,
        two: RESOURCE_OXYGEN,
        time: 10,
    },
    [RESOURCE_GHODIUM_HYDRIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_GHODIUM,
        two: RESOURCE_HYDROGEN,
        time: 10,
    },
    [RESOURCE_GHODIUM_OXIDE]: {
        keep: COMPOUND_T1_DEFAULT,
        one: RESOURCE_GHODIUM,
        two: RESOURCE_OXYGEN,
        time: 10,
    },
    [RESOURCE_HYDROXIDE]: {
        keep: COMPOUND_T2_DEFAULT * 2,
        one: RESOURCE_HYDROGEN,
        two: RESOURCE_OXYGEN,
        time: 20,
    },
    [RESOURCE_UTRIUM_ACID]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_UTRIUM_HYDRIDE,
        two: RESOURCE_HYDROXIDE,
        time: 5,
    },
    [RESOURCE_UTRIUM_ALKALIDE]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_UTRIUM_OXIDE,
        two: RESOURCE_HYDROXIDE,
        time: 5,
    },
    [RESOURCE_KEANIUM_ACID]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_KEANIUM_HYDRIDE,
        two: RESOURCE_HYDROXIDE,
        time: 5,
    },
    [RESOURCE_KEANIUM_ALKALIDE]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_KEANIUM_OXIDE,
        two: RESOURCE_HYDROXIDE,
        time: 5,
    },
    [RESOURCE_LEMERGIUM_ACID]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_LEMERGIUM_HYDRIDE,
        two: RESOURCE_HYDROXIDE,
        time: 10,
    },
    [RESOURCE_LEMERGIUM_ALKALIDE]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_LEMERGIUM_OXIDE,
        two: RESOURCE_HYDROXIDE,
        time: 5,
    },
    [RESOURCE_ZYNTHIUM_ACID]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_ZYNTHIUM_HYDRIDE,
        two: RESOURCE_HYDROXIDE,
        time: 40,
    },
    [RESOURCE_ZYNTHIUM_ALKALIDE]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_ZYNTHIUM_OXIDE,
        two: RESOURCE_HYDROXIDE,
        time: 5,
    },
    [RESOURCE_GHODIUM_ACID]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_GHODIUM_HYDRIDE,
        two: RESOURCE_HYDROXIDE,
        time: 15,
    },
    [RESOURCE_GHODIUM_ALKALIDE]: {
        keep: COMPOUND_T2_DEFAULT,
        one: RESOURCE_GHODIUM_OXIDE,
        two: RESOURCE_HYDROXIDE,
        time: 30,
    },
    [RESOURCE_CATALYZED_UTRIUM_ACID]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_UTRIUM_ACID,
        two: RESOURCE_CATALYST,
        time: 60,
    },
    [RESOURCE_CATALYZED_UTRIUM_ALKALIDE]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_UTRIUM_ALKALIDE,
        two: RESOURCE_CATALYST,
        time: 60,
    },
    [RESOURCE_CATALYZED_KEANIUM_ACID]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_KEANIUM_ACID,
        two: RESOURCE_CATALYST,
        time: 60,
    },
    [RESOURCE_CATALYZED_KEANIUM_ALKALIDE]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_KEANIUM_ALKALIDE,
        two: RESOURCE_CATALYST,
        time: 60,
    },
    [RESOURCE_CATALYZED_LEMERGIUM_ACID]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_LEMERGIUM_ACID,
        two: RESOURCE_CATALYST,
        time: 65,
    },
    [RESOURCE_CATALYZED_LEMERGIUM_ALKALIDE]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_LEMERGIUM_ALKALIDE,
        two: RESOURCE_CATALYST,
        time: 60,
    },
    [RESOURCE_CATALYZED_ZYNTHIUM_ACID]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_ZYNTHIUM_ACID,
        two: RESOURCE_CATALYST,
        time: 160,
    },
    [RESOURCE_CATALYZED_ZYNTHIUM_ALKALIDE]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_ZYNTHIUM_ALKALIDE,
        two: RESOURCE_CATALYST,
        time: 60,
    },
    [RESOURCE_CATALYZED_GHODIUM_ACID]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_GHODIUM_ACID,
        two: RESOURCE_CATALYST,
        time: 80,
    },
    [RESOURCE_CATALYZED_GHODIUM_ALKALIDE]: {
        keep: COMPOUND_T3_DEFAULT,
        one: RESOURCE_GHODIUM_ALKALIDE,
        two: RESOURCE_CATALYST,
        time: 150,
    },
}


function initializeLab(protoLab, colony) {
    const fn = "initializeLab";
    const labOne = protoLab.labOne;
    const labTwo = protoLab.labTwo;
    const state = protoLab.state;
    const reaction = protoLab.reaction;
    const amount = protoLab.amount;
    const boost = protoLab.boost;
    return new Lab(colony, labOne, labTwo, state, reaction, amount, boost);
}


function initializeOutputLab(lab, protoLab, colony) {
    const state = protoLab.state;
    const resource = protoLab.resource;
    const amount = protoLab.amount;
    return new OutputLab(colony, lab, state, resource, amount);
}


class OutputLab {
    constructor(colony, lab, state, resource, amount) {
        this.colony = colony;
        this.lab = lab;
        this._state = state;
        this._resource = resource;
        this._amount = amount;
    }

    get state() {
        return this._state;
    }

    set state(newState) {
        this._state = newState;
        this.colony.room.memory.lab.boost[this.lab.ref].state = newState;
    }

    get resource() {
        return this._resource;
    }

    set resource(newResource) {
        this._resource = newResource;
        this.colony.room.memory.lab.boost[this.lab.ref].resource = newResource;
    }

    get amount() {
        return this._amount;
    }

    set amount(newAmount) {
        this._amount = newAmount;
        this.colony.room.memory.lab.boost[this.lab.ref].amount = newAmount;
    }

    get boosting() {
        return this.state === LAB_BOOSTING;
    }

    get unboosting() {
        return this.state === LAB_UNBOOSTING;
    }
}


class Lab {
    constructor(colony, labOne, labTwo, state, reaction, amount, boost) {
        this.colony = colony;
        this.room = colony.room;
        this._labOne = utilities.deref(labOne);
        this._labTwo = utilities.deref(labTwo);
        this._state = state; // Available > Requested > Working > Done
        this._reaction = reaction;
        this._amount = amount;
        this.boost = boost;
        if (reaction) {
            this.resourceOne = REACTIONS[reaction].one;
            this.resourceTwo = REACTIONS[reaction].two;
        } else {
            this.resourceOne = null;
            this.resourceTwo = null;
        }
    }
    // TODO: Add api for unboosting creeps.

    get labOne() {
        if (!this._labOne) {
            if (!this.room.memory.lab.labOne) {
                const labOnePos = base.LAYOUT["6"]["lab"][0];
                const labOneFind = this.room.lookForAt(LOOK_STRUCTURES, this.room.anchor.x + labOnePos.x, this.room.anchor.y + labOnePos.y);
                const labOneFiltered = _.filter(labOneFind, (s) => s.structureType === STRUCTURE_LAB)[0];
                if (labOneFiltered) {
                    this._labOne = labOneFiltered;
                    this.room.memory.lab.labOne = labOneFiltered.ref;
                }
            }
        }
        return this._labOne;
    }

    get labTwo() {
        if (!this._labTwo) {
            if (!this.room.memory.lab.labTwo) {
                const labTwoPos = base.LAYOUT["6"]["lab"][2];
                const labTwoFind = this.room.lookForAt(LOOK_STRUCTURES, this.room.anchor.x + labTwoPos.x, this.room.anchor.y + labTwoPos.y);
                const labTwoFiltered = _.filter(labTwoFind, (s) => s.structureType === STRUCTURE_LAB)[0];
                if (labTwoFiltered) {
                    this._labTwo = labTwoFiltered;
                    this.room.memory.lab.labTwo = labTwoFiltered.ref;
                }
            }
        }
        return this._labTwo;
    }

    get state() {
        return this._state;
    }

    set state(newState) {
        this._state = newState;
        this.room.memory.lab.state = newState;
    }

    get reaction() {
        return this._reaction;
    }

    set reaction(newReaction) {
        this._reaction = newReaction
        this.room.memory.lab.reaction = newReaction;
        if (newReaction) {
            this.resourceOne = REACTIONS[newReaction].one;
            this.resourceTwo = REACTIONS[newReaction].two;

            this.room.memory.lab.resourceOne = REACTIONS[newReaction].one;
            this.room.memory.lab.resourceTwo = REACTIONS[newReaction].two;
        }

    }

    get amount() {
        return this._amount;
    }

    set amount(newAmount) {
        this._amount = newAmount;
        this.room.memory.lab.amount = newAmount;
    }

    get outputLabs() {
        // TODO: Move output lab boosting to a class?
        if (!this._outputLabs) {
            this._outputLabs = [];
            for (let lab of this.room.labs) {
                if (lab !== this.labOne && lab !== this.labTwo) {
                    if (!(lab.ref in this.room.memory.lab.boost)) {
                        this.room.memory.lab.boost[lab.ref] = {state: LAB_AVAILABLE, resource: null, amount: null};
                    }
                    let newOutputLab = initializeOutputLab(lab, this.room.memory.lab.boost[lab.ref], this.colony);
                    this._outputLabs.push(newOutputLab);
                }
            }
        }
        return this._outputLabs;
    }

    get outputLabsEmpty() {
        if (!this._outputLabsEmpty) {
            let storage = 0;
            for (let labObj of this.outputLabs) {
                storage += labObj.lab.store.getUsedCapacity(this.reaction);
            }
            this._outputLabsEmpty = (storage === 0);
        }
        return this._outputLabsEmpty;
    }

    get available() {
        return this.state === LAB_AVAILABLE;
    }

    get requested() {
        return this.state === LAB_REQUESTED;
    }

    get working() {
        return this.state === LAB_WORKING;
    }

    get done() {
        return this.state === LAB_DONE;
    }

    get boosting() {
        for (let lab of this.outputLabs) {
            if (lab.boosting || lab.unboosting) {
                return true;
            }
        }
    }

    get labTicksLeft() {
        let amount = this.labOne.store.getUsedCapacity(this.resourceOne);
        let time = REACTIONS[this.reaction].time;
        let totalTicks = ((amount / 5 / this.outputLabs.length) * time);
        let timeLeft = utilities.ticksToTime(totalTicks);
        return timeLeft;
    }

    get outputLabAvailable() {
        for (let lab of this.outputLabs) {
            if (!lab.boosting && !lab.unboosting) {
                return true;
            }
        }
    }

    availableOutputLabForBoosting() {
        for (let lab of this.outputLabs) {
            if (!lab.boosting && !lab.unboosting) {
                return lab;
            }
        }
    }

    availableOutputLabForUnBoosting() {
        // Cannot unboost when on cooldown.
        for (let lab of this.outputLabs) {
            if (!lab.boosting && !lab.unboosting && lab.cooldown <= 150) {
                return lab;
            }
        }
    }

    unboostRequest() {
        let labObj = this.availableOutputLabForUnBoosting();
        if (labObj) {
            labObj.state = LAB_UNBOOSTING;
            labObj.resource = null;
            labObj.amount = null;
            return labObj.lab;
        }
    }

    boostRequest(resource, amount) {
        let labObj = this.availableOutputLabForBoosting();
        if (labObj) {
            labObj.state = LAB_BOOSTING;
            labObj.resource = resource;
            labObj.amount = amount;
            return labObj.lab;
        }   
    }

    resourceBoostAvailable(boostType) {
        let options = {};

        for (let resource of BOOST_TYPES[boostType]) {
            options[resource] = this.colony.resourceStorage(resource);
        }

        return options;
    }

    refreshLabRef() {
        this.room.memory.lab.labOne = null;
        this.room.memory.lab.labTwo = null;
    }

    startNewOrder() {
        const fn = "Lab.startNewOrder";
        for (let compoundName in REACTIONS) {
            let compoundObj = REACTIONS[compoundName];
            let currentStorage = this.colony.resourceStorage(compoundName);
            if (currentStorage < compoundObj.keep) {
                let resourceOneStorage = this.colony.resourceStorage(compoundObj.one);
                let resourceTwoStorage = this.colony.resourceStorage(compoundObj.two);
                let totalNeeded = compoundObj.keep - currentStorage;
                if (resourceOneStorage === 0 || resourceTwoStorage === 0) {
                    continue;
                } else if (resourceOneStorage < totalNeeded || resourceTwoStorage < totalNeeded) {
                    totalNeeded = _.min([resourceOneStorage, resourceTwoStorage]);
                } else if (totalNeeded > 3000) {
                    totalNeeded = 3000;
                }
                if (totalNeeded % 5 !== 0) {
                    totalNeeded = totalNeeded - (totalNeeded % 5);
                    if (totalNeeded === 0) {
                        log.write(log.LAB, fn, `${this.colony.name} ignoring order of ${compoundName} of less than 5.`);
                        continue;
                    }
                }
                if (resourceOneStorage >= totalNeeded && resourceTwoStorage >= totalNeeded) {
                    log.write(log.LAB, fn, `${this.colony.name} prepare ${totalNeeded} of ${compoundName} to be created!`);
                    this.state = LAB_REQUESTED;
                    this.amount = totalNeeded;
                    this.reaction = compoundName;
                    break;
                }
            }
        }
    }

    monitorRequested() {
        const fn = "Lab.monitorRequested";
        if (this.labOne.store.getUsedCapacity(this.resourceOne) === this.amount && this.labTwo.store.getUsedCapacity(this.resourceTwo) === this.amount) {
            log.write(log.LAB, fn, `${this.colony.name} ready to start reaction.`);
            this.state = LAB_WORKING;
        }
    }

    executeReaction() {
        const fn = "Lab.executeReaction";
        if (this.outputLabs.length > 0) {
            for (let labObj of this.outputLabs) {
                if (labObj.lab.cooldown === 0 && !labObj.boosting && !labObj.unboosting) {
                    labObj.lab.runReaction(this.labOne, this.labTwo);
                }
            }
        }
        if (this.labOne.store.getUsedCapacity(this.resourceOne) === 0 && this.labTwo.store.getUsedCapacity(this.resourceTwo) === 0) {
            log.write(log.LAB, fn, `${this.colony.name} lab work completed.`);
            this.state = LAB_DONE;
        }
    }

    run() {
        const fn = "Lab.run";
        // Wishful input lab destruction protection
        if (this.room.labs.length >= 3 && (!this.labOne || !this.labTwo)) {
            this.refreshLabRef();
            if (!this.labOne || !this.labTwo) {
                // Still got nothing back on input labs, but we have 3 labs.
                log.write(log.ERROR|log.LAB, fn, `${this.colony.name} cannot detect one or both input labs.`);
                return;
            }
        }
        if (this.available) {
            this.startNewOrder();
        } else if (this.requested) {
            this.monitorRequested();
        } else if (this.working) {
            this.executeReaction();
        } else if (this.done && this.outputLabsEmpty) {
            this.amount = 0
            this.state = LAB_AVAILABLE;
            this.reaction = null,
            this.resourceOne = null;
            this.resourceTwo = null;
        }
    }
}


module.exports = {
    initializeLab: initializeLab,
    Lab: Lab,
}